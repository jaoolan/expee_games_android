package com.fourello.expee

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.graphics.BitmapFactory
import android.os.Build
import android.util.Log
import androidx.core.app.NotificationCompat
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.fourello.expee.activities.MainMenuActivity
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import com.google.gson.Gson

class MyFirebaseMessagingService : FirebaseMessagingService() {
    private val CHANNEL_ID = "com.fourello.expee.android"
    private var broadcaster: LocalBroadcastManager? = null

    override fun onCreate() {
        super.onCreate()
        broadcaster = LocalBroadcastManager.getInstance(this)

    }
    override fun onMessageReceived(remoteMessage: RemoteMessage) {
        super.onMessageReceived(remoteMessage)

        Log.i("NotificationsService", "Got a remote message 🎉")
        try {
            val outputJson: String = Gson().toJson(remoteMessage.notification!!.body )
            val withoutQuotes_line1 = outputJson.replace("\"", "")
            showNotification(withoutQuotes_line1, "expee", applicationContext)
            Log.i("Notifications", outputJson)
        }catch (e: Exception){
            Log.i("Notifications error",e.message)


        }    }
    private fun showNotification( body: String, title: String, ctx: Context) {
        val intent = Intent(ctx, MainMenuActivity::class.java)
        val contentIntent = PendingIntent.getActivity(ctx, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT)

        val builder = NotificationCompat.Builder(
            ctx,
            CHANNEL_ID
        )

        val notification = builder
            .setSmallIcon(R.drawable.expee_icon_small)

            .setContentTitle(title)
            .setContentText(body)
//            .setStyle(
//                NotificationCompat.BigTextStyle()
//                    .bigText(body)
//            )
            .setChannelId(CHANNEL_ID)
            .setAutoCancel(true)
            .setContentIntent(contentIntent)
            .setPriority(NotificationCompat.PRIORITY_HIGH)
            .setDefaults(NotificationCompat.DEFAULT_SOUND)
            .build()


        //            NotificationManager notificationManager = (NotificationManager) ctx.getSystemService(Context.NOTIFICATION_SERVICE);
        //            if (notificationManager != null) {
        //                notificationManager.notify(1, notification);
        //            }

        //            if (notificationManager != null) {
        //                notificationManager.notify(0, notification);
        //            }
        val notificationManager: NotificationManager?

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val name = ctx.getString(R.string.notification)
            val importance = NotificationManager.IMPORTANCE_HIGH
            val channel = NotificationChannel(
                CHANNEL_ID,
                name,
                importance
            )
            notificationManager = ctx.getSystemService(NotificationManager::class.java)
            notificationManager?.createNotificationChannel(channel)
        } else {
            notificationManager = ctx.getSystemService(
                Context.NOTIFICATION_SERVICE
            ) as NotificationManager
        }

        notificationManager?.notify(1, notification)
    }




    override fun onDeletedMessages() {
        super.onDeletedMessages()
    }

    override fun onMessageSent(s: String) {
        super.onMessageSent(s)
    }

    override fun onSendError(s: String, e: Exception) {
        super.onSendError(s, e)
    }
}