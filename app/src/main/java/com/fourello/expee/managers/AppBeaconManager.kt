package com.fourello.expee.managers

import android.annotation.SuppressLint
import android.content.Context
import android.os.RemoteException
import android.util.Log
import com.fourello.expee.model.Campaign
import com.fourello.expee.model.MainProducts
import com.fourello.expee_kotlin.model.BeaconListData

import org.altbeacon.beacon.*
import java.lang.Exception
import java.util.*

open class AppBeaconManager(context: Context) {

    var beaconManager: BeaconManager

    private var mBeaconConsumer: BeaconConsumer? = null

    init {
        beaconManager = BeaconManager.getInstanceForApplication(context)
        beaconManager.beaconParsers.clear()
        beaconManager.beaconParsers.add(BeaconParser().setBeaconLayout("m:2-3=beac,i:4-19,i:20-21,i:22-23,p:24-24,d:25-25"))
        beaconManager.beaconParsers.add(BeaconParser().setBeaconLayout("m:2-3=0215,i:4-19,i:20-21,i:22-23,p:24-24"))
        beaconManager.beaconParsers.add(BeaconParser().setBeaconLayout(BeaconParser.ALTBEACON_LAYOUT))


     //   initBeaconsWhite()
        if (sBeaconDetailList!!.size == 0) {
            //    initBeacons();
            //   initBeaconsWhite();
            fromOnline = false
        }

    }

    fun setBeaconConsumer(beaconConsumer: BeaconConsumer) {
        mBeaconConsumer = beaconConsumer
        beaconManager.bind(mBeaconConsumer!!)
    }


    fun startRangingAndMonitoringInRegion() {
        if (mBeaconConsumer == null) return

        if (!beaconManager.isAnyConsumerBound) {
            beaconManager.bind(mBeaconConsumer!!)
        }

        try {
            beaconManager.startRangingBeaconsInRegion(ALL_REGION)
            beaconManager.startMonitoringBeaconsInRegion(ALL_REGION)
        } catch (e: RemoteException) {
            Log.e("Stop", e.message)
        }

    }

    @SuppressLint("LongLogTag")
    fun stopRangingAndMonitoringInRegion() {
        if (mBeaconConsumer == null) return

        if (!beaconManager.isAnyConsumerBound) {
            beaconManager.bind(mBeaconConsumer!!)
        }

        try {
            beaconManager.stopRangingBeaconsInRegion(ALL_REGION)
            beaconManager.stopMonitoringBeaconsInRegion(ALL_REGION)
        } catch (e: RemoteException) {
            Log.e("StopRangingAndMonitoringInRegion : ", e.message)
        }

    }

    // temporary code
    // sample code for testing beacons
    private fun addBeaconToList(
        beaconId: String?,
        uniqueName: String?,
        uuid: String?,
        major: Int?,
        minor: Int?,
        productId: String?,
        productName: String?,
        recipe: Int?,
        video: Int?,
        productImage: String?,
        productCode: String?,
        productImageDrawable: Int?
    ) {

        val beaconDetail = BeaconListData()
        val campaigns = Campaign()
        val campaign = Campaign()

        val product = ArrayList<MainProducts>()

        val product1 = MainProducts()


//        product1.id =(productId)
//        product1.setLabel(productName)
//        product1.setImage_regular_full(productImage)
//        // product1.setmImageAdDrawable(productImageDrawable);
//        //  product1.setVideo(video);
//        //  product1.setRecipe(recipe);
//        product1.setProduct_id(productCode)
//        product.add(product1)
//
//        campaign.setProducts(product)
//
//        beaconDetail.setId(beaconId)
//        beaconDetail.setUuid(uuid)
//        beaconDetail.setMajor(major)
//        beaconDetail.setMinor(minor)

        val region = Region(
            uniqueName,
            Identifier.fromUuid(UUID.fromString(uuid)),
            Identifier.fromInt(major!!),
            Identifier.fromInt(minor!!)
        )

        //campaigns.add(campaign);

        beaconDetail.mRegion = region
//        beaconDetail.setCampaigns(campaigns)

        sBeaconDetailList!!.add(beaconDetail)
    }


    private fun initBeaconsWhite() {


//        Beacon 04
//        BluetoothClass.Device.Major: 42271
//        Minor: 47184
//
//        Beacon 05
//        BluetoothClass.Device.Major: 25834
//        Minor: 2544
//
//        Beacon 06
//        BluetoothClass.Device.Major: 1518
//        Minor: 62261
//
//        Beacon 07
//        BluetoothClass.Device.Major: 23599
//        Minor: 50568
        addBeaconToList(
            "4",
            "Nestea2",
            "B9407F30-F5F8-466E-AFF9-25556B57FE6D",
            22249,
            31868,
            "4",
            "Nestea",
            0,
            null,
            "",
            "04800361291446",
            null
        );
        addBeaconToList(
            "5",
            "KokoKrunch2",
            "B9407F30-F5F8-466E-AFF9-25556B57FE6D",
            25834,
            2544,
            "5",
            "Koko Krunch",
            null,
            0,
            "",
            "04800361291446",
            null
        );
        addBeaconToList(
            "6",
            "Chuckie2",
            "B9407F30-F5F8-466E-AFF9-25556B57FE6D",
            1518,
            62261,
            "6",
            "Chuckie",
            0,
            null,
            "",
            "04800361291446",
            null
        );
        addBeaconToList(
            "7",
            "Magic2",
            "B9407F30-F5F8-466E-AFF9-25556B57FE6D",
            23599,
            50568,
            "7",
            "Magic Sarap",
            null,
            0,
            "",
            "04800361291446",
            null
        );

    }

    companion object {

        private val ALL_REGION = Region(
            "allRegion", null, null, null
        )
        var fromOnline: Boolean? = false
        private var sBeaconDetailList: MutableList<BeaconListData>? = ArrayList<BeaconListData>()

        fun getNearestBeacon(beacons: List<Beacon>): BeaconListData? {
            val beacon = findNearestBeacon(beacons)
            var b: BeaconListData? = null

            for (temp in sBeaconDetailList!!) {
                try {
                    if (temp.major == beacon!!.id2.toInt() && temp.minor == beacon!!.id3.toInt()) {

                        b = temp
                        break
                    }
                } catch (e: Exception) {
                    Log.e("error", e.message)

                }

            }
            return b
        }

        fun getBeaconByMajorMinor(major: Int, minor: Int): BeaconListData? {
            var beaconDetail: BeaconListData? = null
            for (temp in sBeaconDetailList!!) {
                if (temp.major == major && temp.minor == minor) {
                    beaconDetail = temp
                    break
                }
            }
            return beaconDetail
        }

        private fun findNearestBeacon(beacons: List<Beacon>): Beacon? {
            var nearestBeacon: Beacon? = null
            var nearestBeaconsDistance = -1.0

            for (beacon in beacons) {
                val distance = calculateDistance(beacon.txPower, beacon.rssi.toDouble())
                if (distance > -1 && (distance < nearestBeaconsDistance || nearestBeacon == null)) {
                    nearestBeacon = beacon
                    nearestBeaconsDistance = distance
                }
            }
            return nearestBeacon
        }

        private fun calculateDistance(txPower: Int, rssi: Double): Double {
            if (rssi == 0.0) {
                return -1.0
            }

            val ratio = rssi * 1.0 / txPower
            return if (ratio < 1.0) {
                Math.pow(ratio, 10.0)
            } else {
                0.89976 * Math.pow(ratio, 7.7095) + 0.111
            }
        }


        fun filteredFoundBeacons(collection: Collection<Beacon>): List<Beacon> {
            val oldList = ArrayList(collection)
            val newList = ArrayList<Beacon>()

            for (b in oldList) {
                for (beaconDetail in sBeaconDetailList!!) {
                    if (b.id2.toInt() == beaconDetail.mRegion!!.id2.toInt() && b.id3.toInt() == beaconDetail.mRegion!!.id3.toInt()) {

                        newList.add(b)

                        break
                    }
                }
            }
            return newList
        }

        fun clearBeaconList() {
            if (sBeaconDetailList != null) sBeaconDetailList!!.clear()
        }

        fun setBeaconDetailList(beaconDetailList: MutableList<BeaconListData>) {
            sBeaconDetailList = beaconDetailList
        }

        val beaconList: List<BeaconListData>?
            get() = sBeaconDetailList
    }
    /*
          White Beacon

          Beacon 01
          Major = 50118
          Minor = 52608

          Beacon 02
          Major = 37517
          Minor = 31233

          Beacon 03
          Major = 12656
          Minor = 30910
          **/
}
