package com.fourello.expee.receivers

import android.annotation.SuppressLint
import android.app.AlarmManager
import android.app.PendingIntent
import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothDevice
import android.bluetooth.BluetoothManager
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.Bundle

import com.fourello.expee.activities.MainMenuActivity
import com.fourello.expee.constant.RequestCode
import com.fourello.expee.constant.Time
import com.fourello.expee.constant.keys
import com.fourello.expee.managers.AppBeaconManager
import com.fourello.expee_kotlin.model.BeaconListData


import uk.co.alt236.bluetoothlelib.device.BluetoothLeDevice
import uk.co.alt236.bluetoothlelib.device.beacon.BeaconType
import uk.co.alt236.bluetoothlelib.device.beacon.BeaconUtils
import uk.co.alt236.bluetoothlelib.device.beacon.ibeacon.IBeaconDevice
import java.util.*

class BluetoothDeviceReceiver : BroadcastReceiver() {

    private var mContext: Context? = null
    private var mAdapter: BluetoothAdapter? = null
   // private var mUser: User? = null
    private var mBeaconDetail: BeaconListData? = null

    private var mBeaconNotificationList: MutableList<BeaconListData>? = null
    private val mainMenuActivity1: MainMenuActivity? = null

    private val mLeScannerCallback = object : BluetoothAdapter.LeScanCallback {
        override fun onLeScan(device: BluetoothDevice, rssi: Int, scanRecord: ByteArray) {
            val deviceLe = BluetoothLeDevice(
                device,
                rssi,
                scanRecord,
                System.currentTimeMillis()
            )

            if (BeaconUtils.getBeaconType(deviceLe) === BeaconType.IBEACON) {
                val beaconDevice = IBeaconDevice(deviceLe)
                var beaconDetail:BeaconListData = AppBeaconManager.getBeaconByMajorMinor(
                    beaconDevice.getMajor(),
                    beaconDevice.getMinor()
                ) ?: return

                if (mBeaconNotificationList!!.size == 2) {
                    mAdapter!!.stopLeScan(this)
                    return
                }

                if (mBeaconNotificationList!!.size > 0) {
                    val lastBeacon = mBeaconNotificationList!![mBeaconNotificationList!!.size - 1]

                    if (!lastBeacon.equals(beaconDetail)) {
                        mBeaconNotificationList!!.add(beaconDetail)
                    } else {
                        beaconDetail = BeaconListData()
                    }
                } else {
                    mBeaconNotificationList!!.add(beaconDetail)
                }
                if (beaconDetail == null) return
 //               showNotificationu(beaconDetail)
            }
        }
    }


    val deviceName: String
        get() {
            val manufacturer = Build.MANUFACTURER
            val model = Build.MODEL
            return if (model.toLowerCase().startsWith(manufacturer.toLowerCase())) {
                capitalize(model)
            } else {
                capitalize(manufacturer) + " " + model
            }
        }
    //    public BluetoothDeviceReceiver(MainMenuActivity mainMenuActivity) {
    //
    //       // mainMenuActivity1 = mainMenuActivity;
    //    }

    @SuppressLint("Deprecation")
    override fun onReceive(context: Context, intent: Intent?) {
        mContext = context
        mBeaconNotificationList = ArrayList<BeaconListData>()

        AppBeaconManager(context)

        if (intent != null) {
            val bundle = intent.getBundleExtra(keys.BUNDLE_TO_BLUETOOTH_RECEVIER)
            if (bundle != null) {
            //   mUser = bundle.getParcelable(Key.BUNDLE_USER_KEY)
            }
        }

        val manager = context.getSystemService(
            Context.BLUETOOTH_SERVICE
        ) as BluetoothManager
        if (manager != null) {
            mAdapter = manager.adapter
            if (mAdapter != null) {
                mAdapter!!.startLeScan(mLeScannerCallback)
            }
        }
    }
    fun startBluetoothScan(context: Context) {
    //fun startBluetoothScan(context: Context, user: User) {
        val am = context.getSystemService(
            Context.ALARM_SERVICE
        ) as AlarmManager

        val intent = Intent(context, BluetoothDeviceReceiver::class.java)
        val bundle = Bundle()
     //   bundle.putParcelable(Key.BUNDLE_USER_KEY, user)
        intent.putExtra(keys.BUNDLE_TO_BLUETOOTH_RECEVIER, bundle)
        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK

        val pi = PendingIntent.getBroadcast(
            context,
            RequestCode.BLUETOOTH_SCAN_DEVICE,
            intent,
            PendingIntent.FLAG_ONE_SHOT
        )

        if (am != null) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                am.setExactAndAllowWhileIdle(
                    AlarmManager.RTC_WAKEUP,
                    System.currentTimeMillis() + Time.BLUETOOTH_START_SCAN_DELAY,
                    pi
                )
            } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                am.setExact(
                    AlarmManager.RTC_WAKEUP,
                    System.currentTimeMillis() + Time.BLUETOOTH_START_SCAN_DELAY,
                    pi
                )
            } else {
                am.set(
                    AlarmManager.RTC_WAKEUP,
                    System.currentTimeMillis() + Time.BLUETOOTH_START_SCAN_DELAY,
                    pi
                )
            }
        }
    }

//    private fun showNotificationu(beaconDetail: BeaconListData?) {
//        mBeaconDetail = beaconDetail
//
//        val sharedPrefManager = SharedPrefManager.getInstance(mContext)
//        val count = sharedPrefManager.getInt(Key.SHARED_PREF_NOTIFICATION_COUNT) + 1
//
//        if (count >= 6) {
//            if (mAdapter != null) {
//                mAdapter!!.stopLeScan(mLeScannerCallback)
//            }
//            return
//        }
//
//        sharedPrefManager.putInt(Key.SHARED_PREF_NOTIFICATION_COUNT, count)
//
//        val dataManager = DataManager.newInstance(mContext)
//        if (mUser == null) {
//            dataManager.getUserByStatus(object : IDatabaseCallback<User>() {
//                fun onSuccess(response: User) {
//                    mUser = response
//                    if (!foregrounded())
//                        if (!mBeaconDetail!!.campaigns!!.products!!.get(0).action_left_type.equals("Void")) {
//                            showNotification(mBeaconDetail!!)
//                        }
//                }
//
//                fun onError(throwable: Throwable) {
//                  //  Logger.e("GetUserByStatus : " + throwable.message)
//                }
//            })
//        } else {
//            if (!foregrounded())
//                if (!mBeaconDetail!!.campaigns!!.products!!.get(0).action_left_type.equals("Void")) {
//                    showNotification(mBeaconDetail!!)
//                }
//        }
//    }
//
//    private fun showNotification(beaconListData: BeaconListData) {
//        val notifyIntent = Intent(mContext, MainMenuActivity::class.java)
//        val bundle = Bundle()
//        bundle.putParcelable(Key.BUNDLE_BEACON_KEY, mBeaconDetail)
//        bundle.putParcelable(Key.BUNDLE_USER_KEY, mUser)
//        notifyIntent.putExtra(Key.BUNDLE_FROM_NOTIFICATION, bundle)
//
//        val pendingIntent = PendingIntent.getActivity(
//            mContext,
//            mBeaconDetail!!.hashCode(),
//            notifyIntent,
//            PendingIntent.FLAG_ONE_SHOT
//        )
//
//        val builder = NotificationCompat.Builder(
//            mContext!!,
//            CHANNEL_ID
//        )
//
//        val notification = builder.setSmallIcon(R.drawable.ic_notif_logo)
//            .setLargeIcon(
//                BitmapFactory.decodeResource(
//                    mContext!!.resources,
//                    R.mipmap.ic_launcher_2
//                )
//            )
//            .setContentTitle(mContext!!.getString(R.string.notification_beacon_title))
//            .setContentText(
//                "Hi " + mUser!!.getFirstName() + "! " + String.format(
//                    beaconListData.campaigns!!.notification!!,
//                    mUser!!.getFirstName()
//                )
//            )
//            .setChannelId(CHANNEL_ID)
//            .setAutoCancel(true)
//            .setContentIntent(pendingIntent)
//            .setPriority(2)
//            .setDefaults(NotificationCompat.DEFAULT_SOUND)
//            .build()
//
//        setAnalitics(beaconListData)
//
//        // notification.flags |= Notification.FLAG_FOREGROUND_SERVICE;
//        val notificationManager: NotificationManager?
//
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
//            val name = mContext!!.getString(R.string.notification)
//            val importance = NotificationManager.IMPORTANCE_HIGH
//            val channel = NotificationChannel(
//                CHANNEL_ID,
//                name,
//                importance
//            )
//            notificationManager = mContext!!.getSystemService(NotificationManager::class.java)
//            notificationManager?.createNotificationChannel(channel)
//        } else {
//            notificationManager = mContext!!.getSystemService(
//                Context.NOTIFICATION_SERVICE
//            ) as NotificationManager
//        }
//
//        notificationManager?.notify(mBeaconDetail!!.hashCode(), notification)
//
//    }
//
//
//    fun setAnalitics(beaconListData: BeaconListData) {
//
//        val mGson = GsonBuilder()
//            .setLenient()
//            .create()
//        val client = OkHttpClient.Builder()
//            .connectTimeout(Time.WEB_SERVICE_TIMEOUT, TimeUnit.SECONDS)
//            .writeTimeout(Time.WEB_SERVICE_TIMEOUT, TimeUnit.SECONDS)
//            .readTimeout(Time.WEB_SERVICE_TIMEOUT, TimeUnit.SECONDS)
//            .build()
//
//        val retrofit = Retrofit.Builder()
//            .client(client)
//            .baseUrl(mContext!!.getString(R.string.base_url))
//            .addConverterFactory(ScalarsConverterFactory.create())
//            .addConverterFactory(GsonConverterFactory.create(mGson))
//            .build()
//        val token = Prefs.getString(Key.USER_TOKEN, "0")
//        val beaconService = retrofit.create<BeaconService>(BeaconService::class.java!!)
//
//        val objectCall: Call<Any>
//        var osVersion: String? = Build.VERSION.RELEASE
//        if (osVersion == null) {
//            osVersion = "1"
//        }
//        var deviceName: String? = deviceName
//        if (deviceName == null) {
//            deviceName = "1"
//        }
//        val date = SimpleDateFormat("yyyy-MM-dd hh:mm:ss", Locale.getDefault()).format(Date())
//        val notifiedItemsList = ArrayList<NotifiedItems>()
//        val notifiedItems = NotifiedItems()
//        notifiedItems.beacon_id = beaconListData.id
//        notifiedItems.branch_id = beaconListData.branch_id
//        notifiedItems.date = date
//
//        notifiedItemsList.add(notifiedItems)
//
//
//        val item = Gson().toJson(notifiedItemsList)
//
//        // Prefs.putString(Key.BEACON_BRANCH_ANALYTICS,beaconListData.getBranch_id());
//        //Prefs.putString(Key.BEACON_ID_ANALYTICS,beaconListData.getId());
//
//        objectCall = beaconService.getNewImpressionNotified(
//            "Notified",
//            "Impression",
//            beaconListData.branch_id,
//            beaconListData.id,
//            "Android",
//            osVersion,
//            deviceName,
//            item,
//            "application/json",
//            "Bearer $token"
//        )
//
//        objectCall.enqueue(object : Callback<Any> {
//            override fun onResponse(call: Call<Any>, response: Response<Any>) {
//                try {
//                    if (response.errorBody() != null) {
//                        val errrorbody = response.errorBody()!!.string()
//                        Log.d("notif", "notified")
//                    }
//
//                    if (response.code() == 201) {
//
//                        Log.d("notif", "notified :) ")
//                    } else {
//                        Log.d("notif", "failed :( ")
//                    }
//
//                    Log.d("notif", date)
//                    Log.d("notif", item)
//                    Log.d("notif", response.message())
//
//                } catch (e: IOException) {
//                    e.printStackTrace()
//                }
//
//
//            }
//
//            override fun onFailure(call: Call<Any>, t: Throwable) {
//                Log.d("notif", "failed")
//            }
//        })
//    }

    fun capitalize(input: String): String {

        val words = input.toLowerCase().split(" ".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
        val builder = StringBuilder()
        for (i in words.indices) {
            val word = words[i]

            if (i > 0 && word.length > 0) {
                builder.append(" ")
            }

            val cap = word.substring(0, 1).toUpperCase() + word.substring(1)
            builder.append(cap)
        }
        return builder.toString()
    }

    companion object {

        private val CHANNEL_ID = "com.fourello.expee.android"
    }
}