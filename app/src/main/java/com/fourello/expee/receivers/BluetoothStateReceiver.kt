package com.fourello.expee.receivers

import android.bluetooth.BluetoothAdapter
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent

class BluetoothStateReceiver : BroadcastReceiver() {

//    private val mViewModel: BeaconViewModel
//
//    constructor() {}
//
//    constructor(viewModel: BeaconViewModel) {
//        mViewModel = viewModel
//    }

    override fun onReceive(context: Context, intent: Intent) {
        val action = intent.action ?: return
        if (action == BluetoothAdapter.ACTION_STATE_CHANGED) {
            val state = intent.getIntExtra(
                BluetoothAdapter.EXTRA_STATE,
                BluetoothAdapter.ERROR
            )
            when (state) {
               // BluetoothAdapter.STATE_OFF -> mViewModel.mBluetooth.set(false)
              //  BluetoothAdapter.STATE_ON -> mViewModel.mBluetooth.set(true)
            }
        }
    }
}