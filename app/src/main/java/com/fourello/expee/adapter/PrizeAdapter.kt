package com.fourello.expee.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.fourello.expee.R
import com.fourello.expee.activities.MainMenuActivity
import com.fourello.expee.constant.keys
import com.fourello.expee.fragmentdialog.RedeemDialogFragment
import com.fourello.expee.model.PrizeInfo
import com.pixplicity.easyprefs.library.Prefs

class PrizeAdapter constructor(listString : MutableList<PrizeInfo>,  searchCont :MainMenuActivity?
                           ) : RecyclerView.Adapter<PrizeAdapter.ItemViewHolder>() {


    var listExample: MutableList<PrizeInfo> = listString

    val mainMenuActivity : MainMenuActivity? = searchCont


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {
        val itemView = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_prize, parent, false)


        return PrizeAdapter.ItemViewHolder(itemView)
    }

    override fun getItemCount(): Int {
        return listExample.size
    }
    override fun onBindViewHolder(holder: PrizeAdapter.ItemViewHolder, position: Int) {

        var winCode = listExample[position].result
        var selectedGame = listExample[position].game!!.code
        if (selectedGame == "ariel_spin_the_wheel") {
            if(winCode == "1") {

                holder.prize_image.setImageDrawable(ContextCompat.getDrawable(mainMenuActivity!!.applicationContext,R.drawable.prize_ariel))

                holder.mPrizeLabel.text = mainMenuActivity.getString(R.string.grand_prize)
            }else if(winCode == "2"){
                holder.prize_image.setImageDrawable(ContextCompat.getDrawable(mainMenuActivity!!.applicationContext,R.drawable.prize_ariel_consol_one))

                holder.mPrizeLabel.text = mainMenuActivity.getString(R.string.consolation_prize)
            }else if(winCode == "3"){
                holder.prize_image.setImageDrawable(ContextCompat.getDrawable(mainMenuActivity!!.applicationContext,R.drawable.prize_ariel_consol_two))

                holder.mPrizeLabel.text = mainMenuActivity.getString(R.string.consolation_prize)
            }

            holder.mPrizeBrand.text = "Ariel"

        } else if (selectedGame == "pantene_spin_the_wheel") {

            if(winCode == "1") {

                holder.prize_image.setImageDrawable(ContextCompat.getDrawable(mainMenuActivity!!.applicationContext,R.drawable.prize_pantene))

                holder.mPrizeLabel.text = mainMenuActivity.getString(R.string.grand_prize)
            }else if(winCode == "2"){
                holder.prize_image.setImageDrawable(ContextCompat.getDrawable(mainMenuActivity!!.applicationContext,R.drawable.prize_pantene_consol_one))

                holder.mPrizeLabel.text = mainMenuActivity.getString(R.string.consolation_prize)
            }else if(winCode == "3"){
                holder.prize_image.setImageDrawable(ContextCompat.getDrawable(mainMenuActivity!!.applicationContext,R.drawable.prize_pantene_consol_two))

                holder.mPrizeLabel.text = mainMenuActivity.getString(R.string.consolation_prize)
            }

            holder.mPrizeBrand.text = "Pantene"

        } else if (selectedGame == "lucky_me_spin_the_wheel") {
            if(winCode == "1") {

                holder.prize_image.setImageDrawable(ContextCompat.getDrawable(mainMenuActivity!!.applicationContext,R.drawable.prize_luckyme))

                holder.mPrizeLabel.text = mainMenuActivity.getString(R.string.grand_prize)
            }else if(winCode == "2"){
                holder.prize_image.setImageDrawable(ContextCompat.getDrawable(mainMenuActivity!!.applicationContext,R.drawable.prize_luckyme_consol_one))

                holder.mPrizeLabel.text = mainMenuActivity.getString(R.string.consolation_prize)
            }else if(winCode == "3"){
                holder.prize_image.setImageDrawable(ContextCompat.getDrawable(mainMenuActivity!!.applicationContext,R.drawable.prize_luckyme_consol_two))

                holder.mPrizeLabel.text = mainMenuActivity.getString(R.string.consolation_prize)
            }
            holder.mPrizeBrand.text = "Lucky Me!"

        }


//        holder.mPrizeLabel.text = listExample[position].result + " " + listExample[position].status
//        holder.mPrizeBrand.text = listExample[position].game!!.code

        holder.itemView.setOnClickListener {

            val ft =  mainMenuActivity!!.supportFragmentManager.beginTransaction()
            val dialogFragment = RedeemDialogFragment( mainMenuActivity,listExample[position])
            dialogFragment.isCancelable = false
            dialogFragment.show(ft, "redeem")
            Prefs.putBoolean(keys.SHOWINGDIALOG,true)
        }

        holder.mCardView.setOnClickListener {
            val ft =  mainMenuActivity!!.supportFragmentManager.beginTransaction()
            val dialogFragment = RedeemDialogFragment( mainMenuActivity,listExample[position])
            dialogFragment.isCancelable = false
            dialogFragment.show(ft, "redeem")
            Prefs.putBoolean(keys.SHOWINGDIALOG,true)
        }
    }

    class ItemViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {


        var prize_image: ImageView = itemView.findViewById(R.id.prize_photo)
        var mPrizeLabel : TextView = itemView.findViewById(R.id.prize_level)
        var mPrizeBrand : TextView = itemView.findViewById(R.id.prize_brand)
        var mCardView : CardView = itemView.findViewById(R.id.prize_card)
    }





}