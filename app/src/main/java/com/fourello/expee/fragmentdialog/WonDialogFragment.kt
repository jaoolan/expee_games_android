package com.fourello.expee.fragmentdialog


import android.graphics.Color
import android.os.Bundle
import android.view.*
import androidx.core.content.ContextCompat
import androidx.fragment.app.DialogFragment

import com.fourello.expee.R
import com.fourello.expee.activities.GameActivity
import com.fourello.expee.constant.keys
import com.pixplicity.easyprefs.library.Prefs
import kotlinx.android.synthetic.main.fragment_won_dialog.view.*
import nl.dionsegijn.konfetti.models.Shape
import nl.dionsegijn.konfetti.models.Size


class WonDialogFragment  constructor(selectedGame : String,winCode : String,gameAct : GameActivity?): DialogFragment() {
    val gameActivity = gameAct
    val selectedGame =selectedGame
    val winCode = winCode
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val realView = inflater.inflate(R.layout.fragment_won_dialog, container, false)

        realView. viewKonfetti.build()
            .addColors(Color.YELLOW, Color.RED, Color.MAGENTA)
            .setDirection(0.0, 359.0)
            .setSpeed(1f, 5f)
            .setFadeOutEnabled(true)
            .setTimeToLive(2000L)
            .addShapes(Shape.RECT, Shape.CIRCLE)
            .addSizes(Size(12))
            .setPosition(-50f, realView. viewKonfetti.width + 50f, -100f, -50f)
            .streamFor(300, 10000L)




        if (selectedGame == "ariel") {
            if(winCode == "1") {

                realView.prize_image.setImageDrawable(ContextCompat.getDrawable(context!!,R.drawable.prize_ariel))

                realView.prize_title.text = getString(R.string.ariel_grand_prize)
            }else if(winCode == "2"){
                realView.prize_image.setImageDrawable(ContextCompat.getDrawable(context!!,R.drawable.prize_ariel_consol_one))

                realView.prize_title.text = getString(R.string.ariel_consol_one_prize)
            }else if(winCode == "3"){
                realView.prize_image.setImageDrawable(ContextCompat.getDrawable(context!!,R.drawable.prize_ariel_consol_two))

                realView.prize_title.text = getString(R.string.ariel_consol_two_prize)
            }



        } else if (selectedGame == "pantene") {

            if(winCode == "1") {

                realView.prize_image.setImageDrawable(ContextCompat.getDrawable(context!!,R.drawable.prize_pantene))

                realView.prize_title.text = getString(R.string.pantene_grand_prize)
            }else if(winCode == "2"){
                realView.prize_image.setImageDrawable(ContextCompat.getDrawable(context!!,R.drawable.prize_pantene_consol_one))

                realView.prize_title.text = getString(R.string.pantene_consol_one_prize)
            }else if(winCode == "3"){
                realView.prize_image.setImageDrawable(ContextCompat.getDrawable(context!!,R.drawable.prize_pantene_consol_two))

                realView.prize_title.text = getString(R.string.pantene_consol_two_prize)
            }


        } else if (selectedGame == "lucky") {
            if(winCode == "1") {

                realView.prize_image.setImageDrawable(ContextCompat.getDrawable(context!!,R.drawable.prize_luckyme))

                realView.prize_title.text = getString(R.string.luckyme_grand_prize)
            }else if(winCode == "2"){
                realView.prize_image.setImageDrawable(ContextCompat.getDrawable(context!!,R.drawable.prize_luckyme_consol_one))

                realView.prize_title.text = getString(R.string.luckyme_consol_one_prize)
            }else if(winCode == "3"){
                realView.prize_image.setImageDrawable(ContextCompat.getDrawable(context!!,R.drawable.prize_luckyme_consol_two))

                realView.prize_title.text = getString(R.string.luckyme_consol_two_prize)
            }


        }


        dialog!!.window!!.requestFeature(Window.FEATURE_NO_TITLE)
        dialog!!.window!!.setBackgroundDrawableResource(R.drawable.shape_rounded_corners_white_more_radius)
        dialog!!.window!!.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT)


        realView.btn_go_to_redeem.setOnClickListener {

            Prefs.putBoolean(keys.TO_REDEEM,true)
            Prefs.putBoolean(keys.SHOWINGDIALOG,false)
            gameActivity!!.finish()
            dismiss()

        }

        return realView
    }


}
