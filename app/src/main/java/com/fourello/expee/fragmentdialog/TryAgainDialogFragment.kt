package com.fourello.expee.fragmentdialog


import android.os.Bundle
import android.view.*
import androidx.fragment.app.DialogFragment

import com.fourello.expee.R
import com.fourello.expee.activities.GameActivity
import com.fourello.expee.constant.keys
import com.pixplicity.easyprefs.library.Prefs
import kotlinx.android.synthetic.main.fragment_try_again_dialog.view.*


class TryAgainDialogFragment constructor(gameAct : GameActivity): DialogFragment() {
    val gameActivity = gameAct;
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
       val realView = inflater.inflate(R.layout.fragment_try_again_dialog, container, false)
        dialog!!.window!!.requestFeature(Window.FEATURE_NO_TITLE)
        dialog!!.window!!.setBackgroundDrawableResource(R.drawable.shape_rounded_corners_white_more_radius)
        dialog!!.window!!.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT)

        realView.btn_try_again.setOnClickListener {
            gameActivity.finish()
            Prefs.putBoolean(keys.SHOWINGDIALOG,false)
            dismiss()

        }

    return realView
    }


}
