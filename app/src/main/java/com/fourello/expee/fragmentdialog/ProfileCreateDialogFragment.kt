package com.fourello.expee.fragmentdialog


import android.os.Bundle
import android.util.Log
import android.view.*
import androidx.appcompat.app.AlertDialog
import androidx.core.content.ContextCompat
import androidx.fragment.app.DialogFragment

import com.fourello.expee.R
import com.fourello.expee.activities.MainMenuActivity

import com.fourello.expee.constant.keys
import com.fourello.expee.model.*
import com.fourello.expee.webservice.UserService
import com.google.gson.GsonBuilder
import com.pixplicity.easyprefs.library.Prefs
import kotlinx.android.synthetic.main.fragment_profile_create_dialog.view.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class ProfileCreateDialogFragment constructor(mainAct : MainMenuActivity): DialogFragment() {
    val mGson = GsonBuilder()
        .setLenient()
        .create()
    var genderSelected = ""
    val mainMenuActivity = mainAct
    var ageSelected = ""
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val realView = inflater.inflate(R.layout.fragment_profile_create_dialog, container, false)

        dialog!!.window!!.requestFeature(Window.FEATURE_NO_TITLE)
        dialog!!.window!!.setBackgroundDrawableResource(R.drawable.shape_rounded_corners_white_more_radius)
        dialog!!.window!!.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT)

        val userInfoString = Prefs.getString(keys.USER_FULL_DATA, "")

        val userProfileData: UserProfileData = mGson.fromJson(userInfoString, UserProfileData::class.java)



        realView.btn_male.setOnClickListener {

            realView.btn_male.alpha = 1f
            realView.btn_female.alpha = .5f
            realView.btn_other.alpha = .5f
            genderSelected = "male"
        }
        realView.btn_female.setOnClickListener {

            realView.btn_male.alpha = .5f
            realView.btn_female.alpha = 1f
            realView.btn_other.alpha = .5f
            genderSelected = "female"
        }

        realView.btn_other.setOnClickListener {

            realView.btn_male.alpha = .5f
            realView.btn_female.alpha = .5f
            realView.btn_other.alpha = 1f

            genderSelected = "not specified"
        }


        realView.btn_age_one.setOnClickListener {

            realView.btn_age_one.backgroundTintList = ContextCompat.getColorStateList(activity!!, R.color.btn_blue)
            realView.btn_age_two.backgroundTintList = ContextCompat.getColorStateList(activity!!, R.color.gray_age)
            realView.btn_age_tree.backgroundTintList = ContextCompat.getColorStateList(activity!!, R.color.gray_age)
            realView.btn_age_four.backgroundTintList = ContextCompat.getColorStateList(activity!!, R.color.gray_age)
            realView.btn_age_five.backgroundTintList = ContextCompat.getColorStateList(activity!!, R.color.gray_age)
            ageSelected = "18-24"
        }

        realView.btn_age_two.setOnClickListener {
            realView.btn_age_one.backgroundTintList = ContextCompat.getColorStateList(activity!!, R.color.gray_age)
            realView.btn_age_two.backgroundTintList = ContextCompat.getColorStateList(activity!!, R.color.btn_blue)
            realView.btn_age_tree.backgroundTintList = ContextCompat.getColorStateList(activity!!, R.color.gray_age)
            realView.btn_age_four.backgroundTintList = ContextCompat.getColorStateList(activity!!, R.color.gray_age)
            realView.btn_age_five.backgroundTintList = ContextCompat.getColorStateList(activity!!, R.color.gray_age)
            ageSelected = "25-39"
        }

        realView.btn_age_tree.setOnClickListener {
            realView.btn_age_one.backgroundTintList = ContextCompat.getColorStateList(activity!!, R.color.gray_age)
            realView.btn_age_two.backgroundTintList = ContextCompat.getColorStateList(activity!!, R.color.gray_age)
            realView.btn_age_tree.backgroundTintList = ContextCompat.getColorStateList(activity!!, R.color.btn_blue)
            realView.btn_age_four.backgroundTintList = ContextCompat.getColorStateList(activity!!, R.color.gray_age)
            realView.btn_age_five.backgroundTintList = ContextCompat.getColorStateList(activity!!, R.color.gray_age)
            ageSelected = "40-54"
        }

        realView.btn_age_four.setOnClickListener {
            realView.btn_age_one.backgroundTintList = ContextCompat.getColorStateList(activity!!, R.color.gray_age)
            realView.btn_age_two.backgroundTintList = ContextCompat.getColorStateList(activity!!, R.color.gray_age)
            realView.btn_age_tree.backgroundTintList = ContextCompat.getColorStateList(activity!!, R.color.gray_age)
            realView.btn_age_four.backgroundTintList = ContextCompat.getColorStateList(activity!!, R.color.btn_blue)
            realView.btn_age_five.backgroundTintList = ContextCompat.getColorStateList(activity!!, R.color.gray_age)
            ageSelected = "55-74"
        }
        realView.btn_age_five.setOnClickListener {
            realView.btn_age_one.backgroundTintList = ContextCompat.getColorStateList(activity!!, R.color.gray_age)
            realView.btn_age_two.backgroundTintList = ContextCompat.getColorStateList(activity!!, R.color.gray_age)
            realView.btn_age_tree.backgroundTintList = ContextCompat.getColorStateList(activity!!, R.color.gray_age)
            realView.btn_age_four.backgroundTintList = ContextCompat.getColorStateList(activity!!, R.color.gray_age)
            realView.btn_age_five.backgroundTintList = ContextCompat.getColorStateList(activity!!, R.color.btn_blue)
            ageSelected = "74 & above"
        }

        realView.btn_update.setOnClickListener {

            if (genderSelected.trim() != "") {
                if (ageSelected.trim() != "") {
                    updateProfile(userProfileData.id!!,ageSelected, genderSelected,realView)
                } else {

                    displayDialog("Please select your age range.")
                }

            } else {

                displayDialog("Please select your gender.")
            }
        }

        realView.close_profile.setOnClickListener {

            dismiss()

        }

        return realView
    }


    fun updateProfile(userid : String,age: String, gender: String,realView : View) {
        realView.progress_layout_profile.visibility = View.VISIBLE
        var token: String = Prefs.getString(keys.USER_TOKEN, "");
        val apiService = UserService.create(this.getString(R.string.base_url))


        val call = apiService.createProfile(
            userid, gender, age, "PUT", "application/json",
            "Bearer $token"
        )

        call.enqueue(object : Callback<UpdateCreditsResponse> {

            override fun onResponse(call: Call<UpdateCreditsResponse>, response: Response<UpdateCreditsResponse>) {

                if (response.code() == 200) {
                    Prefs.putString(keys.USER_FULL_DATA, mGson.toJson(response.body()!!.userProfileData))

                    val builder = AlertDialog.Builder(activity!!)
                    builder.setMessage("Profile created.")
                    builder.setPositiveButton("Ok") { dialog, _ ->

                        val ft = mainMenuActivity.supportFragmentManager.beginTransaction()
                        val dialogFragment = WelcomeDialogFragment()
                        dialogFragment.isCancelable = false
                        dialogFragment.show(ft, "redeem")

                        dismiss()
                        dialog.dismiss()
                    }
                    val dialog: AlertDialog = builder.create()
                    dialog.show()
                }

                realView.progress_layout_profile.visibility = View.GONE
            }

            override fun onFailure(call: Call<UpdateCreditsResponse>, t: Throwable) {

                Log.e("aa", t.toString());
                realView.progress_layout_profile.visibility = View.GONE

            }

        })
    }

    fun displayDialog(message: String) {
        val builder = AlertDialog.Builder(activity!!)
        builder.setMessage(message)
        builder.setPositiveButton("Ok") { dialog, _ ->
            dialog.dismiss()
        }
        val dialog: AlertDialog = builder.create()
        dialog.show()
    }
}
