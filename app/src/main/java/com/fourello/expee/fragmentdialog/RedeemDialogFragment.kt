package com.fourello.expee.fragmentdialog


import android.os.Bundle
import android.view.*
import androidx.core.content.ContextCompat
import androidx.fragment.app.DialogFragment

import com.fourello.expee.R
import com.fourello.expee.activities.MainMenuActivity
import com.fourello.expee.constant.keys
import com.fourello.expee.model.PrizeInfo
import com.pixplicity.easyprefs.library.Prefs
import kotlinx.android.synthetic.main.fragment_redeem_dialog.view.*
import kotlinx.android.synthetic.main.fragment_redeem_dialog.view.prize_image
import kotlinx.android.synthetic.main.fragment_redeem_dialog.view.prize_title


class RedeemDialogFragment constructor(mainAct : MainMenuActivity, prizeInfo : PrizeInfo) : DialogFragment() {
    val mainAct = mainAct
    val prizeInfo = prizeInfo
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val realView = inflater.inflate(R.layout.fragment_redeem_dialog, container, false)

        dialog!!.window!!.requestFeature(Window.FEATURE_NO_TITLE)
        dialog!!.window!!.setBackgroundDrawableResource(R.drawable.shape_rounded_corners_white_more_radius)
        dialog!!.window!!.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT)


        val selectedGame = prizeInfo.game!!.code
        val winCode : String = prizeInfo.result!!

        if (selectedGame == "ariel_spin_the_wheel") {
            if(winCode == "1") {

                realView.prize_image.setImageDrawable(ContextCompat.getDrawable(context!!,R.drawable.prize_ariel))
                realView.prize_title.text = getString(R.string.ariel_grand_prize)
                realView.prize_label.text = mainAct.getString(R.string.grand_prize)
            }else if(winCode == "2"){
                realView.prize_image.setImageDrawable(ContextCompat.getDrawable(context!!,R.drawable.prize_ariel_consol_one))
                realView.prize_label.text = mainAct.getString(R.string.consolation_prize)
                realView.prize_title.text = getString(R.string.ariel_consol_one_prize)
            }else if(winCode == "3"){
                realView.prize_image.setImageDrawable(ContextCompat.getDrawable(context!!,R.drawable.prize_ariel_consol_two))
                realView.prize_label.text = mainAct.getString(R.string.consolation_prize)
                realView.prize_title.text = getString(R.string.ariel_consol_two_prize)
            }

            realView.prize_brand.text = "Ariel"


        } else if (selectedGame == "pantene_spin_the_wheel") {

            if(winCode == "1") {

                realView.prize_image.setImageDrawable(ContextCompat.getDrawable(context!!,R.drawable.prize_pantene))
                realView.prize_label.text = mainAct.getString(R.string.grand_prize)
                realView.prize_title.text = getString(R.string.pantene_grand_prize)
            }else if(winCode == "2"){
                realView.prize_image.setImageDrawable(ContextCompat.getDrawable(context!!,R.drawable.prize_pantene_consol_one))
                realView.prize_label.text = mainAct.getString(R.string.consolation_prize)
                realView.prize_title.text = getString(R.string.pantene_consol_one_prize)
            }else if(winCode == "3"){
                realView.prize_image.setImageDrawable(ContextCompat.getDrawable(context!!,R.drawable.prize_pantene_consol_two))
                realView.prize_label.text = mainAct.getString(R.string.consolation_prize)
                realView.prize_title.text = getString(R.string.pantene_consol_two_prize)
            }
            realView.prize_brand.text = "Pantene"

        } else if (selectedGame == "lucky_me_spin_the_wheel") {
            if(winCode == "1") {

                realView.prize_image.setImageDrawable(ContextCompat.getDrawable(context!!,R.drawable.prize_luckyme))
                realView.prize_label.text = mainAct.getString(R.string.grand_prize)
                realView.prize_title.text = getString(R.string.luckyme_grand_prize)
            }else if(winCode == "2"){
                realView.prize_image.setImageDrawable(ContextCompat.getDrawable(context!!,R.drawable.prize_luckyme_consol_one))
                realView.prize_label.text = mainAct.getString(R.string.consolation_prize)
                realView.prize_title.text = getString(R.string.luckyme_consol_one_prize)
            }else if(winCode == "3"){
                realView.prize_image.setImageDrawable(ContextCompat.getDrawable(context!!,R.drawable.prize_luckyme_consol_two))
                realView.prize_label.text = mainAct.getString(R.string.consolation_prize)
                realView.prize_title.text = getString(R.string.luckyme_consol_two_prize)
            }
            realView.prize_brand.text = "Lucky Me!"

        }


        realView.close_redeem.setOnClickListener {
            Prefs.putBoolean(keys.SHOWINGDIALOG,false)
            dismiss()

        }

        realView.btn_claim_prize.setOnClickListener {


            val ft = mainAct.supportFragmentManager.beginTransaction()
            val dialogFragment = ClaimPinDialogFragment(mainAct,prizeInfo,realView.prize_title.text.toString())
            dialogFragment.isCancelable = false
            dialogFragment.show(ft, "redeem")
            dismiss()
//            val intent = Intent(activity, RedeemActivity::class.java)
//            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
//            activity!!.startActivity(intent)
//            activity!!.overridePendingTransition(
//                R.anim.enterfrom,
//                R.anim.enterto
//            )
//            dismiss()

        }

        return realView
    }


}
