package com.fourello.expee.fragmentdialog


import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.*
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
import androidx.fragment.app.DialogFragment

import com.fourello.expee.R
import com.fourello.expee.activities.GameActivity
import com.fourello.expee.activities.MainMenuActivity
import com.fourello.expee.constant.keys
import com.fourello.expee.webservice.UserService
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.pixplicity.easyprefs.library.Prefs
import kotlinx.android.synthetic.main.fragment_redeem_credits_dialog.view.*
import retrofit2.Call
import com.fourello.expee.model.*
import kotlinx.android.synthetic.main.fragment_redeem_credits_dialog.*
import kotlinx.android.synthetic.main.fragment_redeem_credits_dialog.view.code_two
import okhttp3.OkHttpClient
import org.json.JSONObject
import java.util.concurrent.TimeUnit


class RedeemCreditsDialogFragment constructor(mainAct: MainMenuActivity) : DialogFragment() {

    val mainAct = mainAct
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val realView = inflater.inflate(com.fourello.expee.R.layout.fragment_redeem_credits_dialog, container, false)


        dialog!!.window!!.requestFeature(Window.FEATURE_NO_TITLE)
        dialog!!.window!!.setBackgroundDrawableResource(com.fourello.expee.R.drawable.shape_rounded_corners_white_more_radius)
        dialog!!.window!!.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT)

        realView.close_redeem_credits.setOnClickListener {
            Prefs.putBoolean(keys.SHOWINGDIALOG, false)
            dismiss()

        }


        realView.code_one.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {}
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            enableBtnGame(realView)
            }
        })
        realView.code_two.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {}
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                enableBtnGame(realView)
            }
        })

        realView.code_tree.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {}
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                enableBtnGame(realView)
            }
        })

        realView.code_four.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {}
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                enableBtnGame(realView)
            }
        })

        realView.code_five.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {}
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                enableBtnGame(realView)
            }
        })
        realView.code_six.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {}
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                enableBtnGame(realView)
            }
        })

        //code format xxxxx-xx-xxxx-xx-xxx-xx

        realView.btn_game_code.setOnClickListener {
            realView.progress_layout_redeem.visibility = View.VISIBLE
            enterGameCode(realView.code_one.text.toString() + "-" +
                    realView.code_two.text.toString() + "-" +
                    realView.code_tree.text.toString() + "-" +
                    realView.code_four.text.toString() + "-" +
                    realView.code_five.text.toString() + "-" +
                    realView.code_six.text.toString() , realView)
            realView.edt_game_code.hideKeyboard()
        }

        realView.code_one.addTextChangedListener(object : TextWatcher{
            override fun afterTextChanged(p0: Editable?) {}
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                if(realView.code_one.length() == 5){
                    code_two.requestFocus()
                }
          }
        })
        realView.code_two.addTextChangedListener(object : TextWatcher{
            override fun afterTextChanged(p0: Editable?) {}
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                if(realView.code_two.length() == 2){
                    code_tree.requestFocus()
                }
            }
        })
        realView.code_tree.addTextChangedListener(object : TextWatcher{
            override fun afterTextChanged(p0: Editable?) {}
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                if(realView.code_tree.length() == 4){
                    code_four.requestFocus()
                }
            }
        })
        realView.code_four.addTextChangedListener(object : TextWatcher{
            override fun afterTextChanged(p0: Editable?) {}
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                if(realView.code_four.length() == 2){
                    code_five.requestFocus()
                }
            }
        })

        realView.code_five.addTextChangedListener(object : TextWatcher{
            override fun afterTextChanged(p0: Editable?) {}
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                if(realView.code_five.length() == 3){
                    code_six.requestFocus()
                }
            }
        })


        realView.code_two.setOnKeyListener { view, i, keyEvent ->
            if(i == KeyEvent.KEYCODE_DEL){
                if(code_two.text.toString().isEmpty()){
                    code_one.requestFocus(code_one.length())
                }
            }
            false }
        realView.code_tree.setOnKeyListener { view, i, keyEvent ->
            if(i == KeyEvent.KEYCODE_DEL){
                if(code_tree.text.toString().isEmpty()){
                    code_two.requestFocus(code_two.length())
                }
            }
            false }
        realView.code_four.setOnKeyListener { view, i, keyEvent ->
            if(i == KeyEvent.KEYCODE_DEL){
                if(code_four.text.toString().isEmpty()){
                    code_tree.requestFocus(code_tree.length())
                }
            }
            false }

        realView.code_five.setOnKeyListener { view, i, keyEvent ->
            if(i == KeyEvent.KEYCODE_DEL){
                if(code_five.text.toString().isEmpty()){
                    code_four.requestFocus(code_four.length())
                }
            }
            false }

        realView.code_six.setOnKeyListener { view, i, keyEvent ->
            if(i == KeyEvent.KEYCODE_DEL){
                if(code_six.text.toString().isEmpty()){
                    code_five.requestFocus(code_five.length())
                }
            }
            false }


        return realView
    }


    fun enableBtnGame(realView: View){

        if ((realView.code_one.text.isNotEmpty()) and (realView.code_two.text.isNotEmpty())
            and (realView.code_two.text.isNotEmpty()) and (realView.code_two.text.isNotEmpty())
            and (realView.code_five.text.isNotEmpty()) and (realView.code_six.text.isNotEmpty())) {
            realView.btn_game_code.alpha = 1f
            realView.btn_game_code.isEnabled = true
        } else {

            realView.btn_game_code.alpha = .5f
            realView.btn_game_code.isEnabled = false
        }

    }


    // Redeem game credits API
    fun enterGameCode(gameCode: String, realView: View) {
        val mGson = GsonBuilder()
            .setLenient()
            .create()
        val token = Prefs.getString(keys.USER_TOKEN, "")
        val apiService = UserService.create(this.getString(com.fourello.expee.R.string.base_url))
        val callService = apiService.enterGameCode(
            gameCode,
            "application/json", "Bearer $token")

        callService.enqueue(object : retrofit2.Callback<Object> {
            override fun onResponse(
                call: Call<Object>,
                response: retrofit2.Response<Object>?
            ) {

                if (response != null) {

                    if (response.code() == 200) {

                        val a_json_string = mGson.toJson(response!!.body())
                       val RedeemCodeResponse = Gson().fromJson(a_json_string, RedeemCodeResponse::class.java)

                        val redeemCreditsGames = RedeemCodeResponse.userProfileData!!.credits!!.games!!
                        var currStrAriel = 0
                        var currStrPantene = 0
                        var currStrLucky = 0
                        if (redeemCreditsGames != null) {

                            if (redeemCreditsGames.isNotEmpty()) {

                                for (gameCreds: RedeemCodeGames in redeemCreditsGames) {
                                    if (gameCreds.code == getString(com.fourello.expee.R.string.ariel_spin)) {
                                        var currStr = Prefs.getString(keys.CREDITS_ARIEL, "0")
                                        var currInt = currStr.toInt() + gameCreds.count!!
                                        Prefs.putString(keys.CREDITS_ARIEL, currInt.toString())

                                        currStrAriel = gameCreds.count!!

                                    } else if (gameCreds.code == getString(com.fourello.expee.R.string.pantene_spin)) {
                                        var currStr = Prefs.getString(keys.CREDITS_PANTENE, "0")
                                        var currInt = currStr.toInt() + gameCreds.count!!

                                        Prefs.putString(keys.CREDITS_PANTENE, currInt.toString())

                                        currStrPantene = gameCreds.count!!

                                    } else if (gameCreds.code == getString(com.fourello.expee.R.string.lucky_me_spin)) {

                                        var currStr = Prefs.getString(keys.CREDITS_LUCKYME, "0")
                                        var currInt = currStr.toInt() + gameCreds.count!!
                                        Prefs.putString(keys.CREDITS_LUCKYME, currInt.toString())
                                        currStrLucky = gameCreds.count!!

                                    }
                                }


                            }

                        }

                        var ffGames = ""
                        if(currStrAriel != 0) {

                            if(currStrAriel == 1) {
                                ffGames = "Ariel’s Spin 2 Win: " + currStrAriel.toString() + " credit\n"
                            }else{
                                ffGames = "Ariel’s Spin 2 Win: " + currStrAriel.toString() + " credits\n"
                            }
                        }
                        if(currStrPantene != 0 ){
                            if(currStrPantene == 1) {
                                ffGames = ffGames + "Pantene’s Spin 2 Win: " + currStrPantene.toString() + " credit\n"
                            }else{
                                ffGames = ffGames + "Pantene’s Spin 2 Win: " + currStrPantene.toString() + " credits\n"
                            }
                        }
                        if(currStrLucky != 0){

                            if(currStrLucky == 1) {
                                ffGames = ffGames + "Lucky Me’s Spin 2 Win: " + currStrLucky.toString() + " credit"
                            }else{

                                ffGames = ffGames + "Lucky Me’s Spin 2 Win: " + currStrLucky.toString() + " credits"
                            }

                        }


                        if((currStrAriel == 0) and (currStrPantene == 0) and (currStrLucky == 0)){

                            val builder = AlertDialog.Builder(activity!!)
                            builder.setTitle("Message")
                            builder.setMessage("Sorry, your purchase didn’t meet the minimum requirement.")
                            builder.setPositiveButton("Ok") { dialog, _ ->
                              //  dismiss()

                                dialog.dismiss()
                            }
                            val dialog: AlertDialog = builder.create()
                            dialog.show()
                            emptyCodes(realView)
                        }else{

                            val builder = AlertDialog.Builder(activity!!)
                            builder.setTitle("Valid Game Code")
                            builder.setMessage("Congrats! You have earned game credits on the ff games: \n\n"  + ffGames)
                            builder.setPositiveButton("Ok") { dialog, _ ->
                                dismiss()
                                Prefs.putBoolean(keys.SHOWINGDIALOG, false)
                                dialog.dismiss()
                            }
                            val dialog: AlertDialog = builder.create()
                            dialog.show()
                        }



                        realView.edt_game_code.setText("")
                        mainAct.updateCreditsCount()

                    } else if (response.code() == 404) {
                        Toast.makeText(context, "Not Found", Toast.LENGTH_SHORT).show()
                    } else {
                        //Toast.makeText(context, "Server Error", Toast.LENGTH_SHORT).show()
                        realView.edt_game_code.setText("")

                        try {
                            val jObjError = JSONObject(response.errorBody()!!.string())
                         //   Toast.makeText(context, jObjError.getString("errors"), Toast.LENGTH_LONG).show()



                            if(jObjError.getString("errors") == "Code is already used") {
                                val builder = AlertDialog.Builder(activity!!)
                                builder.setTitle("Code Used")
                                builder.setMessage("Sorry, the code you have entered has already been used.")
                                builder.setPositiveButton("Ok") { dialog, _ ->
                                    dialog.dismiss()
                                }
                                val dialog: AlertDialog = builder.create()
                                dialog.show()
                                emptyCodes(realView)
                            }else {

                                val builder = AlertDialog.Builder(activity!!)
                                builder.setTitle("Invalid Code")
                                builder.setMessage("Sorry, the code you have entered is invalid.")
                                builder.setPositiveButton("Ok") { dialog, _ ->
                                    dialog.dismiss()
                                }
                                val dialog: AlertDialog = builder.create()
                                dialog.show()

                                emptyCodes(realView)
                            }

                        } catch (e: Exception) {
                            Toast.makeText(context, e.message, Toast.LENGTH_LONG).show()
                        }


                        //
                    }
                }
                realView.progress_layout_redeem.visibility = View.GONE
            }

            override fun onFailure(call: Call<Object>, t: Throwable) {
                Log.e("aa", t.toString());
                displayDialog("Server Error, Please try again.")
                Toast.makeText(context, "Server Error", Toast.LENGTH_SHORT).show()
                realView.progress_layout_redeem.visibility = View.GONE
            }
        })

    }


    fun emptyCodes(realView: View)
    {
        realView.code_one.setText("")
        realView.code_two.setText("")
        realView.code_tree.setText("")
        realView.code_four.setText("")
        realView.code_five.setText("")
        realView.code_six.setText("")
        realView.code_one.requestFocus()


        }


    fun displayDialog(message: String) {
        val builder = AlertDialog.Builder(activity!!)
        builder.setMessage(message)
        builder.setPositiveButton("Ok") { dialog, _ ->
            dialog.dismiss()
        }
        val dialog: AlertDialog = builder.create()
        dialog.show()
    }

    fun View.hideKeyboard() {
        val imm = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(windowToken, 0)
    }
}
