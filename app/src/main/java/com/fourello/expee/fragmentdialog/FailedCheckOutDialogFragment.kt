package com.fourello.expee.fragmentdialog


import android.os.Bundle
import android.view.*
import androidx.fragment.app.DialogFragment

import com.fourello.expee.R
import com.fourello.expee.activities.ScannerActivity
import kotlinx.android.synthetic.main.fragment_failed_check_out_dialog.view.*


class FailedCheckOutDialogFragment constructor(message: String, scanAct : ScannerActivity) : DialogFragment() {
    val mess = message
    val scannerActivity = scanAct
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
      val realView =  inflater.inflate(R.layout.fragment_failed_check_out_dialog, container, false)
        dialog!!.window!!.requestFeature(Window.FEATURE_NO_TITLE)
        dialog!!.window!!.setBackgroundDrawableResource(R.drawable.shape_rounded_corners_white_more_radius)
        dialog!!.window!!.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT)


        realView.failed_mess.text = mess

        realView.btn_failed.setOnClickListener {

            scannerActivity.finish()
            dismiss()
        }

  return realView
    }


}
