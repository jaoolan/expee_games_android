package com.fourello.expee.fragmentdialog


import android.graphics.drawable.Drawable
import android.os.Bundle
import android.os.CountDownTimer
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import androidx.core.content.ContextCompat
import androidx.fragment.app.DialogFragment
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import com.fourello.expee.GlideApp

import com.fourello.expee.R
import com.fourello.expee.activities.MainMenuActivity
import com.fourello.expee_kotlin.model.BeaconListData
import kotlinx.android.synthetic.main.fragment_story_beacon_dialog.view.*

class StoryBeaconDialogFragment constructor(mainAct : MainMenuActivity, beacondetail : BeaconListData) : DialogFragment() {

    private val myProfileList = intArrayOf(
        com.fourello.expee.R.drawable.pantene_logo, com.fourello.expee.R.drawable.story_two_thumb,
        com.fourello.expee.R.drawable.story_tree_thumb)
    val mainAct = mainAct
    val beacondetail = beacondetail

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
      //  dialog!!.window!!.attributes.windowAnimations = R.style.DialogAnimation;
        val realView = inflater.inflate(R.layout.fragment_story_beacon_dialog, container, false)


        realView.brand_name.text = beacondetail.campaigns!!.name

        if(beacondetail.campaigns!!.name!!.toLowerCase().trim().contains("pantene")){
            realView.brand_logo.setImageDrawable(ContextCompat.getDrawable(context!!,myProfileList[0]))

        }else if(beacondetail.campaigns!!.name!!.toLowerCase().trim().contains("ariel")){
            realView.brand_logo.setImageDrawable(ContextCompat.getDrawable(context!!,myProfileList[1]))

        }else if(beacondetail.campaigns!!.name!!.toLowerCase().trim().contains("lucky")){

            realView.brand_logo.setImageDrawable(ContextCompat.getDrawable(context!!,myProfileList[2]))

        }

        var mCountDownTimer: CountDownTimer? = null
        GlideApp.with(activity!!)
            .load(beacondetail.campaigns!!.products!![0].image_regular_full)
            .listener(object : RequestListener<Drawable>{
                override fun onLoadFailed(
                    e: GlideException?,
                    model: Any?,
                    target: Target<Drawable>?,
                    isFirstResource: Boolean
                ): Boolean {
                    dismiss()
                    return false
                }

                override fun onResourceReady(
                    resource: Drawable?,
                    model: Any?,
                    target: Target<Drawable>?,
                    dataSource: DataSource?,
                    isFirstResource: Boolean
                ): Boolean {
                    val mProgressBar: ProgressBar = realView.findViewById<ProgressBar>(com.fourello.expee.R.id.progress)
                    realView.progress_circle.visibility = View.GONE
                    var i = 0

                    mProgressBar.progress = i
                    mCountDownTimer = object : CountDownTimer(5000, 100) {

                        override fun onTick(millisUntilFinished: Long) {
                            Log.v("Log_tag", "Tick of Progress$i$millisUntilFinished")
                            i++
                            mProgressBar.progress = i * 100 / (5000 / 100)

                        }

                        override fun onFinish() {
                            //Do what you want

                            i++
                            mProgressBar.progress = 100
                            mainAct.doneStory(false)
                            dismiss()
                            activity!!.overridePendingTransition(R.anim.enterfrom,R.anim.enterto)
                        }
                    }
                    mCountDownTimer!!.start()
                    return false
                }

            })
            .into(realView.story_image)




        realView.close_story.setOnClickListener {
            if(mCountDownTimer != null) {
                mCountDownTimer!!.cancel()
            }
            mainAct.doneStory(false)
            dismiss()
            activity!!.overridePendingTransition(R.anim.exitfrom, R.anim.exitto)

        }

        return realView
    }


}
