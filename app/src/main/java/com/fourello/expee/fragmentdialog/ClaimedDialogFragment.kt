package com.fourello.expee.fragmentdialog


import android.os.Bundle
import android.view.*
import androidx.fragment.app.DialogFragment

import com.fourello.expee.R
import com.fourello.expee.constant.keys
import com.pixplicity.easyprefs.library.Prefs
import kotlinx.android.synthetic.main.fragment_claimed_dialog.view.*


class ClaimedDialogFragment : DialogFragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
       val realView = inflater.inflate(R.layout.fragment_claimed_dialog, container, false)
        dialog!!.window!!.requestFeature(Window.FEATURE_NO_TITLE)
        dialog!!.window!!.setBackgroundDrawableResource(com.fourello.expee.R.drawable.shape_rounded_corners_white_more_radius)
        dialog!!.window!!.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT)


        realView.done_claimed.setOnClickListener {
            Prefs.putBoolean(keys.SHOWINGDIALOG,false)
            dismiss()

        }

    return realView
    }


}
