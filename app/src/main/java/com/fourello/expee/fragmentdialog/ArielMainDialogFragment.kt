package com.fourello.expee.fragmentdialog


import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.*
import android.widget.Toast
import androidx.fragment.app.DialogFragment

import com.fourello.expee.R
import com.fourello.expee.activities.GameActivity
import com.fourello.expee.activities.MainMenuActivity
import com.fourello.expee.activities.ScannerActivity
import com.fourello.expee.constant.keys
import com.fourello.expee.model.SelectWinnerFailed
import com.fourello.expee.model.SelectWinnerWon
import com.fourello.expee.webservice.UserService
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.pixplicity.easyprefs.library.Prefs
import kotlinx.android.synthetic.main.fragment_games_main_dialog.view.*
import retrofit2.Call
import java.lang.Exception


class ArielMainDialogFragment constructor(mainAct : MainMenuActivity) : DialogFragment() {
    var arielCreds = "0"
    val mainAct = mainAct
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val realView = inflater.inflate(R.layout.fragment_games_main_dialog, container, false)

        dialog!!.window!!.requestFeature(Window.FEATURE_NO_TITLE)
        dialog!!.window!!.setBackgroundDrawableResource(R.drawable.shape_rounded_corners_white_more_radius)
        dialog!!.window!!.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT)

        arielCreds = Prefs.getString(keys.CREDITS_ARIEL,"0")

        if(arielCreds == "0"){
            realView.btn_play.visibility = View.GONE
            realView.no_creds_lay.visibility = View.VISIBLE
        }else{
            realView.btn_play.visibility = View.VISIBLE
            realView.no_creds_lay.visibility = View.GONE
        }


        realView.btn_play.setOnClickListener {


            callSelectWinners()

        }



        realView.btn_geme_one.setOnClickListener {

//            val intent = Intent(activity!!, ScannerActivity::class.java)
//            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
//            activity!!.startActivity(intent)
//            activity!!.overridePendingTransition(
//                R.anim.enterfrom,
//                R.anim.enterto
//            )

            val ft = fragmentManager!!.beginTransaction()
            val dialogFragment = RedeemCreditsDialogFragment(mainAct)
            dialogFragment.isCancelable = false
            dialogFragment.show(ft, "redeem")
            Prefs.putBoolean(keys.SHOWINGDIALOG, true)


            dismiss()


        }

        realView.close_ariel.setOnClickListener {
            Prefs.putBoolean(keys.SHOWINGDIALOG,false)
            dismiss()
        }

        return realView

    }

    fun callSelectWinners() {
        val mGson = GsonBuilder()
            .setLenient()
            .create()

       // val seleceted = Prefs.getString(keys.SELECTED_GAME, "")
        var gameCode = "ariel_spin_the_wheel"


        val token = Prefs.getString(keys.USER_TOKEN, "")
        val apiService = UserService.create(this.getString(R.string.base_url))
        val callService = apiService.selectWinner(
            gameCode,
             "PUT", "application/json", "Bearer $token"
        )
        callService.enqueue(object : retrofit2.Callback<Object> {
            override fun onResponse(
                call: Call<Object>,
                response: retrofit2.Response<Object>?
            ) {
                val a_json_string = mGson.toJson(response!!.body())
                if (response != null) {

                    //game winner code 1,2,3,
                    // failed 4,5,6
                    if (response.code() == 200) {

                        try {
                            val failed: SelectWinnerFailed =
                                Gson().fromJson<SelectWinnerFailed>(a_json_string, SelectWinnerFailed::class.java)
                            Prefs.putString(keys.WINNER_CODE, failed.responseData.toString())
                        } catch (e: Exception) {
                            val won: SelectWinnerWon =
                                Gson().fromJson<SelectWinnerWon>(a_json_string, SelectWinnerWon::class.java)
                            Prefs.putString(keys.WINNER_CODE, won.responseData!!.result.toString())

                        }

                        mainAct.updateCreditsAPI()

                        if(arielCreds == "1"){
                            Prefs.putString(keys.CREDITS_ARIEL,"0")
                        }

                        val intent = Intent(mainAct, GameActivity::class.java)
                        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                        mainAct.startActivity(intent)
                        mainAct.overridePendingTransition(
                            R.anim.enterfrom,
                            R.anim.enterto
                        )
                        dismiss()


                    } else if (response.code() == 400) {
                        Toast.makeText(context,"Server Error",Toast.LENGTH_SHORT).show()
                    } else {
                        Toast.makeText(context,"Server Error",Toast.LENGTH_SHORT).show()
                    }
                }
            }

            override fun onFailure(call: Call<Object>, t: Throwable) {
                Log.e("aa", t.toString());

                Toast.makeText(context,"Server Error", Toast.LENGTH_SHORT).show()

            }
        })

    }



}
