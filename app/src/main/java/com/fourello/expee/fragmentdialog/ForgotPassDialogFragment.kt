package com.fourello.expee.fragmentdialog


import android.os.Bundle
import android.util.Log
import android.view.*
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.DialogFragment

import com.fourello.expee.R
import com.fourello.expee.webservice.UserService
import kotlinx.android.synthetic.main.fragment_forgot_pass_dialog.view.*

import retrofit2.Call


class ForgotPassDialogFragment : DialogFragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
       val realView =inflater.inflate(R.layout.fragment_forgot_pass_dialog, container, false)

        dialog!!.window!!.requestFeature(Window.FEATURE_NO_TITLE)
        dialog!!.window!!.setBackgroundDrawableResource(R.drawable.shape_rounded_corners_white_more_radius)
        dialog!!.window!!.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT)


        realView.btn_send.setOnClickListener {

            if(realView.edt_log_email_forgot.text.toString().trim() != "") {
                callWebService(realView.edt_log_email_forgot.text.toString(),realView)

            }else{

                displayDialog("Please enter your registered email.")
            }
        }

        realView.close_forgot.setOnClickListener {

            dismiss()

        }

        return realView
    }

    fun callWebService(email : String,realView : View) {

       realView.progress_layout_forgot.visibility = View.VISIBLE
        val apiService = UserService.create(this.getString(R.string.base_url))

        val callService = apiService.forgotPassword(email.trim(),  "application/json")
        callService.enqueue(object : retrofit2.Callback<Object> {
            override fun onResponse(
                call: Call<Object>,
                response: retrofit2.Response<Object>?
            ) {
                if (response != null) {

                    if (response.code() == 200) {
                        val builder = AlertDialog.Builder(activity!!)
                        builder.setMessage("Email was sent.")
                        builder.setPositiveButton("Ok") { dialog, _ ->
                            dismiss()
                            dialog.dismiss()
                        }
                        val dialog: AlertDialog = builder.create()
                        dialog.show()



                    } else {
                        displayDialog("Something went wrong!")

                    }
                }
                realView.progress_layout_forgot.visibility = View.GONE
            }

            override fun onFailure(call: Call<Object>, t: Throwable) {
                Log.e("aa", t.toString());
                realView.progress_layout_forgot.visibility = View.GONE
                displayDialog("Something went wrong!!")
            }
        })

    }

    fun displayDialog(message: String) {
        val builder = AlertDialog.Builder(activity!!)
        builder.setMessage(message)
        builder.setPositiveButton("Ok") { dialog, _ ->
            dialog.dismiss()
        }
        val dialog: AlertDialog = builder.create()
        dialog.show()
    }
}
