package com.fourello.expee.fragmentdialog


import android.content.Context
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.*
import androidx.fragment.app.DialogFragment
import com.fourello.expee.activities.MainMenuActivity
import kotlinx.android.synthetic.main.fragment_claim_pin_dialog.view.*
import com.mukesh.OtpView

import android.util.Log
import android.view.inputmethod.InputMethodManager
import android.widget.Toast


import com.fourello.expee.constant.keys
import com.fourello.expee.model.ClaimedPrizeResponse
import com.fourello.expee.model.PrizeInfo
import com.fourello.expee.webservice.UserService
import com.pixplicity.easyprefs.library.Prefs
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class ClaimPinDialogFragment constructor(mainAct : MainMenuActivity,
                                         prizeInfo: PrizeInfo,
                                         prizeTitle : String) : DialogFragment() {

    val prizeTitle = prizeTitle
    val prizeInfo = prizeInfo
    val pin = prizeInfo.code
    val mainMenuActivity = mainAct
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
      val realView =  inflater.inflate(com.fourello.expee.R.layout.fragment_claim_pin_dialog, container, false)


        dialog!!.window!!.requestFeature(Window.FEATURE_NO_TITLE)
        dialog!!.window!!.setBackgroundDrawableResource(com.fourello.expee.R.drawable.shape_rounded_corners_white_more_radius)
        dialog!!.window!!.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT)

        realView.prize_title.text = prizeTitle



        val otpView: OtpView
        otpView = realView.findViewById(com.fourello.expee.R.id.otp_view)
        otpView.setOtpCompletionListener { otp ->

            if(otp == pin){
                realView.progress_circular.visibility = View.VISIBLE
                realView.wrong_pin.text = "Successful!"
                realView.wrong_pin.setTextColor(context!!.getColor(android.R.color.holo_green_dark))
                realView.wrong_pin.visibility = View.GONE
                //Thread.sleep(1000)
                otpView.isEnabled = false

                callGetPrizes(prizeInfo.id!!,realView)

            }else{
                otpView.isEnabled = true
                realView.wrong_pin.text = "Wrong PIN!"
                realView.wrong_pin.setTextColor(context!!.getColor(android.R.color.holo_red_light))
                realView.wrong_pin.visibility = View.VISIBLE
                realView.progress_circular.visibility = View.GONE
            }

            //Toast.makeText(context!!, otp , Toast.LENGTH_SHORT).show()
        }

        realView.close_pin.setOnClickListener {
            Prefs.putBoolean(keys.SHOWINGDIALOG,false)
            val imm = context!!.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager?
            imm!!.hideSoftInputFromWindow(otpView.getWindowToken(), 0)
            dismiss()
        }

        otpView.addTextChangedListener(object : TextWatcher{
            override fun afterTextChanged(p0: Editable?) {

            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

          }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            realView.wrong_pin.visibility = View.GONE
         }


        })

        otpView.requestFocus()

        val imm = context!!.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager?
        imm!!.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0)

        return realView
    }


    fun callGetPrizes(id: String,realView : View){
        var token : String = Prefs.getString(keys.USER_TOKEN,"");
        val apiService = UserService.create(this.getString(com.fourello.expee.R.string.base_url))
        val call = apiService.claimPrizes(id,"PUT","application/json",
            "Bearer $token"
        )

        call.enqueue(object : Callback<ClaimedPrizeResponse> {

            override fun onResponse(call: Call<ClaimedPrizeResponse>, response: Response<ClaimedPrizeResponse>) {

                if(response.code() == 200){
                    mainMenuActivity.callGetPrizes()
                    val ft = mainMenuActivity.supportFragmentManager.beginTransaction()
                    val dialogFragment = ClaimedDialogFragment()
                    dialogFragment.isCancelable = false
                    dialogFragment.show(ft, "redeem")
                    dismiss()

                }else{
                    Toast.makeText(context!!,"Failed",Toast.LENGTH_SHORT).show()
                    realView.progress_circular.visibility = View.GONE
                }
            }
            override fun onFailure(call: Call<ClaimedPrizeResponse>, t: Throwable) {

                Toast.makeText(context!!,"Failed",Toast.LENGTH_SHORT).show()
                Log.e("aa", t.toString());
                realView.progress_circular.visibility = View.GONE

            }

        })
    }



}
