package com.fourello.expee.fragmentdialog


import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.*
import android.widget.Toast
import androidx.fragment.app.DialogFragment

import com.fourello.expee.R
import com.fourello.expee.activities.GameActivity
import com.fourello.expee.activities.MainMenuActivity
import com.fourello.expee.activities.ScannerActivity
import com.fourello.expee.constant.keys
import com.fourello.expee.model.SelectWinnerFailed
import com.fourello.expee.model.SelectWinnerWon
import com.fourello.expee.webservice.UserService
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.pixplicity.easyprefs.library.Prefs
import kotlinx.android.synthetic.main.fragment_luckey_dialog.view.*
import kotlinx.android.synthetic.main.fragment_luckey_dialog.view.btn_play
import retrofit2.Call
import java.lang.Exception


class LuckeyDialogFragment  constructor(mainAct : MainMenuActivity): DialogFragment() {
    var luckyCreds = "0"
    val mainAct = mainAct
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
      val realView = inflater.inflate(R.layout.fragment_luckey_dialog, container, false)


        dialog!!.window!!.requestFeature(Window.FEATURE_NO_TITLE)
        dialog!!.window!!.setBackgroundDrawableResource(R.drawable.shape_rounded_corners_white_more_radius)
        dialog!!.window!!.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT)

        luckyCreds = Prefs.getString(keys.CREDITS_LUCKYME,"0")

        if(luckyCreds == "0"){
            realView.btn_play.visibility = View.GONE
            realView.no_creds_lay_lucky.visibility = View.VISIBLE
        }else{
            realView.btn_play.visibility = View.VISIBLE
            realView.no_creds_lay_lucky.visibility = View.GONE
        }



        realView.btn_geme_one.setOnClickListener {

            val ft = fragmentManager!!.beginTransaction()
            val dialogFragment = RedeemCreditsDialogFragment(mainAct)
            dialogFragment.isCancelable = false
            dialogFragment.show(ft, "redeem")
            Prefs.putBoolean(keys.SHOWINGDIALOG, true)


            dismiss()



        }
        realView.btn_play.setOnClickListener {

            callSelectWinners()

        }

        realView.close_lucky.setOnClickListener {
            Prefs.putBoolean(keys.SHOWINGDIALOG,false)
            dismiss()
        }

    return realView
    }
    fun callSelectWinners() {
        val mGson = GsonBuilder()
            .setLenient()
            .create()

        // val seleceted = Prefs.getString(keys.SELECTED_GAME, "")
        var gameCode = "lucky_me_spin_the_wheel"

        //http://files.taktylstudios.com/projects/fourello/expee-wheel/ariel/0.1.0/?result=1
//        if (seleceted == "ariel") {
//            gameCode = "ariel_spin_the_wheel"
//        } else if (seleceted == "pantene") {
//            gameCode = "pantene_spin_the_wheel"
//
//        } else if (seleceted == "lucky") {
//            gameCode = "luckyme_spin_the_wheel"
//        }

        val token = Prefs.getString(keys.USER_TOKEN, "")
        val apiService = UserService.create(this.getString(R.string.base_url))
        val callService = apiService.selectWinner(
            gameCode,
             "PUT", "application/json", "Bearer $token"
        )
        callService.enqueue(object : retrofit2.Callback<Object> {
            override fun onResponse(
                call: Call<Object>,
                response: retrofit2.Response<Object>?
            ) {
                val a_json_string = mGson.toJson(response!!.body())
                if (response != null) {

                    if (response.code() == 200) {

                        try {
                            val failed: SelectWinnerFailed =
                                Gson().fromJson<SelectWinnerFailed>(a_json_string, SelectWinnerFailed::class.java)
                            Prefs.putString(keys.WINNER_CODE, failed.responseData.toString())
                            //       Toast.makeText(applicationContext,failed.responseData.toString(),Toast.LENGTH_SHORT).show()
                        } catch (e: Exception) {
                            val won: SelectWinnerWon =
                                Gson().fromJson<SelectWinnerWon>(a_json_string, SelectWinnerWon::class.java)
                            Prefs.putString(keys.WINNER_CODE, won.responseData!!.result.toString())
                            //      Toast.makeText(applicationContext,won.responseData!!.game_id.toString(),Toast.LENGTH_SHORT).show()
                        }

                        mainAct.updateCreditsAPI()

                        if(luckyCreds == "1"){
                            Prefs.putString(keys.CREDITS_LUCKYME,"0")
                        }

                        val intent = Intent(mainAct, GameActivity::class.java)
                        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                        mainAct.startActivity(intent)
                        mainAct.overridePendingTransition(
                            R.anim.enterfrom,
                            R.anim.enterto
                        )
                        dismiss()

//                      Prefs.getString(keys.WINNER_CODE,response.body()!!.responseData!!.toString())
//                        Toast.makeText(applicationContext,response.body()!!.responseData!!.toString(),Toast.LENGTH_SHORT).show()

                        // if(response.body() instanceof )


                    } else if (response.code() == 400) {
                        //        displayDialog("Something went wrong!")
                        Toast.makeText(context,"Server Error: " + response.message(),Toast.LENGTH_SHORT).show()
                    } else {
                        //        displayDialog("Something went wrong!")
                        Toast.makeText(context,"Server Error: " + response.message(),Toast.LENGTH_SHORT).show()
                    }
                }
                //    progress_layout_login.visibility = View.GONE
            }

            override fun onFailure(call: Call<Object>, t: Throwable) {
                Log.e("aa", t.toString());
                Toast.makeText(context,"Server Error: " + t.message,Toast.LENGTH_SHORT).show()
                //    progress_layout_login.visibility = View.GONE
                //    displayDialog("Something went wrong!! " +t.message)
            }
        })

    }

}
