package com.fourello.expee.fragmentdialog


import android.os.Bundle
import android.view.*
import androidx.core.content.ContextCompat
import androidx.fragment.app.DialogFragment

import com.fourello.expee.R
import com.fourello.expee.constant.keys
import com.fourello.expee.model.UserProfileData
import com.google.gson.GsonBuilder
import com.pixplicity.easyprefs.library.Prefs
import kotlinx.android.synthetic.main.fragment_profile_create_dialog.view.btn_age_four
import kotlinx.android.synthetic.main.fragment_profile_create_dialog.view.btn_age_one
import kotlinx.android.synthetic.main.fragment_profile_create_dialog.view.btn_age_tree
import kotlinx.android.synthetic.main.fragment_profile_create_dialog.view.btn_age_two
import kotlinx.android.synthetic.main.fragment_profile_create_dialog.view.btn_female
import kotlinx.android.synthetic.main.fragment_profile_create_dialog.view.btn_male
import kotlinx.android.synthetic.main.fragment_profile_create_dialog.view.btn_other
import kotlinx.android.synthetic.main.fragment_profile_create_dialog.view.close_profile
import kotlinx.android.synthetic.main.fragment_profile_view_info.view.*


class ProfileViewInfoFragment : DialogFragment() {
    val mGson = GsonBuilder()
        .setLenient()
        .create()
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
       val realView =  inflater.inflate(R.layout.fragment_profile_view_info, container, false)
        dialog!!.window!!.requestFeature(Window.FEATURE_NO_TITLE)
        dialog!!.window!!.setBackgroundDrawableResource(R.drawable.shape_rounded_corners_white_more_radius)
        dialog!!.window!!.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT)

        val userInfoString = Prefs.getString(keys.USER_FULL_DATA,"")

        val userProfileData : UserProfileData = mGson.fromJson(userInfoString, UserProfileData::class.java)

        if(userProfileData.information!!.full_name != "") {
            realView.full_name.text = userProfileData.information!!.full_name
        }else{
            realView.full_name.text = userProfileData.information!!.first_name + " " + userProfileData.information!!.last_name
        }
        if(userProfileData.information!!.age_range == "18-24"){
            realView.btn_age_one.backgroundTintList = ContextCompat.getColorStateList(activity!!, R.color.btn_blue)
            realView.btn_age_two.backgroundTintList = ContextCompat.getColorStateList(activity!!, R.color.gray_age)
            realView.btn_age_tree.backgroundTintList = ContextCompat.getColorStateList(activity!!, R.color.gray_age)
            realView.btn_age_four.backgroundTintList = ContextCompat.getColorStateList(activity!!, R.color.gray_age)
            realView.btn_age_five.backgroundTintList = ContextCompat.getColorStateList(activity!!, R.color.gray_age)


        }else if(userProfileData.information!!.age_range == "25-39"){
            realView.btn_age_one.backgroundTintList = ContextCompat.getColorStateList(activity!!, R.color.gray_age)
            realView.btn_age_two.backgroundTintList = ContextCompat.getColorStateList(activity!!, R.color.btn_blue)
            realView.btn_age_tree.backgroundTintList = ContextCompat.getColorStateList(activity!!, R.color.gray_age)
            realView.btn_age_four.backgroundTintList = ContextCompat.getColorStateList(activity!!, R.color.gray_age)
            realView.btn_age_five.backgroundTintList = ContextCompat.getColorStateList(activity!!, R.color.gray_age)


        }else if(userProfileData.information!!.age_range == "40-54"){

            realView.btn_age_one.backgroundTintList = ContextCompat.getColorStateList(activity!!, R.color.gray_age)
            realView.btn_age_two.backgroundTintList = ContextCompat.getColorStateList(activity!!, R.color.gray_age)
            realView.btn_age_tree.backgroundTintList = ContextCompat.getColorStateList(activity!!, R.color.btn_blue)
            realView.btn_age_four.backgroundTintList = ContextCompat.getColorStateList(activity!!, R.color.gray_age)
            realView.btn_age_five.backgroundTintList = ContextCompat.getColorStateList(activity!!, R.color.gray_age)

        }else if(userProfileData.information!!.age_range == "55-74"){

            realView.btn_age_one.backgroundTintList = ContextCompat.getColorStateList(activity!!, R.color.gray_age)
            realView.btn_age_two.backgroundTintList = ContextCompat.getColorStateList(activity!!, R.color.gray_age)
            realView.btn_age_tree.backgroundTintList = ContextCompat.getColorStateList(activity!!, R.color.gray_age)
            realView.btn_age_four.backgroundTintList = ContextCompat.getColorStateList(activity!!, R.color.btn_blue)
            realView.btn_age_five.backgroundTintList = ContextCompat.getColorStateList(activity!!, R.color.gray_age)

        }else if(userProfileData.information!!.age_range == "74 & above"){

            realView.btn_age_one.backgroundTintList = ContextCompat.getColorStateList(activity!!, R.color.gray_age)
            realView.btn_age_two.backgroundTintList = ContextCompat.getColorStateList(activity!!, R.color.gray_age)
            realView.btn_age_tree.backgroundTintList = ContextCompat.getColorStateList(activity!!, R.color.gray_age)
            realView.btn_age_four.backgroundTintList = ContextCompat.getColorStateList(activity!!, R.color.gray_age)
            realView.btn_age_five.backgroundTintList = ContextCompat.getColorStateList(activity!!, R.color.btn_blue)

        }


        if(userProfileData.information!!.gender == "male"){
            realView.btn_male.alpha = 1f
            realView.btn_female.alpha = .5f
            realView.btn_other.alpha = .5f


        }else if(userProfileData.information!!.gender == "female"){
            realView.btn_male.alpha = .5f
            realView.btn_female.alpha = 1f
            realView.btn_other.alpha = .5f


        }else if(userProfileData.information!!.gender == "not specified"){

            realView.btn_male.alpha = .5f
            realView.btn_female.alpha = .5f
            realView.btn_other.alpha = 1f

        }



        realView.close_profile.setOnClickListener {
            Prefs.putBoolean(keys.SHOWINGDIALOG, false)
            dismiss()

        }

        return realView
    }


}
