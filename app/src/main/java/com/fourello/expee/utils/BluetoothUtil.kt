package com.fourello.expee.utils

import android.bluetooth.BluetoothAdapter

object BluetoothUtil {

    private var mAdapter: BluetoothAdapter? = null

    val isBluetoothEnable: Boolean
        get() {
            if (mAdapter == null) {
                mAdapter = BluetoothAdapter.getDefaultAdapter()
            }
            return mAdapter != null && mAdapter!!.isEnabled
        }

    fun enableBluetooth() {
        if (mAdapter == null) {
            mAdapter = BluetoothAdapter.getDefaultAdapter()
        }
        if (mAdapter != null) mAdapter!!.enable()
    }
}