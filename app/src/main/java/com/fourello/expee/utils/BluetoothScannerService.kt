package com.fourello.expee.utils

import android.annotation.SuppressLint
import android.app.Service
import android.content.Intent
import android.os.IBinder

@SuppressLint("Registered")
class BluetoothScannerService : Service() {

    override fun onStartCommand(intent: Intent, flags: Int, startId: Int): Int {
        return Service.START_NOT_STICKY
    }

    override fun onBind(intent: Intent): IBinder? {
        return null
    }
}