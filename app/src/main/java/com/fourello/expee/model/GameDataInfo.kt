package com.fourello.expee.model

import com.google.gson.annotations.SerializedName

class GameDataInfo {

//    "id": 1,
//    "name": "Ariel Game",
//    "code": "ariel_spin_the_wheel",
//    "description": "Ariel Game",
//    "instruction": "Claim at Customer Service",
//    "type": "spin_the_wheel",
//    "image": "",
//    "cost_per_credit": 50,
//    "valid_until": "2019-11-30 00:00:00",
//    "partner_id": null,
//    "created_at": "2019-11-11 14:07:44",
//    "updated_at": "2019-11-11 14:07:44"

    @SerializedName("id")
    var id: String? = null

    @SerializedName("name")
    var name: String? = null

    @SerializedName("code")
    var code: String? = null

    @SerializedName("description")
    var description: String? = null

    @SerializedName("instruction")
    var instruction: String? = null

    @SerializedName("type")
    var type: String? = null

    @SerializedName("image")
    var image: String? = null

    @SerializedName("cost_per_credit")
    var cost_per_credit: String? = null

    @SerializedName("valid_until")
    var valid_until: String? = null
}