package com.fourello.expee.model

import com.google.gson.annotations.SerializedName

class SelectWinnerWon {

    @SerializedName("status")
    var status: String? = null

    @SerializedName("code")
    var code: Int = 0

    @SerializedName("responseData")
    var responseData: WinnerDetail? = null

}