package com.fourello.expee.model

import com.google.gson.annotations.SerializedName

class UserProfileData {

    @SerializedName("id")
    var id: String? = null

    @SerializedName("username")
    var username: String? = null

    @SerializedName("email")
    var email: String? = null

    @SerializedName("contact_number")
    var mobile_number: String? = null

    @SerializedName("status")
    var status: String? = null

    @SerializedName("is_mobile_verified")
    var is_mobile_verified: String? = null

    @SerializedName("registration_origin")
    var registration_origin: String? = null

    @SerializedName("referral_code")
    var referral_code: String? = null

    @SerializedName("referral_id")
    var referral_id: String? = null

    @SerializedName("created_at")
    var created_at: String? = null

    @SerializedName("information")
    var information: UserInformation? = null

    @SerializedName("game_credits")
    var game_credits: MutableList<GameCredits>? = null

    @SerializedName("referred")
    var referred: List<String>? = null

    @SerializedName("wallet")
    var wallet: UserWallet? = null

    @SerializedName("rewards")
    var rewards: MutableList<String>? = null
}