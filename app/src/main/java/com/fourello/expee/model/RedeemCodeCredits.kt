package com.fourello.expee.model

import com.google.gson.annotations.SerializedName

class RedeemCodeCredits {

    @SerializedName("games")
    var games: MutableList<RedeemCodeGames>? = null
}