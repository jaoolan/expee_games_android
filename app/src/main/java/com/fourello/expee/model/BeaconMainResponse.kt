package com.fourello.expee_kotlin.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class BeaconMainResponse {


    @SerializedName("status")
    @Expose
     var status: String? = null

    @SerializedName("code")
    @Expose
     var code: Int = 0

    @SerializedName("responseData")
    @Expose
     var responseData: List<BeaconListData>? = null
}