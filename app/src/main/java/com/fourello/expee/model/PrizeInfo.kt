package com.fourello.expee.model

import com.google.gson.annotations.SerializedName

class PrizeInfo {

//    "id": 1,
//    "name": "Grand Prize Ariel",
//    "valid_date": "2019-11-18",
//    "description": null,
//    "instruction": null,
//    "image": "",
//    "status": "claimed",
//    "claim_information": null,
//    "chance": 30,
//    "user_id": "1c3c9898-40ce-4d0b-a14b-02a7c8c28b3c",
//    "game_id": 1,
//    "created_at": null,
//    "updated_at": "2019-11-19 20:05:58",
//    "branch_id": "38d56d46-3449-4154-91ef-01d1e182632d",
//    "result": 1,
//    "code": "0000",
//    "user":


    @SerializedName("id")
    var id: String? = null

    @SerializedName("name")
    var name: String? = null

    @SerializedName("valid_date")
    var valid_date: String? = null

    @SerializedName("status")
    var status: String? = null

    @SerializedName("chance")
    var chance: String? = null

    @SerializedName("user_id")
    var user_id: String? = null

    @SerializedName("game_id")
    var game_id: String? = null

    @SerializedName("branch_id")
    var branch_id: String? = null

    @SerializedName("result")
    var result: String? = null

    @SerializedName("code")
    var code: String? = null

    @SerializedName("game")
    var game: GameDataInfo? = null
}