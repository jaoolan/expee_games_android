package com.fourello.expee.model

import com.google.gson.annotations.SerializedName

class GameCheckoutResponse {


    @SerializedName("status")
    var status: String? = null

    @SerializedName("code")
    var code: String = ""

    @SerializedName("responseData")
    var total_credits:  String? = null

}