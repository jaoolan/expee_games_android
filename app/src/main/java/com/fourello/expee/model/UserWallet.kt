package com.fourello.expee.model

import com.google.gson.annotations.SerializedName

class UserWallet {

//    "id": "dd7f3d6b-4d81-4b36-9e73-4439838cc5ea",
//    "usable_point": 779.75,
//    "pending_point": 0,
//    "reward_level": "bronze",
//    "multiplier": 0.1,
//    "lifetime_point": 793.5,
//    "expee_point": 779.75,
//    "user_id": "929baba2-9868-4729-86c2-23e0ac6d5e9f"

    @SerializedName("id")
    var id: String? = null

    @SerializedName("usable_point")
    var usable_point: String? = null

    @SerializedName("pending_point")
    var pending_point: String? = null

    @SerializedName("reward_level")
    var reward_level: String? = null

    @SerializedName("multiplier")
    var multiplier: String? = null

    @SerializedName("lifetime_point")
    var lifetime_point: String? = null


    @SerializedName("expee_point")
    var expee_point: String? = null
}