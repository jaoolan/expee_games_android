package com.fourello.expee.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class Campaign {

    @SerializedName("id")
    @Expose
     var id: String? = null

    @SerializedName("name")
    @Expose
     var name: String? = null

    @SerializedName("type")
    @Expose
     var type: String? = null

    @SerializedName("demographic")
    @Expose
     var demographic: String? = null

    @SerializedName("discount_type")
    @Expose
     var discount_type: String? = null

    @SerializedName("discount_value")
    @Expose
     var discount_value: String? = null


    @SerializedName("notification")
    @Expose
     var notification: String? = null

    @SerializedName("expiration_date")
    @Expose
     var expiration_date: String? = null

    @SerializedName("beacon_id")
    @Expose
     var beacon_id: String? = null

    @SerializedName("recipe_id")
    @Expose
     var recipe_id: String? = null

    @SerializedName("user_id")
    @Expose
     var user_id: String? = null

    @SerializedName("product_id")
    @Expose
     var product_id: String? = null


    @SerializedName("products")
    @Expose
     var products: List<MainProducts>? = null
}