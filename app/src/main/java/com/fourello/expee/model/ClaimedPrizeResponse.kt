package com.fourello.expee.model

import com.google.gson.annotations.SerializedName

class ClaimedPrizeResponse {

    @SerializedName("status")
    var status: String? = null

    @SerializedName("code")
    var code: String = ""

    @SerializedName("responseData")
    var claimePrizeInfo:  ClaimePrizeInfo? = null
}