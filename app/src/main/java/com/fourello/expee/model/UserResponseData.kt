package com.fourello.expee.model

import com.google.gson.annotations.SerializedName

class UserResponseData{


    @SerializedName("user")
    var user: UserProfileData? = null

    @SerializedName("token")
    var token: UserToken? = null

}