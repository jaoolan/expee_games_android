package com.fourello.expee.model

import com.google.gson.annotations.SerializedName

class UserInformation {

    @SerializedName("first_name")
    var first_name: String? = null

    @SerializedName("middle_name")
    var middle_name: String? = null

    @SerializedName("last_name")
    var last_name: String? = null

    @SerializedName("full_name")
    var full_name: String? = null


    @SerializedName("address")
    var address: String? = null

    @SerializedName("gender")
    var gender: String? = null

    // "age_range
    @SerializedName("age_range")
    var age_range: String? = null

    @SerializedName("date_of_birth")
    var date_of_birth: String? = null

    @SerializedName("profile_picture")
    var profile_picture: String? = null


    @SerializedName("profile_picture_full")
    var profile_picture_full: String? = null
}