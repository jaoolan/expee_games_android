package com.fourello.expee.model

import com.google.gson.annotations.SerializedName

class UserToken {

    @SerializedName("accessToken")
    var accessToken: String? = null
}