package com.fourello.expee.model

import com.google.gson.annotations.SerializedName

class GameCheckoutData {

    @SerializedName("total_credit")
    var total_credit: Int = 0

}