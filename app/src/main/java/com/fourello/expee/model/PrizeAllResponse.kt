package com.fourello.expee.model

import com.google.gson.annotations.SerializedName

class PrizeAllResponse {

    @SerializedName("status")
    var status: String? = null

    @SerializedName("code")
    var code: String = ""

    @SerializedName("responseData")
    var responseData:  PrizeAllResponseData? = null
}