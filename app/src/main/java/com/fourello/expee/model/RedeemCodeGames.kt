package com.fourello.expee.model

import com.google.gson.annotations.SerializedName

class RedeemCodeGames {

    @SerializedName("id")
    var id:String? = null


    @SerializedName("name")
    var name:String? = null

    @SerializedName("code")
    var code:String? = null

    @SerializedName("description")
    var description:String? = null

    @SerializedName("instruction")
    var instruction:String? = null

    @SerializedName("type")
    var type:String? = null

    @SerializedName("image")
    var image:String? = null

    @SerializedName("cost_per_credit")
    var cost_per_credit:String? = null

    @SerializedName("valid_until")
    var valid_until:String? = null

    @SerializedName("created_at")
    var created_at:String? = null

    @SerializedName("count")
    var count:Int? = null
}