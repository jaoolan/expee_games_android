package com.fourello.expee.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class MainProducts {

    @SerializedName("id")
    @Expose
     var id: String? = null

    @SerializedName("label")
    @Expose
     var label: String? = null

    @SerializedName("note")
    @Expose
     var note: String? = null

    @SerializedName("demographic_value")
    @Expose
     var demographic_value: String? = null

    @SerializedName("image_regular")
    @Expose
     var image_regular: String? = null

    @SerializedName("image_tall")
    @Expose
     var image_tall: String? = null

    @SerializedName("is_action_left_internal")
    @Expose
     var is_action_left_internal: String? = null

    @SerializedName("action_left_title")
    @Expose
     var action_left_title: String? = null

    @SerializedName("action_left_type")
    @Expose
     var action_left_type: String? = null

    @SerializedName("action_left_url")
    @Expose
     var action_left_url: String? = null

    @SerializedName("is_action_right_internal")
    @Expose
     var is_action_right_internal: String? = null

    @SerializedName("action_right_title")
    @Expose
     var action_right_title: String? = null

    @SerializedName("action_right_type")
    @Expose
     var action_right_type: String? = null

    @SerializedName("action_right_url")
    @Expose
     var action_right_url: String? = null

    @SerializedName("campaign_id")
    @Expose
     var campaign_id: String? = null

    @SerializedName("product_id")
    @Expose
     var product_id: String? = null

    @SerializedName("user_id")
    @Expose
     var user_id: String? = null

//
//    @SerializedName("recipe_value")
//    @Expose
//     var recipe_value: Recipe? = null

    @SerializedName("right_action_value")
    @Expose
     var right_action_value: String? = null

    @SerializedName("image_regular_full")
    @Expose
     var image_regular_full: String? = null


    @SerializedName("image_tall_full")
    @Expose
     var image_tall_full: String? = null
}