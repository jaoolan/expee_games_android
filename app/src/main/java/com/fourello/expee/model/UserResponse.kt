package com.fourello.expee.model

import com.google.gson.annotations.SerializedName

class UserResponse {

    @SerializedName("status")
    var status: String? = null

    @SerializedName("code")
    var code: String? = null

    @SerializedName("responseData")
    var userResponseData: UserResponseData? = null
}

