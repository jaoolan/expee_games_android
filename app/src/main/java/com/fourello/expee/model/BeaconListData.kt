package com.fourello.expee_kotlin.model

import com.fourello.expee.model.Campaign
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import org.altbeacon.beacon.Region

class BeaconListData {


    @SerializedName("id")
    @Expose
     var id: String? = null

    @SerializedName("uuid")
    @Expose
     var uuid: String? = null

    @SerializedName("major")
    @Expose
     var major: Int = 0

    @SerializedName("minor")
    @Expose
     var minor: Int = 0

    @SerializedName("name")
    @Expose
     var name: String? = null

    @SerializedName("location")
    @Expose
     var location: String? = null

    @SerializedName("branch_id")
    @Expose
     var branch_id: String? = null

    @SerializedName("campaigns")
    @Expose
     var campaigns: Campaign? = null

    var mRegion: Region? = null

}