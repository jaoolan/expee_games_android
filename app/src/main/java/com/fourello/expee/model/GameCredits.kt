package com.fourello.expee.model

import com.google.gson.annotations.SerializedName

class GameCredits {

//    "id": 7,
//    "credit": 100,
//    "used": 0,
//    "comment": null,
//    "checkout_id": "00001750-e955-487c-aeb0-ccf5f7d23700",
//    "user_id": "1c3c9898-40ce-4d0b-a14b-02a7c8c28b3c",
//    "game_id": 3,
//    "created_at": "2019-11-14 14:56:22",
//    "updated_at": "2019-11-16 11:45:30"

    @SerializedName("id")
    var id: String? = null

    @SerializedName("credit")
    var credit: String? = null

    @SerializedName("used")
    var used: String? = null

    @SerializedName("comment")
    var comment: String? = null

    @SerializedName("checkout_id")
    var checkout_id: String? = null

    @SerializedName("user_id")
    var user_id: String? = null

    @SerializedName("game_id")
    var game_id: String? = null

    @SerializedName("created_at")
    var created_at: String? = null
}