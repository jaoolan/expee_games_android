package com.fourello.expee.model

import com.google.gson.annotations.SerializedName

class ClaimePrizeInfo {

    @SerializedName("id")
    var id: String = ""

    @SerializedName("name")
    var name: String = ""

    @SerializedName("valid_date")
    var valid_date: String = ""

    @SerializedName("description")
    var description: String = ""

    @SerializedName("instruction")
    var instruction: String = ""

    @SerializedName("image")
    var image: String = ""

    @SerializedName("status")
    var status: String = ""

    @SerializedName("claim_information")
    var claim_information: String = ""

    @SerializedName("chance")
    var chance: String = ""

    @SerializedName("user_id")
    var user_id: String = ""

    @SerializedName("game_id")
    var game_id: String = ""

    @SerializedName("result")
    var result: String = ""

    @SerializedName("code")
    var code: String = ""

    @SerializedName("branch_id")
    var branch_id: String = ""
}