package com.fourello.expee.model

import com.google.gson.annotations.SerializedName

class RedeemCodeResponse {

    @SerializedName("status")
    var status: String? = null

    @SerializedName("code")
    var code: String? = null

    @SerializedName("responseData")
    var userProfileData: RedeemCodeDetail? = null
}