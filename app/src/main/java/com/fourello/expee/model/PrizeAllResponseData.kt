package com.fourello.expee.model

import com.google.gson.annotations.SerializedName

class PrizeAllResponseData {

    @SerializedName("current_page")
    var current_page: String = ""

    @SerializedName("data")
    var dataList: MutableList<PrizeInfo> = ArrayList()

    @SerializedName("first_page_url")
    var first_page_url: String = ""

    @SerializedName("total")
    var total: String = ""

    @SerializedName("next_page_url")
    var next_page_url: String = ""
}