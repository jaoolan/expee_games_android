package com.fourello.expee.model

import com.google.gson.annotations.SerializedName

class CheckoutFailedMessage {

    @SerializedName("message")
    var message: String =""
}