package com.fourello.expee.constant

class keys {

    companion object {
        lateinit var instance: keys

        val USER_ID = "user_id"
        val USER_TOKEN = "user_token_ke"
        val USER_FULL_DATA = "user_full_data"
        val USER_PHONE_NUMBER = "user_phone_number"

        val WINNER_CODE = "winner_code"

        val CREDITS_ARIEL = "credits_ariel"
        val CREDITS_PANTENE = "credits_pantene"
        val CREDITS_LUCKYME = "credits_luckyme"


        val MUSIC_CURRENT_ALBUM_PLAY = "music_current_album_play"
        val MUSIC_CURRENT_PLAY = "mucis_current_play"


        val SELECTED_GAME = "selected_game"


        val BUNDLE_TO_BLUETOOTH_RECEVIER = "bundle_to_bluetooth_receiver"


        val TO_REDEEM = "to_redeem"


        val SHOWINGDIALOG = "showng_dialog"


      val REMEMBER_ME = "remember_me"
        val REMEMBER_EMail = "remember_email"

        val REMEMBER_Password = "remember_password"
    }

    init {
        instance = this
    }

}
