package com.fourello.expee.activities

import android.annotation.TargetApi
import android.content.Context
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.graphics.PorterDuff
import android.graphics.PorterDuffXfermode
import android.graphics.Rect
import android.graphics.RectF
import android.os.Build
import android.util.AttributeSet
import android.widget.LinearLayout
import com.fourello.expee.R

import com.pixplicity.easyprefs.library.Prefs


class RectStep2OverlayViewRedeem : LinearLayout {
    private var bitmap: Bitmap? = null

    constructor(context: Context) : super(context) {}

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {}

    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {}

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int, defStyleRes: Int) : super(
        context,
        attrs,
        defStyleAttr,
        defStyleRes
    ) {
    }

    override fun dispatchDraw(canvas: Canvas) {
        super.dispatchDraw(canvas)

        if (bitmap == null) {
            createWindowFrame()
        }
        canvas.drawBitmap(bitmap!!, 0f, 0f, null)
    }

    protected fun createWindowFrame() {
        bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888)
        val osCanvas = Canvas(bitmap!!)
        val strokePaint = Paint()

        strokePaint.style = Paint.Style.STROKE
        strokePaint.color = Color.RED
        strokePaint.strokeWidth = 9f

        val topPart = Paint()
        topPart.style = Paint.Style.FILL
        //turn antialiasing on
        topPart.isAntiAlias = true
        topPart.color = Color.RED
        topPart.strokeWidth = 1f
        topPart.textSize = 16f
        //paint.setTextSize(30);

        //osCanvas.translate(0, 200);

        val outerRectangle = RectF(0f, 0f, width.toFloat(), height.toFloat())

        val paint = Paint(Paint.ANTI_ALIAS_FLAG)
        paint.color = resources.getColor(R.color.black_75)
        osCanvas.drawRect(outerRectangle, paint)


        paint.color = Color.TRANSPARENT
        paint.xfermode = PorterDuffXfermode(PorterDuff.Mode.SRC_OUT)
        //paint.setStyle(Paint.Style.STROKE);
        val centerX = (width / 2).toFloat()
        val centerY = (height / 2).toFloat()
        // float radius = getResources().getDimensionPixelSize(R.dimen.radius);


        osCanvas.drawRect(
            (left + (right - left) / 9).toFloat(),
            (top + (bottom - top) / 4.5).toFloat(),
            (right - (right - left) / 9).toFloat(),
            (bottom - (bottom - top) / 7).toFloat(), //b
            paint
        )

        osCanvas.drawText("TOP PART", 0f, 0f, topPart)
        // osCanvas.translate(50,50);
        // DisplayMetrics metrics = new DisplayMetrics();
        // getWindowManager().getDefaultDisplay().getMetrics(metrics);
        //  float scale = metrics.densityDpi;

        // osCanvas.drawRect(leftX-20*scale, leftY-20*scale, rightX+20*scale, rightY, strokePaint);
        //        osCanvas.drawRect(getLeft()+(getRight()-getLeft())/10.1f,
        //                getTop()+(getBottom()-getTop())/15.9f,
        //                getRight()-(getRight()-getLeft())/10.2f,
        //                getBottom()-(getBottom()-getTop())/8.2f,strokePaint);


        //        Rect rect = new Rect(getLeft()+(getRight()-getLeft())/Math.round(10.1f),
        //                getTop()+(getBottom()-getTop())/Math.round(15.9f),
        //                getRight()-(getRight()-getLeft())/Math.round(10.2f),
        //                getBottom()-(getBottom()-getTop())/Math.round(8.2f));
        val rect = Rect(
            left + (right - left) / 10,
            top + (bottom - top) / 2,
            right - (right - left) / 10,
            bottom - (bottom - top) / 2
        )

        //   osCanvas.drawRect(rect,strokePaint);
        Prefs.putInt("LEFT", left + (right - left) / 10)
        Prefs.putInt("TOP", top + (bottom - top) / 2)
        Prefs.putInt("RIGHT", right - (right - left) / 10)
        Prefs.putInt("BOTTOM", bottom - (bottom - top) / 4)


        val width = rect.width()

        val height = rect.height()

        val sample = RectF(
            (left + (right - left) / 5).toFloat(),
            top + (bottom - top) / 1.75f,
            (right - (right - left) / 5).toFloat(),
            bottom - (bottom - top) / 1.75f
        )

        //     float  width = osCanvas.getWidth();
        //
        //      float  height = osCanvas.getHeight();

        Prefs.putInt("WIT", width)
        Prefs.putInt("HIT", height)

    }

    override fun isInEditMode(): Boolean {
        return true
    }

    override fun onLayout(changed: Boolean, l: Int, t: Int, r: Int, b: Int) {
        super.onLayout(changed, l, t, r, b)
        bitmap = null
    }
}
