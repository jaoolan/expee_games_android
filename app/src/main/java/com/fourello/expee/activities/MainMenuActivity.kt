package com.fourello.expee.activities

import android.Manifest
import android.content.*
import android.content.pm.PackageManager
import android.net.ConnectivityManager
import android.net.Uri
import android.os.AsyncTask
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.view.GravityCompat
import androidx.fragment.app.DialogFragment
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.fourello.expee.R
import com.fourello.expee.adapter.PrizeAdapter
import com.fourello.expee.constant.keys
import com.fourello.expee.fragmentdialog.*
import com.fourello.expee.fragments.FaqFragment
import com.fourello.expee.helper.GridSpacingItemDecoration
import com.fourello.expee.managers.AppBeaconManager
import com.fourello.expee.model.*
import com.fourello.expee.receivers.BluetoothDeviceReceiver
import com.fourello.expee.receivers.BluetoothStateReceiver
import com.fourello.expee.utils.BluetoothScannerService
import com.fourello.expee.webservice.UserService
import com.fourello.expee_kotlin.model.BeaconListData
import com.fourello.expee_kotlin.model.BeaconMainResponse
import com.google.android.gms.tasks.OnSuccessListener
import com.google.firebase.FirebaseApp
import com.google.firebase.iid.FirebaseInstanceId
import com.google.firebase.iid.InstanceIdResult
import com.google.gson.GsonBuilder
import com.pixplicity.easyprefs.library.Prefs
import kotlinx.android.synthetic.main.activity_main_menu.*
import kotlinx.android.synthetic.main.drawer_custom_layout.*
import okhttp3.OkHttpClient
import org.altbeacon.beacon.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.converter.scalars.ScalarsConverterFactory
import java.io.IOException
import java.net.InetSocketAddress
import java.net.Socket
import java.net.SocketAddress
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.TimeUnit
import kotlin.collections.ArrayList
import kotlin.concurrent.timerTask

class MainMenuActivity : AppCompatActivity(), BeaconConsumer, RangeNotifier {
    private var newToken: String? = null
    var mBluetoothDeviceReceiver: BluetoothDeviceReceiver? = null
    var mAppBeaconManager: AppBeaconManager? = null
    private var mBluetoothStateReceiver: BluetoothStateReceiver? = null
    var showingStory: Boolean = false
    var prizeAdapter: PrizeAdapter? = null
    internal lateinit var mReceiver: BroadcastReceiver
    var mGson = GsonBuilder()
        .setLenient()
        .create()
    var lastBeaconId = ""
    override fun didRangeBeaconsInRegion(p0: MutableCollection<Beacon>?, p1: Region?) {


  //turn off beacon, executed jun 20
//        val filteredList = AppBeaconManager.filteredFoundBeacons(p0!!)
//        if (filteredList.isNotEmpty()) {
//
//            if (!showingStory) {
//
//
//                val beaconDetail = AppBeaconManager.getNearestBeacon(filteredList)
//                if (lastBeaconId != beaconDetail!!.id!!) {
//                    if (beaconDetail!!.campaigns!! != null) {
//                        if (!beaconDetail!!.campaigns!!.name!!.contains("expee")) {
//
//                            if (beaconDetail!!.campaigns!!.expiration_date != null) {
//
//                                var dtExpire = beaconDetail!!.campaigns!!.expiration_date
//
//                                //  var expired = "2021-01-01 00:00:00"
//                                var format = SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//                                var  dateNow  =  Date();
//                                try {
//                                    var dateExpire: Date = format.parse(dtExpire)
//
//                                    var dateTime = format.format(dateNow)
//
//                                    dateNow = format.parse(dateTime)
//
//                                    if(dateNow.before(dateExpire)){
//
//                                        if (mainLay.visibility == View.VISIBLE) {
//                                            var dialogShowing = Prefs.getBoolean(keys.SHOWINGDIALOG, true)
//                                            if (!dialogShowing) {
//
//                                                if (!isFinishing()) {
//                                                    try {
//                                                        lastBeaconId = beaconDetail!!.id!!
//
//                                                        showingStory = true
//                                                        val ft = supportFragmentManager.beginTransaction()
//                                                        val dialogFragment = StoryBeaconDialogFragment(this, beaconDetail!!)
//                                                        dialogFragment.isCancelable = false
//                                                        dialogFragment.setStyle(DialogFragment.STYLE_NO_FRAME, 0)
//                                                        dialogFragment.show(ft, "redeem")
//                                                    } catch (e: Exception) {
//
//                                                    }
//                                                }
//                                            }
//                                        }
//
//
//
//
//                                    }else{
//                                        Log.d("Date Expire", "Yes")
//                                    }
//
//                                } catch (e: ParseException) {
//                                    e.printStackTrace();
//                                }
//
//
//
//
//
//
//                            }
//                        }
//                    }
//                }
//
//            }
//
//        }

    }

    //turn off beacon jan 20
    override fun onBeaconServiceConnect() {
//        mAppBeaconManager!!.beaconManager.addRangeNotifier(this)
//
//        // if (mBeaconViewModel.mBluetooth.get()) {
//        mAppBeaconManager!!.startRangingAndMonitoringInRegion()
//        //  }
    }

    //turn off beacon jan 20
    fun startBluetoothScanner() {
        if (ActivityCompat.checkSelfPermission(
                this@MainMenuActivity,
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) == PackageManager.PERMISSION_GRANTED
        ) {
//            ActivityCompat.requestPermissions(
//                this.activity!!,
//                arrayOf(Manifest.permission.BLUETOOTH),
//                11)
            //  if (mViewModel.mUser != null) {
            mBluetoothDeviceReceiver!!.startBluetoothScan(
                this!!.applicationContext!!
                //     mViewModel.mUser
            )
            // }
        } else {
//            ActivityCompat.requestPermissions(
//                this.activity!!,
//                arrayOf(Manifest.permission.BLUETOOTH),
//                11)
            ActivityCompat.requestPermissions(
                this@MainMenuActivity,
                arrayOf(Manifest.permission.ACCESS_COARSE_LOCATION),
                12
            )
        }
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main_menu)
        Prefs.Builder()
            .setContext(this)
            .setMode(ContextWrapper.MODE_PRIVATE)
            .setPrefsName(packageName)
            .setUseDefaultSharedPreference(true)
            .build()
//
        Prefs.putBoolean(keys.SHOWINGDIALOG, false)
        val filter = IntentFilter()
        filter.addAction(ConnectivityManager.CONNECTIVITY_ACTION)
        mReceiver = MyReceiver()
        registerReceiver(mReceiver, filter)

        Prefs.getBoolean(keys.SHOWINGDIALOG, false)

        val userInfoString = Prefs.getString(keys.USER_FULL_DATA, "")

        val userProfileData: UserProfileData = mGson.fromJson(userInfoString, UserProfileData::class.java)

        if (userProfileData.information!!.age_range == "") {
            val ft1 = supportFragmentManager.beginTransaction()
            val dialogFragment1 = ProfileCreateDialogFragment(this)
            dialogFragment1.isCancelable = false
            dialogFragment1.show(ft1, "profile")
            Prefs.putBoolean(keys.SHOWINGDIALOG, true)
        }

        FirebaseApp.initializeApp(applicationContext)

        FirebaseInstanceId.getInstance().instanceId.addOnSuccessListener(
            this
        ) { instanceIdResult ->
            newToken = instanceIdResult.token
            Log.e("newToken", newToken)
            registerNotif(newToken!!)
        }



        startService(Intent(applicationContext, BluetoothScannerService::class.java))
        mAppBeaconManager = AppBeaconManager(this.applicationContext)

        mBluetoothDeviceReceiver = BluetoothDeviceReceiver()

        mAppBeaconManager!!.setBeaconConsumer(this)

//turn off beacon jan 20
//        startBluetoothScanner()

        ActivityCompat.requestPermissions(
            this@MainMenuActivity,
            arrayOf(Manifest.permission.BLUETOOTH),
            110
        )

        btn_redeem_home.setOnClickListener {

            main_lay_games.visibility = View.GONE
            main_lay_redem.visibility = View.VISIBLE
            Prefs.putBoolean(keys.SHOWINGDIALOG, false)
            game_icon.setColorFilter(getColor(R.color.unselected))
            redeem_icon.setColorFilter(getColor(R.color.blue_bottom))
            game_txt.setTextColor(getColor(R.color.unselected))
            redeem_txt.setTextColor(getColor(R.color.blue_bottom))

            callGetPrizes()



        }



        btn_game_code.setOnClickListener {

            val ft = supportFragmentManager.beginTransaction()
            val dialogFragment = RedeemCreditsDialogFragment(this)
            dialogFragment.isCancelable = false
            dialogFragment.show(ft, "redeem")
            Prefs.putBoolean(keys.SHOWINGDIALOG, true)

        }

        btn_game_home.setOnClickListener {

            main_lay_games.visibility = View.VISIBLE
            main_lay_redem.visibility = View.GONE
            Prefs.putBoolean(keys.SHOWINGDIALOG, false)
            redeem_icon.setColorFilter(getColor(R.color.unselected))
            game_icon.setColorFilter(getColor(R.color.blue_bottom))
            redeem_txt.setTextColor(getColor(R.color.unselected))
            game_txt.setTextColor(getColor(R.color.blue_bottom))
        }



        btn_drawer.setOnClickListener {

            toggle()

        }

        btn_game_one.setOnClickListener {


            val ft = supportFragmentManager.beginTransaction()
            val dialogFragment = ArielMainDialogFragment(this)
            dialogFragment.isCancelable = false
            dialogFragment.show(ft, "redeem")
            Prefs.putBoolean(keys.SHOWINGDIALOG, true)
            Prefs.putString(keys.SELECTED_GAME, "ariel")

        }

        btn_game_two.setOnClickListener {

            val ft = supportFragmentManager.beginTransaction()
            val dialogFragment = PanteneDialogFragment(this)
            dialogFragment.isCancelable = false
            dialogFragment.show(ft, "redeem")
            Prefs.putBoolean(keys.SHOWINGDIALOG, true)
            Prefs.putString(keys.SELECTED_GAME, "pantene")
        }


        btn_game_tree.setOnClickListener {

            val ft = supportFragmentManager.beginTransaction()
            val dialogFragment = LuckeyDialogFragment(this)
            dialogFragment.isCancelable = false
            dialogFragment.show(ft, "redeem")
            Prefs.putBoolean(keys.SHOWINGDIALOG, true)
            Prefs.putString(keys.SELECTED_GAME, "lucky")

        }

        btn_story_one.setOnClickListener {


            val intent = Intent(applicationContext, StoryActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
            intent.putExtra("STORY", "one")
            applicationContext.startActivity(intent)

            overridePendingTransition(
                R.anim.enterfrom,
                R.anim.enterto
            )
            Prefs.putBoolean(keys.SHOWINGDIALOG, true)

        }

        btn_story_two.setOnClickListener {


            val intent = Intent(applicationContext, StoryActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
            intent.putExtra("STORY", "two")
            applicationContext.startActivity(intent)
            overridePendingTransition(
                R.anim.enterfrom,
                R.anim.enterto
            )
            Prefs.putBoolean(keys.SHOWINGDIALOG, true)

        }
        btn_story_tree.setOnClickListener {


            val intent = Intent(applicationContext, StoryActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
            intent.putExtra("STORY", "tree")
            applicationContext.startActivity(intent)
            overridePendingTransition(
                R.anim.enterfrom,
                R.anim.enterto
            )
            Prefs.putBoolean(keys.SHOWINGDIALOG, true)

        }


        log_out.setOnClickListener {

            val builder = AlertDialog.Builder(this)
            builder.setMessage("Are you sure you want to logout?")
            builder.setPositiveButton("YES") { dialog, which ->
                Prefs.clear()
                finish()
                // context!!.finish()
                val intent = Intent(applicationContext, LoginActivity::class.java)
                intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK and Intent.FLAG_ACTIVITY_NEW_TASK
                startActivity(intent)
                overridePendingTransition(R.anim.enterfrom, R.anim.enterto)

                dialog.dismiss()

            }
            builder.setNegativeButton("No") { dialog, which ->
                dialog.dismiss()
            }
            val dialog: AlertDialog = builder.create()
            dialog.show()

            toggle()

        }
        drawer_home.setOnClickListener {
            mainLay.visibility = View.VISIBLE
            container.visibility = View.GONE

            toggle()

        }

        drawer_profile.setOnClickListener {
            val ft1 = supportFragmentManager.beginTransaction()
            val dialogFragment1 = ProfileViewInfoFragment()
            dialogFragment1.isCancelable = false
            dialogFragment1.show(ft1, "profile")
            toggle()
            Prefs.putBoolean(keys.SHOWINGDIALOG, true)
        }

        callBeacon()




        faq_drawer.setOnClickListener {
            mainLay.visibility = View.GONE
            container.visibility = View.VISIBLE
            supportFragmentManager.beginTransaction()
                .replace(R.id.container, FaqFragment(), "search")
                .commit()
            toggle()
        }
        how_to_drawer.setOnClickListener {

            val ft = supportFragmentManager.beginTransaction()
            val dialogFragment = WelcomeDialogFragment()
            dialogFragment.isCancelable = false
            dialogFragment.show(ft, "redeem")
            toggle()
            Prefs.putBoolean(keys.SHOWINGDIALOG, true)
        }

        help_drawer.setOnClickListener {

            val intent = Intent(
                Intent.ACTION_VIEW,
                Uri.parse("mailto:" + "help@expee.com.ph")
            )
            intent.putExtra(Intent.EXTRA_SUBJECT, "Help me.")
            startActivity(Intent.createChooser(intent, ""))
            toggle()
        }

        updateCreditsAPI()
        rv_prizes.layoutManager = GridLayoutManager(applicationContext, 2) as RecyclerView.LayoutManager?
        rv_prizes.addItemDecoration(GridSpacingItemDecoration(2, 25, true))
        callGetPrizes()



        swipe_prizes.setOnRefreshListener {

            callGetPrizes()

        }

        refresh_credits.setOnRefreshListener {
            updateCreditsAPI()





        }

    }

    fun updateCreditsCount() {
        pantene_credits_count.text = Prefs.getString(keys.CREDITS_PANTENE, "0")
        ariel_credits_count.text = Prefs.getString(keys.CREDITS_ARIEL, "0")
        lucky_credits_count.text = Prefs.getString(keys.CREDITS_LUCKYME, "0")

    }

    override fun onResume() {
        super.onResume()

        mBluetoothStateReceiver = BluetoothStateReceiver()
        mBluetoothDeviceReceiver = BluetoothDeviceReceiver()

        if (mAppBeaconManager!!.beaconManager.isBound(this)) {

            mAppBeaconManager!!.startRangingAndMonitoringInRegion()

        }
        try {
            val redeem = Prefs.getBoolean(keys.TO_REDEEM, false)
            if (redeem) {
                main_lay_games.visibility = View.GONE
                main_lay_redem.visibility = View.VISIBLE

                game_icon.setColorFilter(getColor(R.color.unselected))
                redeem_icon.setColorFilter(getColor(R.color.blue_bottom))
                game_txt.setTextColor(getColor(R.color.unselected))
                redeem_txt.setTextColor(getColor(R.color.blue_bottom))
                Prefs.putBoolean(keys.TO_REDEEM, false)
                callGetPrizes()

            }
        } catch (e: Exception) {

        }

        updateCreditsAPI()
    }


    fun doneStory(done: Boolean) {

        showingStory = false

    }

    fun toggle() {
        if (drawerLayout.isDrawerVisible(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START);
        } else {
            drawerLayout.openDrawer(GravityCompat.START);
        }
    }


    //get beacon information through API
    fun callBeacon() {
        var token: String = Prefs.getString(keys.USER_TOKEN, "");
        val apiService = UserService.create(this.getString(R.string.base_url))
        val call = apiService.getBeaconNewApi(
            1,
            "Bearer " + token
        )

        call.enqueue(object : Callback<BeaconMainResponse> {

            override fun onResponse(call: Call<BeaconMainResponse>, response: Response<BeaconMainResponse>) {

                if (response.code() == 200) {

                    var beaconListDataList: List<BeaconListData>? = response.body()!!.responseData


                    if (beaconListDataList != null) {
                        beaconListDataList.indices.forEach { x ->
                            val beaconDetail = beaconListDataList[x]
                            beaconDetail.mRegion = Region(
                                beaconDetail.name,
                                Identifier.fromUuid(UUID.fromString(beaconDetail.uuid)),
                                Identifier.fromInt(beaconDetail.major),
                                Identifier.fromInt(beaconDetail.minor)
                            )

                            beaconListDataList.indexOf(beaconDetail)
//                            beaconListDataList
//                            beaconListDataList[x].mRegion
                        }

                        AppBeaconManager.setBeaconDetailList(beaconListDataList as MutableList<BeaconListData>)

                        //  beaconImageDownload(view)

                    }

                    Log.d("afd", "a")


                }
            }

            override fun onFailure(call: Call<BeaconMainResponse>, t: Throwable) {

                Log.e("aa", t.toString());


            }

        })
    }


    //get all game prizes
    fun callGetPrizes() {
        var token: String = Prefs.getString(keys.USER_TOKEN, "");
        val apiService = UserService.create(this.getString(R.string.base_url))
        val call = apiService.getPrizes(
            "application/json",
            "Bearer $token"
        )

        call.enqueue(object : Callback<PrizeAllResponse> {

            override fun onResponse(call: Call<PrizeAllResponse>, response: Response<PrizeAllResponse>) {

                if (response.code() == 200) {
                    val prizeAllResponse: PrizeAllResponse? = response.body()


                    val listUnclaimedPrize: MutableList<PrizeInfo> = ArrayList()
                    if (prizeAllResponse!!.responseData!!.dataList.size > 0) {

                        for (prize: PrizeInfo in prizeAllResponse!!.responseData!!.dataList) {

                            if (prize.status == "unclaimed") {
                                listUnclaimedPrize.add(prize)
                            }
                        }

                        prizeAdapter = PrizeAdapter(listUnclaimedPrize, this@MainMenuActivity)
                        rv_prizes.adapter = prizeAdapter
                    }
                }

                if (swipe_prizes.isRefreshing) {
                    swipe_prizes.isRefreshing = false
                }
            }

            override fun onFailure(call: Call<PrizeAllResponse>, t: Throwable) {

                Log.e("aa", t.toString());
                if (swipe_prizes.isRefreshing) {
                    swipe_prizes.isRefreshing = false
                }

            }

        })
    }


    fun updateCreditsAPI() {

        var userid: String = Prefs.getString(keys.USER_ID, "");
        var token: String = Prefs.getString(keys.USER_TOKEN, "");
        val apiService = UserService.create(this.getString(R.string.base_url))


        val call = apiService.updateCredits(
            userid, "PUT", "application/json",
            "Bearer $token"
        )

        call.enqueue(object : Callback<UpdateCreditsResponse> {

            override fun onResponse(call: Call<UpdateCreditsResponse>, response: Response<UpdateCreditsResponse>) {

                if (response.code() == 200) {

                    val userResponse: UserProfileData? = response.body()!!.userProfileData


                     val gameCredsList = userResponse!!.game_credits


                    if (gameCredsList!!.size > 0) {

                        for (game: GameCredits in gameCredsList) {

                            if (game.game_id == "1") {
                                var totalCreds = game.credit!!.toInt() - game.used!!.toInt()
                                Prefs.putString(keys.CREDITS_ARIEL, totalCreds.toString())
                            } else if (game.game_id == "2") {
                                var totalCreds = game.credit!!.toInt() - game.used!!.toInt()
                                Prefs.putString(keys.CREDITS_PANTENE, totalCreds.toString())
                            } else if (game.game_id == "3") {
                                var totalCreds = game.credit!!.toInt() - game.used!!.toInt()
                                Prefs.putString(keys.CREDITS_LUCKYME, totalCreds.toString())
                            }
                        }
                    } else {
                        Prefs.putString(keys.CREDITS_LUCKYME, "0")
                        Prefs.putString(keys.CREDITS_PANTENE, "0")
                        Prefs.putString(keys.CREDITS_ARIEL, "0")
                    }

                }
                if (refresh_credits.isRefreshing) {
                    refresh_credits.isRefreshing = false
                }
            }

            override fun onFailure(call: Call<UpdateCreditsResponse>, t: Throwable) {
                Log.e("aa", t.toString());
                if (refresh_credits.isRefreshing) {
                    refresh_credits.isRefreshing = false
                }
            }

        })

        updateCreditsCount()

    }



    // check internet connection
    inner class MyReceiver : BroadcastReceiver() {

        override fun onReceive(context: Context, intent: Intent) {
            //    do something
            val isConnected = intent.getBooleanExtra(ConnectivityManager.EXTRA_NO_CONNECTIVITY, false)
            if (isConnected) {
                // player.playWhenReady = false

                notconnected.visibility = View.VISIBLE

                //  displayDialog("No internet connection.")
//                internet_connected.visibility = View.GONE
            } else {

                InternetCheck(object : InternetCheck.Consumer {
                    override fun accept(internet: Boolean?) {

                        if (internet!!) {

                            runOnUiThread(java.lang.Runnable {
                                //  player.playWhenReady = true
                                //  player.playbackState
                                //  internet_disconnect.visibility = View.GONE
                                //  internet_connected.visibility = View.VISIBLE
                                notconnected.visibility = View.GONE
                                connected.visibility = View.VISIBLE
                                val timer = Timer()
                                timer.schedule(timerTask {
                                    runOnUiThread(Runnable {
                                        //    internet_connected.visibility = View.GONE
                                        connected.visibility = View.GONE
                                    })
                                }, 2500)

                            })

                        } else {
                            //  player.playWhenReady = false
                            //  internet_disconnect.visibility = View.VISIBLE

                            Toast.makeText(applicationContext, "Slow internet connection", Toast.LENGTH_SHORT).show()

                            //internet_connected.visibility = View.GONE
                        }

                    }
                })


            }
        }

    }

    internal class InternetCheck(private val mConsumer: Consumer) :
        AsyncTask<Void, Void, Boolean>() {
        interface Consumer {
            fun accept(internet: Boolean?)
        }

        init {
            execute()
        }

        override fun doInBackground(vararg voids: Void): Boolean? {
            try {
                val sock = Socket()
                sock.connect(InetSocketAddress("8.8.8.8", 53) as SocketAddress?, 1500)
                sock.close()
                return true
            } catch (e: IOException) {
                return false
            }
        }

        override fun onPostExecute(internet: Boolean?) {
            mConsumer.accept(internet)
        }
    }

    fun registerNotif(device_toke: String) {

        mGson = GsonBuilder()
            .setLenient()
            .create()

//        val client = OkHttpClient.Builder()
//            .connectTimeout(Time.WEB_SERVICE_TIMEOUT, TimeUnit.SECONDS)
//            .writeTimeout(Time.WEB_SERVICE_TIMEOUT, TimeUnit.SECONDS)
//            .readTimeout(Time.WEB_SERVICE_TIMEOUT, TimeUnit.SECONDS)
//            .build()

        val retrofit = Retrofit.Builder()
            .baseUrl(getString(R.string.base_url))
            .addConverterFactory(ScalarsConverterFactory.create())
            .addConverterFactory(GsonConverterFactory.create(mGson))
         //   .client(client)
            .build()


        val service = retrofit.create(UserService::class.java)
        val token = Prefs.getString(keys.USER_TOKEN, "0")
        service.registerToken(device_toke, "android", "PUT", "application/json", "Bearer $token")
            .enqueue(object : Callback<Object> {
                override fun onResponse(call: Call<Object>, response: Response<Object>) {
                    if (response.code() == 200) {

                        Log.e("newT", "register")

                    } else {

                        Log.e("newTo", "not register ")
                    }
                }

                override fun onFailure(call: Call<Object>, t: Throwable) {
                    Log.e("newT", "not register err")
                    if (t.message == "timeout") {

                        registerNotif(device_toke)

                    }
                }
            })

    }

    fun unRegisterNotif(device_token: String) {
        mGson = GsonBuilder()
            .setLenient()
            .create()

//        val client = OkHttpClient.Builder()
//            .connectTimeout(Time.WEB_SERVICE_TIMEOUT, TimeUnit.SECONDS)
//            .writeTimeout(Time.WEB_SERVICE_TIMEOUT, TimeUnit.SECONDS)
//            .readTimeout(Time.WEB_SERVICE_TIMEOUT, TimeUnit.SECONDS)
//            .build()

        val retrofit = Retrofit.Builder()
            .baseUrl(getString(R.string.base_url))
            .addConverterFactory(ScalarsConverterFactory.create())
            .addConverterFactory(GsonConverterFactory.create(mGson))
          //  .client(client)
            .build()


        val service = retrofit.create(UserService::class.java)
        val token = Prefs.getString(keys.USER_TOKEN, "0")
        service.unRegisterToken(device_token, "PUT", "application/json", "Bearer $token")
            .enqueue(object : Callback<Object> {
                override fun onResponse(call: Call<Object>, response: Response<Object>) {
                    if (response.code() == 200) {

                        Log.e("newTo", "unregister")

                    } else {

                        Log.e("newTo", "not unregister ")
                    }
                }

                override fun onFailure(call: Call<Object>, t: Throwable) {
                    Log.e("newT", "not unregister err")
                    if (t.message == "timeout") {


                    }
                }
            })

    }
}
