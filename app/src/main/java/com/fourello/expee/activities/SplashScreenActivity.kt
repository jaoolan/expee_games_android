package com.fourello.expee.activities

import android.content.ContextWrapper
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.fourello.expee.R
import com.fourello.expee.constant.keys
import com.pixplicity.easyprefs.library.Prefs
import java.util.*
import kotlin.concurrent.timerTask

class SplashScreenActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash_screen)

        initSplash()
    }

    fun initSplash(){
        Prefs.Builder()
            .setContext(this)
            .setMode(ContextWrapper.MODE_PRIVATE)
            .setPrefsName(packageName)
            .setUseDefaultSharedPreference(true)
            .build()

        runOnUiThread {
            val timer = Timer()
            timer.schedule(timerTask {

                val apiToken = Prefs.getString(keys.USER_TOKEN, "")


                // check if token is still existing

                if (apiToken == "") {
                    //    runOnUiThread {
                    val intent = Intent(applicationContext, LoginActivity::class.java)
                    intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                    applicationContext.startActivity(intent)
                    overridePendingTransition(
                        R.anim.fadein,
                        R.anim.fadeout
                    )
                    //    }
                    finish()
                } else {
                    //      runOnUiThread {
                    val intent = Intent(applicationContext, MainMenuActivity::class.java)
                    intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                    applicationContext.startActivity(intent)
                    overridePendingTransition(
                        R.anim.fadein,
                        R.anim.fadeout
                    )

                    finish()
                }
            }, 2000)
        }
    }
}
