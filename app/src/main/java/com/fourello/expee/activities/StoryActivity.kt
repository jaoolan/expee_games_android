package com.fourello.expee.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.WindowManager
import kotlinx.android.synthetic.main.activity_story.*
import android.os.CountDownTimer
import android.widget.ProgressBar
import android.util.Log
import androidx.core.content.ContextCompat
import androidx.viewpager.widget.ViewPager
import com.ToxicBakery.viewpager.transforms.CubeOutTransformer

import com.fourello.expee.adapter.SlidingImage_Adapter
import com.fourello.expee.model.ImageModel
import java.util.ArrayList


class StoryActivity : AppCompatActivity() {
    private var imageModelArrayList: ArrayList<ImageModel>? = null

    private val myImageList = intArrayOf(
        com.fourello.expee.R.drawable.story_one_full, com.fourello.expee.R.drawable.story_two_full,
        com.fourello.expee.R.drawable.story_tree_full)

    private val myProfileList = intArrayOf(
        com.fourello.expee.R.drawable.pantene_logo, com.fourello.expee.R.drawable.story_two_thumb,
        com.fourello.expee.R.drawable.story_tree_thumb)

    private val myNameList = listOf("Pantene","Ariel","Lucky Me!")

    var mainCounter = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(com.fourello.expee.R.layout.activity_story)
        window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN)


        imageModelArrayList = ArrayList()
        imageModelArrayList = populateList()




        vp_story!!.adapter = SlidingImage_Adapter(this@StoryActivity, this.imageModelArrayList!!)
        vp_story!!.setPageTransformer(true, CubeOutTransformer())

        var bundle :Bundle ?=intent.extras
        var message = bundle!!.getString("STORY") // 1

        if(message == "one"){
            vp_story!!.currentItem = 0
        }else if(message == "two"){
            mainCounter = 0
            vp_story!!.currentItem = 1

            brand_profile.setImageDrawable(ContextCompat.getDrawable(applicationContext,myProfileList[1]))
            brand_title.text = myNameList[1]
        }else if(message == "tree"){
            mainCounter = 1
            brand_profile.setImageDrawable(ContextCompat.getDrawable(applicationContext,myProfileList[2]))
            brand_title.text = myNameList[2]
            vp_story!!.currentItem = 2
        }


        val mProgressBar: ProgressBar
        val mCountDownTimer: CountDownTimer
        var i = 0

        mProgressBar = findViewById<ProgressBar>(com.fourello.expee.R.id.progress)
        mProgressBar.progress = i
        mCountDownTimer = object : CountDownTimer(5000, 100) {

            override fun onTick(millisUntilFinished: Long) {
                Log.v("Log_tag", "Tick of Progress$i$millisUntilFinished")
                i++
                mProgressBar.progress = i * 100 / (5000 / 100)

            }

            override fun onFinish() {
                //Do what you want

                mainCounter = mainCounter + 1
                if(vp_story!!.currentItem == 2){
                    finish()
                }else {
                    vp_story!!.currentItem = mainCounter
                }

                mProgressBar.progress = 100






                //  finish()
                overridePendingTransition(com.fourello.expee.R.anim.enterfrom, com.fourello.expee.R.anim.enterto)
            }
        }
        mCountDownTimer.start()


        vp_story?.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {

            override fun onPageScrollStateChanged(state: Int) {
            }

            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {

            }
            override fun onPageSelected(position: Int) {
                brand_profile.setImageDrawable(ContextCompat.getDrawable(applicationContext,myProfileList[position]))
               brand_title.text = myNameList[position]
                mCountDownTimer.cancel()
                i = 0
                mProgressBar.progress = 0
                mCountDownTimer.start()


            }

        })

        close_story.setOnClickListener {

            finish()
            overridePendingTransition(
                com.fourello.expee.R.anim.exitfrom,
                com.fourello.expee.R.anim.exitto
            )

        }
    }

    private fun populateList(): ArrayList<ImageModel> {

        val list = ArrayList<ImageModel>()

        for (i in 0..2) {
            val imageModel = ImageModel()
            imageModel.setImage_drawables(myImageList[i])
            list.add(imageModel)
        }

        return list
    }
}
