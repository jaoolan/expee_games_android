package com.fourello.expee.activities

import android.annotation.SuppressLint
import android.content.*
import android.graphics.Bitmap
import android.graphics.PorterDuff
import android.net.ConnectivityManager
import android.os.AsyncTask
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_game.*
import android.widget.ProgressBar
import android.view.View
import android.webkit.*
import android.widget.Toast
import androidx.core.content.ContextCompat
import com.fourello.expee.constant.keys
import com.pixplicity.easyprefs.library.Prefs
import androidx.appcompat.app.AlertDialog
import com.fourello.expee.R
import com.fourello.expee.fragmentdialog.TryAgainDialogFragment
import com.fourello.expee.fragmentdialog.WonDialogFragment
import java.io.IOException
import java.net.InetSocketAddress
import java.net.Socket
import java.net.SocketAddress
import java.util.*
import kotlin.concurrent.timerTask


class GameActivity : AppCompatActivity() {
    internal lateinit var mReceiver: BroadcastReceiver
    @SuppressLint("SetJavaScriptEnabled", "ObsoleteSdkInt")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(com.fourello.expee.R.layout.activity_game)

        val filter = IntentFilter()
        filter.addAction(ConnectivityManager.CONNECTIVITY_ACTION)
        mReceiver = MyReceiver()
        registerReceiver(mReceiver, filter)

        Prefs.Builder()
            .setContext(this)
            .setMode(ContextWrapper.MODE_PRIVATE)
            .setPrefsName(packageName)
            .setUseDefaultSharedPreference(true)
            .build()


        //Create url based on winCode
        //wincode rule
        // 1 = grand prize
        // 2 and 3 = consolation
        // 4 , 5 and 6 = sorry try again

        val seleceted = Prefs.getString(keys.SELECTED_GAME, "")
        var gameUrl = getString(com.fourello.expee.R.string.game_main_url)
        val winCode = Prefs.getString(keys.WINNER_CODE,"6");
        //http://files.taktylstudios.com/projects/fourello/expee-wheel/ariel/0.1.0/?result=1
        if (seleceted == "ariel") {

            val intent = Intent(applicationContext, StoryTwoActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
            intent.putExtra("STORY", "tree")
            applicationContext.startActivity(intent)
            overridePendingTransition(R.anim.enterfrom, R.anim.enterto)

            gameUrl = gameUrl + "ariel/"+ "0.1.0/?result=" + winCode
        } else if (seleceted == "pantene") {
            val intent = Intent(applicationContext, StoryOneActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
            intent.putExtra("STORY", "tree")
            applicationContext.startActivity(intent)
            overridePendingTransition(R.anim.enterfrom, R.anim.enterto)
            gameUrl = gameUrl + "pantene/"+ "0.1.0/?result=" + winCode

        } else if (seleceted == "lucky") {
            val intent = Intent(applicationContext, StoryTreeActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
            intent.putExtra("STORY", "tree")
            applicationContext.startActivity(intent)
            overridePendingTransition(R.anim.enterfrom, R.anim.enterto)

            gameUrl = gameUrl + "luckyme/"+ "0.1.0/?result=" + winCode


        }


        val mywebview = findViewById<WebView>(com.fourello.expee.R.id.webview)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            // chromium, enable hardware acceleration
            mywebview.setLayerType(View.LAYER_TYPE_HARDWARE, null);
        } else {
            // older android version, disable hardware acceleration
            mywebview.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
        }
        mywebview!!.settings.javaScriptEnabled = true
        mywebview.webViewClient = object : WebViewClient() {
            override fun shouldOverrideUrlLoading(view: WebView?, url: String?): Boolean {
                view?.loadUrl(url)
                return true
            }
        }

        mywebview.loadUrl(gameUrl)

        back_game.setOnClickListener {
            Prefs.putBoolean(keys.SHOWINGDIALOG,false)
            finish()
            overridePendingTransition(com.fourello.expee.R.anim.exitfrom, com.fourello.expee.R.anim.exitto)

        }
        //  val txtview =findViewById(com.fourello.expee_.R.id.tV1);
        //  final ProgressBar pbar = (ProgressBar) findViewById(com.fourello.expee_.R.id.pB1);
        pB1.indeterminateDrawable
            .setColorFilter(
                ContextCompat.getColor(this, com.fourello.expee.R.color.blue_bottom),
                PorterDuff.Mode.SRC_IN
            )

        mywebview!!.webChromeClient = object : WebChromeClient() {
            override fun onProgressChanged(view: WebView, progress: Int) {
                if (progress < 100 && pB1.visibility === ProgressBar.GONE) {
                    pB1.visibility = ProgressBar.VISIBLE
                    tV1.visibility = View.VISIBLE
                }

                //pB1.setProgress(progress)
                if (progress == 100) {
                    pB1.setVisibility(ProgressBar.GONE)
                    tV1.setVisibility(View.GONE)
                    instruct.visibility = View.VISIBLE
                    logo_exp.visibility = View.VISIBLE
                }



            }


            // determine if spin was ended and display dialog based on winCode
            override fun onConsoleMessage(consoleMessage: ConsoleMessage): Boolean {
                android.util.Log.d("WebViewJAO", consoleMessage.message())

                if (consoleMessage.message().contains("Spin complete")) {
                    val winCode = Prefs.getString(keys.WINNER_CODE,"6");
                    if(winCode == "1" || winCode == "2" || winCode == "3" ){
                        val ft = supportFragmentManager.beginTransaction()
                        val dialogFragment = WonDialogFragment(seleceted,winCode, this@GameActivity)
                        dialogFragment.isCancelable = false
                        dialogFragment.show(ft, "redeem")


                    }else{
                        val ft = supportFragmentManager.beginTransaction()
                        val dialogFragment = TryAgainDialogFragment(this@GameActivity)
                        dialogFragment.isCancelable = false
                        dialogFragment.show(ft, "redeem")
                    }


                }

                return true
            }
        }
        mywebview!!.webViewClient = object : WebViewClient() {

            override fun onPageStarted(view: WebView?, url: String?, favicon: Bitmap?) {
//                progressBar.visibility = View.VISIBLE
//                progressBar.progress = 0;
                super.onPageStarted(view, url, favicon)
            }

            override fun shouldOverrideUrlLoading(view: WebView?, url: String?): Boolean {
                view?.loadUrl(url)
                return true
            }

            override fun shouldOverrideUrlLoading(
                view: WebView?,
                request: WebResourceRequest?): Boolean {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    view?.loadUrl(request?.url.toString())
                }
                return true
            }

            override fun onPageFinished(view: WebView?, url: String?) {
                super.onPageFinished(view, url)
               // progressBar.visibility = View.GONE

                pB1.setVisibility(ProgressBar.GONE)
                tV1.setVisibility(View.GONE)
                instruct.visibility = View.VISIBLE
                logo_exp.visibility = View.VISIBLE
            }
        }

    }
    override fun onDestroy() {
        super.onDestroy()
        Prefs.putBoolean(keys.SHOWINGDIALOG,false)
    }
    fun displayDialog(message: String) {
        val builder = AlertDialog.Builder(this)
        builder.setMessage(message)
        builder.setPositiveButton("Ok") { dialog, _ ->
            dialog.dismiss()
        }
        val dialog: AlertDialog = builder.create()
        dialog.show()
    }

    inner class MyReceiver : BroadcastReceiver() {

        override fun onReceive(context: Context, intent: Intent) {
            //    do something
            val isConnected = intent.getBooleanExtra(ConnectivityManager.EXTRA_NO_CONNECTIVITY, false)
            if (isConnected) {
                // player.playWhenReady = false
                displayDialog("No internet connection.")
//                internet_connected.visibility = View.GONE
            } else {

                InternetCheck(object : InternetCheck.Consumer {
                    override fun accept(internet: Boolean?) {

                        if (internet!!) {

                            runOnUiThread(java.lang.Runnable {
                                //  player.playWhenReady = true
                                //  player.playbackState
                                //  internet_disconnect.visibility = View.GONE
                                //  internet_connected.visibility = View.VISIBLE

                                val timer = Timer()
                                timer.schedule(timerTask {
                                    runOnUiThread(Runnable {
                                        //    internet_connected.visibility = View.GONE

                                    })
                                }, 2500)

                            })

                        } else {
                            //  player.playWhenReady = false
                            //  internet_disconnect.visibility = View.VISIBLE

                            Toast.makeText(applicationContext,"Slow internet connection", Toast.LENGTH_SHORT).show()

                            //internet_connected.visibility = View.GONE
                        }

                    }
                })


            }
        }

    }

    internal class InternetCheck(private val mConsumer: Consumer) :
        AsyncTask<Void, Void, Boolean>() {
        interface Consumer {
            fun accept(internet: Boolean?)
        }

        init {
            execute()
        }

        override fun doInBackground(vararg voids: Void): Boolean? {
            try {
                val sock = Socket()
                sock.connect(InetSocketAddress("8.8.8.8", 53) as SocketAddress?, 1500)
                sock.close()
                return true
            } catch (e: IOException) {
                return false
            }
        }

        override fun onPostExecute(internet: Boolean?) {
            mConsumer.accept(internet)
        }
    }

    override fun onBackPressed() {
      //  super.onBackPressed()


    }
}
