package com.fourello.expee.activities

import android.Manifest
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.content.pm.PackageManager
import android.content.res.Configuration
import android.graphics.*
import android.hardware.camera2.*
import android.media.ExifInterface
import android.media.Image
import android.media.ImageReader
import android.net.ConnectivityManager
import android.net.Uri
import android.os.*
import androidx.appcompat.app.AppCompatActivity
import android.util.Log
import android.util.Size
import android.view.Surface
import android.view.TextureView
import android.view.View
import android.view.WindowManager
import android.widget.Button
import android.widget.FrameLayout
import android.widget.Toast
import androidx.annotation.NonNull
import androidx.appcompat.app.AlertDialog
import androidx.core.app.ActivityCompat
import com.fourello.expee.R
import com.fourello.expee.constant.keys
import com.fourello.expee.fragmentdialog.FailedCheckOutDialogFragment
import com.fourello.expee.model.CheckoutFailedMessage
import com.fourello.expee.model.GameCheckoutResponse
import com.fourello.expee.model.SelectWinnerFailed
import com.fourello.expee.model.SelectWinnerWon
import com.fourello.expee.webservice.UserService
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.pixplicity.easyprefs.library.Prefs
import kotlinx.android.synthetic.main.activity_scanner.*
import kotlinx.android.synthetic.main.activity_scanner.step_tree_lay
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import org.json.JSONObject
import retrofit2.Call
import java.io.*
import java.lang.Exception
import java.net.InetSocketAddress
import java.net.Socket
import java.net.SocketAddress
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList
import kotlin.concurrent.timerTask




// ignore this, this is from last version
class ScannerActivity : AppCompatActivity() {

    private val TAG = "AndroidCameraApi"
    private var takePictureButton: Button? = null
    private var textureView: TextureView? = null
//    private val ORIENTATIONS = SparseIntArray() = ArrayList<SparseIntArray>(
//        ORIENTATIONS.append(Surface.ROTATION_0, 90))
//
//
////    static
////    {
////        ORIENTATIONS.append(Surface.ROTATION_0, 90);
////        ORIENTATIONS.append(Surface.ROTATION_90, 0);
////        ORIENTATIONS.append(Surface.ROTATION_180, 270);
////        ORIENTATIONS.append(Surface.ROTATION_270, 180);
////    }


    private var cameraId: String? = null
    protected var cameraDevice: CameraDevice? = null
    protected lateinit var cameraCaptureSessions: CameraCaptureSession
    protected var captureRequest: CaptureRequest? = null
    protected lateinit var captureRequestBuilder: CaptureRequest.Builder
    private var imageDimension: Size? = null
    private var imageReader: ImageReader? = null
    private var mFile: File? = null
    private var mFileSI: File? = null

    private val REQUEST_CAMERA_PERMISSION = 200
    private val mFlashSupported: Boolean = false
    private var mBackgroundHandler: Handler? = null
    private var mBackgroundThread: HandlerThread? = null
    private lateinit var previewSize: Size
    private var mSIBitmap: Bitmap? = null
    private var mReceiptOneBitmap: Bitmap? = null
    private var mReceiptTwoBitmap: Bitmap? = null
    private var mReceiptTreeBitmap: Bitmap? = null
    private var mReceiptFourBitmap: Bitmap? = null

    private var bolSI: Boolean = false
    var ifStepTwo = false
    var ifStepTree = false
    var connected = false
    internal lateinit var mReceiver: BroadcastReceiver
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_scanner)

        window.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN
        )

        val filter = IntentFilter()
        filter.addAction(ConnectivityManager.CONNECTIVITY_ACTION)
        mReceiver = MyReceiver()
        registerReceiver(mReceiver, filter)

        textureView = findViewById(R.id.texture)

        textureView!!.surfaceTextureListener = textureListener


        val seleceted = Prefs.getString(keys.SELECTED_GAME, "")
        var gameCode = getString(com.fourello.expee.R.string.game_main_url)

        //http://files.taktylstudios.com/projects/fourello/expee-wheel/ariel/0.1.0/?result=1
        if (seleceted == "ariel") {
            gameCode = "ariel_spin_the_wheel"
        } else if (seleceted == "pantene") {
            gameCode = "pantene_spin_the_wheel"

        } else if (seleceted == "lucky") {
            gameCode = "lucky_me_spin_the_wheel"
        }

        btn_capture_camera.setOnClickListener {

            if (!ifStepTwo) {
                after_capture_lay.visibility = View.VISIBLE
                btn_retry_image.visibility = View.VISIBLE
                btn_add_image.visibility = View.GONE
                btn_capture_camera.visibility = View.GONE

                takePicture()

            } else {
                btn_retry_image.visibility = View.GONE
                btn_add_image.visibility = View.VISIBLE
                after_capture_lay.visibility = View.VISIBLE
                btn_capture_camera.visibility = View.GONE
                counter_lay.visibility = View.VISIBLE
                takePicture()

                var count = pic_counter.text.toString().toInt()
                count = count + 1

                pic_counter.text = count.toString()



            }
        }


        btn_retry_image.setOnClickListener {
            //mImageView.setImageBitmap(null)
            after_capture_lay.visibility = View.GONE
            btn_capture_camera.visibility = View.VISIBLE
            openCamera()
        }

        btn_add_image.setOnClickListener {
            if(pic_counter.text.toString() != "4") {
                after_capture_lay.visibility = View.GONE
                btn_capture_camera.visibility = View.VISIBLE
                openCamera()
            }else{
                displayDialog("You can only take four photos.")
            }

        }


        btn_next.setOnClickListener {
            openCamera()

            if (!ifStepTwo) {
                ifStepTwo = true
                after_capture_lay.visibility = View.GONE
                btn_capture_camera.visibility = View.VISIBLE
                layout_step_two.foreground = null
                step_two_image_check.visibility = View.VISIBLE
                txt_step_two.visibility = View.VISIBLE
                step_two_image_check.setImageDrawable(getDrawable(R.drawable.scan_step_check))
                photo_instruc.text = "Take a photo of the entire receipt. Make sure to capture the participating products. If receipt is long, take multiple photos."
                // mImageView.setImageBitmap(null)
                // mImageView.visibility = View.VISIBLE

            } else {
                // callSelectWinners()

                runOnUiThread {
                    after_capture_lay.visibility = View.GONE
                    step_tree_percentage_lay.visibility = View.VISIBLE
                    btn_capture_camera.visibility = View.GONE
                    layout_step_tree.foreground = null
                    step_tree_image_check.setImageDrawable(getDrawable(R.drawable.scan_step_check))
                    step_tree_image_check.visibility = View.VISIBLE
                    txt_step_tree.visibility = View.VISIBLE
                    step2_overlay.visibility = View.GONE
                    step_tree_lay.visibility = View.VISIBLE
                    photo_instruc.visibility = View.GONE
                    //mImageView.setImageBitmap(null)
                    val mCountDownTimer: CountDownTimer


                    var i = 0
                    var per = 0
                    mCountDownTimer = object : CountDownTimer(4000, 100) {

                        override fun onTick(millisUntilFinished: Long) {
                            Log.v("Log_tag", "Tick of Progress$i$millisUntilFinished")
                            i++

                            per = i * 100 / (4000 / 40)
                            percentage_text.text = per.toString() + "%"

                            //   mProgressBar.progress = i * 100 / (5000 / 10)

                        }

                        override fun onFinish() {
                            i++
                            percentage_text.text = "40%"
                            if(connected) {
                                callCheckout(gameCode, "")
                            }else{
                                displayDialog("No internet connection")
                            }
                            //    callCheckout("","")
                            //finish()
                        }
                    }
                    mCountDownTimer.start()


                }
            }

        }


        btn_play_game.setOnClickListener {


            val intent = Intent(applicationContext, GameActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
            applicationContext.startActivity(intent)
            overridePendingTransition(
                R.anim.enterfrom,
                R.anim.enterto
            )
            finish()

        }
    }

    override fun onDestroy() {
        super.onDestroy()
        Prefs.putBoolean(keys.SHOWINGDIALOG,false)
    }


    internal var textureListener: TextureView.SurfaceTextureListener = object : TextureView.SurfaceTextureListener {
        override fun onSurfaceTextureAvailable(surface: SurfaceTexture, width: Int, height: Int) {
            //open your camera here
            openCamera()
        }

        override fun onSurfaceTextureSizeChanged(surface: SurfaceTexture, width: Int, height: Int) {
            // Transform you image captured size according to the surface width and height
        }

        override fun onSurfaceTextureDestroyed(surface: SurfaceTexture): Boolean {
            return false
        }

        override fun onSurfaceTextureUpdated(surface: SurfaceTexture) {}
    }
    private val stateCallback = object : CameraDevice.StateCallback() {
        override fun onOpened(camera: CameraDevice) {
            //This is called when the camera is open
            Log.e(TAG, "onOpened")
            cameraDevice = camera
            createCameraPreview()
        }

        override fun onDisconnected(camera: CameraDevice) {
            cameraDevice!!.close()
        }

        override fun onError(camera: CameraDevice, error: Int) {
            cameraDevice!!.close()
            cameraDevice = null
        }
    }
    internal val captureCallbackListener: CameraCaptureSession.CaptureCallback =
        object : CameraCaptureSession.CaptureCallback() {
            override fun onCaptureCompleted(
                session: CameraCaptureSession,
                request: CaptureRequest,
                result: TotalCaptureResult
            ) {
                super.onCaptureCompleted(session, request, result)
                //   Toast.makeText(this@ScannerActivity, "Saved:$file", Toast.LENGTH_SHORT).show()
                createCameraPreview()
            }
        }


    protected fun startBackgroundThread() {
        mBackgroundThread = HandlerThread("Camera Background")
        mBackgroundThread!!.start()
        mBackgroundHandler = Handler(mBackgroundThread!!.getLooper())
    }

    protected fun stopBackgroundThread() {
        mBackgroundThread!!.quitSafely()
        try {
            mBackgroundThread!!.join()
            mBackgroundThread = null
            mBackgroundHandler = null
        } catch (e: InterruptedException) {
            e.printStackTrace()
        }

    }

    protected fun takePicture() {
        //    bolSI = fa
        if (null == cameraDevice) {
            Log.e(TAG, "cameraDevice is null")
            return
        }
        val manager = getSystemService(Context.CAMERA_SERVICE) as CameraManager
        try {
            val characteristics = manager.getCameraCharacteristics(cameraDevice!!.id)
            var jpegSizes: Array<Size>? = null
            if (characteristics != null) {
                jpegSizes = characteristics.get(CameraCharacteristics.SCALER_STREAM_CONFIGURATION_MAP)!!.getOutputSizes(
                    ImageFormat.JPEG
                )
            }
            var width = 0
            var height = 0
            if (jpegSizes != null && jpegSizes.isNotEmpty()) {
                width = jpegSizes[0].width / 2
                height = jpegSizes[0].height / 2
            }

            if (resources.configuration.orientation == Configuration.ORIENTATION_LANDSCAPE) {
                // textureView.surfaceTexture.ra
            } else {
                //    textureView.setAspectRatio(previewSize.height, previewSize.width)
            }
            //   textureView!!.layoutParams = FrameLayout.LayoutParams(width, width)

            val reader = ImageReader.newInstance(width, height, ImageFormat.JPEG, 1)
            val outputSurfaces = ArrayList<Surface>(2)
            outputSurfaces.add(reader.surface)
            outputSurfaces.add(Surface(textureView!!.getSurfaceTexture()))
            val captureBuilder = cameraDevice!!.createCaptureRequest(CameraDevice.TEMPLATE_STILL_CAPTURE)
            captureBuilder.addTarget(reader.surface)
            captureBuilder.set(CaptureRequest.CONTROL_MODE, CameraMetadata.CONTROL_MODE_AUTO)
            // Orientation
            val rotation = windowManager.defaultDisplay.rotation

            val previewSize = jpegSizes!!.last()
// cont.
            val displayRotation = windowManager.defaultDisplay.rotation
            val swappedDimensions = areDimensionsSwapped(displayRotation, characteristics)
// swap width and height if needed
            val rotatedPreviewWidth = if (swappedDimensions) previewSize.height else previewSize.width
            val rotatedPreviewHeight = if (swappedDimensions) previewSize.width else previewSize.height


            //   captureBuilder.set(CaptureRequest.JPEG_ORIENTATION,270)

            captureBuilder.set(
                CaptureRequest.JPEG_ORIENTATION,
                //  getOrientation(rotation,characteristics))
                getJpegOrientation(characteristics, rotation)
            )
            var file = File(Environment.getExternalStorageDirectory().toString() + "/pic.jpg")
            mFile = file
            runOnUiThread {
                val readerListener = object : ImageReader.OnImageAvailableListener {
                    override fun onImageAvailable(reader: ImageReader) {
                        var image: Image? = null
                        try {
                            image = reader.acquireLatestImage()
                            val buffer = image!!.planes[0].buffer
                            val bytes = ByteArray(buffer.capacity())
                            buffer.get(bytes)


                            save(bytes)


                            // runOnUiThread {
                            val bitmapImage = BitmapFactory.decodeByteArray(bytes, 0, bytes.size, null)
                        //    mFileSI = storeImage(bitmapImage)
                            var uri: Uri = Uri.fromFile(mFile)
                            var exif = ExifInterface(uri.getPath())
                            var rotation = exif.getAttributeInt(
                                ExifInterface.TAG_ORIENTATION,
                                ExifInterface.ORIENTATION_NORMAL
                            );
                            var rotationInDegrees = exifToDegrees(rotation);
                            var matrix = Matrix();
                            if (rotation != 0) { matrix.preRotate(rotationInDegrees.toFloat()); }
                            val adjustedBitmap = Bitmap.createBitmap(bitmapImage, 0, 0, bitmapImage.width, bitmapImage.height, matrix, true)

                            if (!bolSI) {
                                bolSI = true

                             mSIBitmap = adjustedBitmap

                            } else {
                                if(mReceiptOneBitmap == null) {
                                    mReceiptOneBitmap = adjustedBitmap
                                }else if(mReceiptTwoBitmap == null){
                                    mReceiptTwoBitmap = adjustedBitmap
                                }else if(mReceiptTreeBitmap == null){
                                    mReceiptTreeBitmap = adjustedBitmap
                                }else if(mReceiptFourBitmap == null){
                                    mReceiptFourBitmap = adjustedBitmap
                                }

                            }

                            //  mFile = storeImage(bitmapImage!!)


                            runOnUiThread {
                                mImageView.visibility = View.VISIBLE
                                mImageView.setImageBitmap(adjustedBitmap)
                            }
                            //    }

                        } catch (e: FileNotFoundException) {
                            e.printStackTrace()
                        } catch (e: IOException) {
                            e.printStackTrace()
                        } finally {
                            image?.close()
                        }
                    }

                    @Throws(IOException::class)
                    private fun save(bytes: ByteArray) {


                        var output: OutputStream? = null
                        try {
                            output = FileOutputStream(file)
                            output!!.write(bytes)
                        } finally {
                            output?.close()
                        }
                    }
                }


                reader.setOnImageAvailableListener(readerListener, mBackgroundHandler)
            }
            val captureListener = object : CameraCaptureSession.CaptureCallback() {
                override fun onCaptureCompleted(
                    session: CameraCaptureSession,
                    request: CaptureRequest,
                    result: TotalCaptureResult
                ) {
                    super.onCaptureCompleted(session, request, result)
                    //   Toast.makeText(this@ScannerActivity, "Saved:$file", Toast.LENGTH_SHORT).show()
                    createCameraPreview()
                }
            }
            cameraDevice!!.createCaptureSession(outputSurfaces, object : CameraCaptureSession.StateCallback() {
                override fun onConfigured(session: CameraCaptureSession) {
                    try {
                        session.capture(captureBuilder.build(), captureListener, mBackgroundHandler)
                    } catch (e: CameraAccessException) {
                        e.printStackTrace()
                    }

                }

                override fun onConfigureFailed(session: CameraCaptureSession) {}
            }, mBackgroundHandler)
        } catch (e: CameraAccessException) {
            e.printStackTrace()
        }

    }

    protected fun createCameraPreview() {
        try {
            val texture = textureView!!.surfaceTexture!!
            texture.setDefaultBufferSize(imageDimension!!.width / 2, imageDimension!!.height / 2)
            val surface = Surface(texture)
            captureRequestBuilder = cameraDevice!!.createCaptureRequest(CameraDevice.TEMPLATE_PREVIEW)
            captureRequestBuilder.addTarget(surface)
            cameraDevice!!.createCaptureSession(listOf(surface), object : CameraCaptureSession.StateCallback() {
                override fun onConfigured(@NonNull cameraCaptureSession: CameraCaptureSession) {
                    //The camera is already closed
                    if (null == cameraDevice) {
                        return
                    }
                    // When the session is ready, we start displaying the preview.
                    cameraCaptureSessions = cameraCaptureSession
                    updatePreview()
                }

                override fun onConfigureFailed(@NonNull cameraCaptureSession: CameraCaptureSession) {
                    //    Toast.makeText(this@ScannerActivity, "Configuration change", Toast.LENGTH_SHORT).show()
                }
            }, null)
        } catch (e: CameraAccessException) {
            e.printStackTrace()
        }

    }

    private fun openCamera() {
        mImageView.visibility = View.GONE
        val manager = getSystemService(Context.CAMERA_SERVICE) as CameraManager
        Log.e(TAG, "is camera open")
        try {
            cameraId = manager.cameraIdList[0]
            val characteristics = manager.getCameraCharacteristics(cameraId)
            val map = characteristics.get(CameraCharacteristics.SCALER_STREAM_CONFIGURATION_MAP)!!
            imageDimension = map.getOutputSizes(SurfaceTexture::class.java)[0]
            // Add permission for camera and let user grant the permission
            if (ActivityCompat.checkSelfPermission(
                    this,
                    Manifest.permission.CAMERA
                ) !== PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                    this,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE
                ) !== PackageManager.PERMISSION_GRANTED
            ) {
                ActivityCompat.requestPermissions(
                    this@ScannerActivity,
                    arrayOf<String>(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE),
                    REQUEST_CAMERA_PERMISSION
                )
                return
            }
            manager.openCamera(cameraId!!, stateCallback, null)
        } catch (e: CameraAccessException) {
            e.printStackTrace()
        }

        Log.e(TAG, "openCamera X")
    }

    protected fun updatePreview() {
        if (null == cameraDevice) {
            Log.e(TAG, "updatePreview error, return")
        }
        captureRequestBuilder.set(CaptureRequest.CONTROL_MODE, CameraMetadata.CONTROL_MODE_AUTO)


        try {
            cameraCaptureSessions.setRepeatingRequest(captureRequestBuilder.build(), null, mBackgroundHandler)
        } catch (e: CameraAccessException) {
            Log.e(TAG, "Failed to start camera preview because it couldn't access camera", e)
        } catch (e: IllegalStateException) {
            Log.e(TAG, "Failed to start camera preview.", e)
        }

    }

    private fun closeCamera() {
        if (null != cameraDevice) {
            cameraDevice!!.close()
            cameraDevice = null
        }
        if (null != imageReader) {
            imageReader!!.close()
            imageReader = null
        }
    }


    override fun onRequestPermissionsResult(requestCode: Int, @NonNull permissions: Array<String>, @NonNull grantResults: IntArray) {
        if (requestCode == REQUEST_CAMERA_PERMISSION) {
            if (grantResults[0] == PackageManager.PERMISSION_DENIED) {
                // close the app
                Toast.makeText(
                    this@ScannerActivity,
                    "Sorry!!!, you can't use this app without granting permission",
                    Toast.LENGTH_LONG
                ).show()
                finish()
            }
        }
    }


    fun getJpegOrientation(c: CameraCharacteristics, deviceOrientation: Int): Int {
        if (deviceOrientation == android.view.OrientationEventListener.ORIENTATION_UNKNOWN) return 0;
        var sensorOrientation = c.get(CameraCharacteristics.SENSOR_ORIENTATION);

        // Round device orientation to a multiple of 90
        var deviceOrientationQ = (deviceOrientation + 45) / 90 * 90;

        // Reverse device orientation for front-facing cameras
        val facingFront = c.get(CameraCharacteristics.LENS_FACING) == CameraCharacteristics.LENS_FACING_FRONT;
        if (facingFront) deviceOrientationQ = -deviceOrientationQ;

        // Calculate desired JPEG orientation relative to camera orientation to make
        // the image upright relative to the device orientation
        var jpegOrientation = (sensorOrientation + deviceOrientation + 360) % 360

        return jpegOrientation;
    }

    protected override fun onResume() {
        super.onResume()
        Log.e(TAG, "onResume")
        startBackgroundThread()
        if (textureView!!.isAvailable) {
            openCamera()
        } else {
            textureView!!.surfaceTextureListener = textureListener
        }
    }

    protected override fun onPause() {
        Log.e(TAG, "onPause")
        //closeCamera();
        stopBackgroundThread()
        super.onPause()
    }


    fun callCheckout(game_code: String, branch_id: String) {

        var finalBitmap = finalcombineImage(mSIBitmap!!, mReceiptOneBitmap!!)

        if(mReceiptTwoBitmap != null){

            finalBitmap = treecombineImage(finalBitmap,mReceiptTwoBitmap!!)

            if(mReceiptTreeBitmap != null){
                finalBitmap = treecombineImage(finalBitmap,mReceiptTreeBitmap!!)

                if(mReceiptFourBitmap != null){
                    finalBitmap = treecombineImage(finalBitmap,mReceiptFourBitmap!!)
                }
            }
        }


        mFile = storeImage(finalBitmap!!)
        // percentage_text.text = "20%"

        val token = Prefs.getString(keys.USER_TOKEN, "")


        val fbody = RequestBody.create(MediaType.parse("image/*"), mFile)
        val fileBody = MultipartBody.Part.createFormData("file", mFile!!.name, fbody)


        val apiService = UserService.create(this.getString(R.string.base_url))
        val mGson = GsonBuilder()
            .setLenient()
            .create()

        image_verified_text.text = "Verifying images..."
        val callService = apiService.gameCheckout(
            fileBody, game_code, "",
            "application/json", "Bearer $token"
        )
        callService.enqueue(object : retrofit2.Callback<Object> {
            override fun onResponse(
                call: Call<Object>,
                response: retrofit2.Response<Object>?
            ) {
                if (response != null) {
                    val a_json_string = mGson.toJson(response!!.body())
                    if (response.code() == 201) {
                        val won: GameCheckoutResponse =
                            Gson().fromJson(a_json_string, GameCheckoutResponse::class.java)

                        val creditsGame = won.total_credits
                        val credsDouble = creditsGame!!.toDouble()

                        if (credsDouble.toInt() > 0) {
                            val mCountDownTimer: CountDownTimer
                            var i = 40
                            var per = 0
                            mCountDownTimer = object : CountDownTimer(4000, 100) {

                                override fun onTick(millisUntilFinished: Long) {
                                    Log.v("Log_tag", "Tick of Progress$i$millisUntilFinished")
                                    i++

                                    per = i * 100 / (4000 / 40)

                                    per + 40

                                    percentage_text.text = per.toString() + "%"

                                    //   mProgressBar.progress = i * 100 / (5000 / 10)

                                }

                                override fun onFinish() {
                                    i++
                                    percentage_text.text = "80%"


                                    if(connected) {
                                        callSelectWinners()
                                    }else{
                                        displayDialog("No internet connection!")
                                    }
                                    //    callCheckout("","")
                                    //finish()
                                }
                            }
                            mCountDownTimer.start()
                        } else {

                            Toast.makeText(applicationContext, "Zero Credits", Toast.LENGTH_SHORT).show()

                        }

                        // Prefs.putString(keys.WINNER_CODE,won.responseData!!.total_credit.toString())
                        // Toast.makeText(applicationContext, won.responseData!!.total_credit.toString(), Toast.LENGTH_SHORT).show()


                        // Toast.makeText(applicationContext,response.body()!!.responseData!!.total_credit.toString(),Toast.LENGTH_SHORT).show()

                    } else if (response.code() == 400) {
                        displayDialog("Something went wrong!")
                    } else if (response.code() == 500) {
//                        val a_json_string = mGson.toJson(response!!.errorBody())
                        image_verified_text.text = "Failed"
                        percentage_text.text = "100%"
                        try {
                            var jObjError = JSONObject(response.errorBody()!!.string());
                            val failed: CheckoutFailedMessage = Gson().fromJson<CheckoutFailedMessage>(
                                jObjError.toString(),
                                CheckoutFailedMessage::class.java
                            )

                            // Toast.makeText(applicationContext,failed.message,Toast.LENGTH_SHORT).show()

                            val ft = supportFragmentManager.beginTransaction()
                            val dialogFragment = FailedCheckOutDialogFragment(failed.message, this@ScannerActivity)
                            dialogFragment.isCancelable = false
                            dialogFragment.show(ft, "redeem")

                        } catch (e: Exception) {
                            displayDialog("Something went wrong!")
                            Toast.makeText(applicationContext, e.message, Toast.LENGTH_LONG).show()
                        }

                        // displayDialog("Something went wrong!" + response.errorBody().toString())

                    } else {
                        displayDialog("Something went wrong!")
                    }
                }
                //    progress_layout_login.visibility = View.GONE
            }

            override fun onFailure(call: Call<Object>, t: Throwable) {
                Log.e("aa", t.toString());
                //    progress_layout_login.visibility = View.GONE
                displayDialog("Something went wrong!!")
            }
        })

    }


    fun callSelectWinners() {
        val mGson = GsonBuilder()
            .setLenient()
            .create()

        val seleceted = Prefs.getString(keys.SELECTED_GAME, "")
        var gameCode = getString(com.fourello.expee.R.string.game_main_url)

        //http://files.taktylstudios.com/projects/fourello/expee-wheel/ariel/0.1.0/?result=1
        if (seleceted == "ariel") {
            gameCode = "ariel_spin_the_wheel"
        } else if (seleceted == "pantene") {
            gameCode = "pantene_spin_the_wheel"

        } else if (seleceted == "lucky") {
            gameCode = "lucky_me_spin_the_wheel"
        }

        val token = Prefs.getString(keys.USER_TOKEN, "")
        val apiService = UserService.create(this.getString(R.string.base_url))
        val callService = apiService.selectWinner(
            gameCode,
             "PUT", "application/json", "Bearer $token"
        )
        callService.enqueue(object : retrofit2.Callback<Object> {
            override fun onResponse(
                call: Call<Object>,
                response: retrofit2.Response<Object>?
            ) {
                val a_json_string = mGson.toJson(response!!.body())
                if (response != null) {

                    if (response.code() == 200) {

                        try {
                            val failed: SelectWinnerFailed =
                                Gson().fromJson<SelectWinnerFailed>(a_json_string, SelectWinnerFailed::class.java)
                            Prefs.putString(keys.WINNER_CODE, failed.responseData.toString())
                            //       Toast.makeText(applicationContext,failed.responseData.toString(),Toast.LENGTH_SHORT).show()
                        } catch (e: Exception) {
                            val won: SelectWinnerWon =
                                Gson().fromJson<SelectWinnerWon>(a_json_string, SelectWinnerWon::class.java)
                            Prefs.putString(keys.WINNER_CODE, won.responseData!!.result.toString())
                            //      Toast.makeText(applicationContext,won.responseData!!.game_id.toString(),Toast.LENGTH_SHORT).show()
                        }


//                      Prefs.getString(keys.WINNER_CODE,response.body()!!.responseData!!.toString())
//                        Toast.makeText(applicationContext,response.body()!!.responseData!!.toString(),Toast.LENGTH_SHORT).show()

                        // if(response.body() instanceof )


                        val mCountDownTimer: CountDownTimer

                        var i = 80
                        var per = 0
                        mCountDownTimer = object : CountDownTimer(2000, 100) {

                            override fun onTick(millisUntilFinished: Long) {
                                Log.v("Log_tag", "Tick of Progress$i$millisUntilFinished")
                                i++

                                per = i * 100 / (2000 / 20)

                                per + 80

                                percentage_text.text = per.toString() + "%"

                                //   mProgressBar.progress = i * 100 / (5000 / 10)

                            }

                            override fun onFinish() {
                                i++
                                percentage_text.text = "100%"
                                image_verified_text.text = "Successful!"
                                btn_play_game.visibility = View.VISIBLE
                                //    callCheckout("","")
                                //finish()
                            }
                        }
                        mCountDownTimer.start()


                    } else if (response.code() == 400) {
                        //        displayDialog("Something went wrong!")
                    } else {
                                displayDialog("Something went wrong!")

                    }
                }
                //    progress_layout_login.visibility = View.GONE
            }

            override fun onFailure(call: Call<Object>, t: Throwable) {
                Log.e("aa", t.toString());
                displayDialog("Something went wrong!")
                //    progress_layout_login.visibility = View.GONE
                //    displayDialog("Something went wrong!! " +t.message)
            }
        })

    }


    fun displayDialog(message: String) {
        val builder = AlertDialog.Builder(this)
        builder.setMessage(message)
        builder.setPositiveButton("Ok") { dialog, _ ->
            finish()
            dialog.dismiss()
        }
        val dialog: AlertDialog = builder.create()
        dialog.show()
    }

    private fun storeImage(image: Bitmap?): File? {
        val pictureFile = getOutputMediaFile()
        if (pictureFile == null) {
            Log.d(
                "tag",
                "Error creating media file, check storage permissions: "
            )// e.getMessage());
            return null
        }
        try {
            val fos = FileOutputStream(pictureFile)
            image!!.compress(Bitmap.CompressFormat.PNG, 100, fos)
            fos.close()
        } catch (e: FileNotFoundException) {
            //  errorDialogInvalidReciept();

            Log.d("tag", "File not found: " + e.message)
        } catch (e: IOException) {
            //  errorDialogInvalidReciept();
            Log.d("tag", "Error accessing file: " + e.message)
        }

        return pictureFile
    }

    private fun getOutputMediaFile(): File? {

        val mediaStorageDir = File(
            (Environment.getExternalStorageDirectory()).toString()
                    + "/Android/data/"
                    + applicationContext.packageName
                    + "/Files"
        )

        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                return null
            }
        }

        val timeStamp = SimpleDateFormat("ddMMyyyy_HHmm").format(Date())
        val mediaFile: File
        val mImageName = "MI_$timeStamp.png"
        mediaFile = File(mediaStorageDir.getPath() + File.separator + mImageName)
        return mediaFile
    }


    fun finalcombineImage(firstImage: Bitmap, secondImage: Bitmap): Bitmap {
        var cs: Bitmap? = null
        val metrics = baseContext.resources.displayMetrics
        // int width = metrics.widthPixels;
        var width = firstImage.width

        if (firstImage.width > secondImage.width) {
            width = firstImage.width

        } else {
            width = secondImage.width
        }

        val height = firstImage.height + secondImage.height
        cs = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888)
        val comboImage = Canvas(cs!!)
        val dest1 = Rect(0, 0, width, firstImage.height) // left,top,right,bottom
        comboImage.drawBitmap(firstImage, null, dest1, null)
        var dest2 = Rect()
        // if (odd == "odd") {
        dest2 = Rect(0, firstImage.height, width, height)
        // } else {
        //    dest2 = Rect(0, height / 2, width, height)
        // }
        comboImage.drawBitmap(secondImage, null, dest2, null)
        return cs
    }

    fun treecombineImage(firstImage: Bitmap, secondImage: Bitmap): Bitmap {
        var cs: Bitmap? = null
        val metrics = baseContext.resources.displayMetrics
        // int width = metrics.widthPixels;
        var width = firstImage.width

        if (firstImage.width > secondImage.width) {
            width = firstImage.width

        } else {
            width = secondImage.width
        }

        val height = firstImage.height + (secondImage.height)
        cs = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888)
        val comboImage = Canvas(cs!!)
        val dest1 = Rect(0, 0, width, firstImage.height) // left,top,right,bottom
        comboImage.drawBitmap(firstImage, null, dest1, null)
        var dest2 = Rect()
        // if (odd == "odd") {
        dest2 = Rect(0, firstImage.height, width, height)
        // } else {
        //    dest2 = Rect(0, height / 2, width, height)
        // }
        comboImage.drawBitmap(secondImage, null, dest2, null)
        return cs
    }


    private fun areDimensionsSwapped(displayRotation: Int, cameraCharacteristics: CameraCharacteristics): Boolean {
        var swappedDimensions = false
        when (displayRotation) {
            Surface.ROTATION_0, Surface.ROTATION_180 -> {
                if (cameraCharacteristics.get(CameraCharacteristics.SENSOR_ORIENTATION) == 90 || cameraCharacteristics.get(
                        CameraCharacteristics.SENSOR_ORIENTATION
                    ) == 270
                ) {
                    swappedDimensions = true
                }
            }
            Surface.ROTATION_90, Surface.ROTATION_270 -> {
                if (cameraCharacteristics.get(CameraCharacteristics.SENSOR_ORIENTATION) == 0 || cameraCharacteristics.get(
                        CameraCharacteristics.SENSOR_ORIENTATION
                    ) == 180
                ) {
                    swappedDimensions = true
                }
            }
            else -> {
                // invalid display rotation
            }
        }
        return swappedDimensions
    }

    private fun getOrientation(rotation: Int, cameraCharacteristics: CameraCharacteristics): Int {
        //  return (ORIENTATIONS.get(rotation) + mSensorOrientation + 270) % 360;
        return (rotation + cameraCharacteristics.get(CameraCharacteristics.SENSOR_ORIENTATION) + 270) % 360;
    }

    fun exifToDegrees(exifOrientation: Int): Int {
        if (exifOrientation == ExifInterface.ORIENTATION_ROTATE_90) {
            return 90; } else if (exifOrientation == ExifInterface.ORIENTATION_ROTATE_180) {
            return 180; } else if (exifOrientation == ExifInterface.ORIENTATION_ROTATE_270) {
            return 270; }
        return 0;
    }



    inner class MyReceiver : BroadcastReceiver() {

        override fun onReceive(context: Context, intent: Intent) {
            //    do something
            val isConnected = intent.getBooleanExtra(ConnectivityManager.EXTRA_NO_CONNECTIVITY, false)
            if (isConnected) {
                // player.playWhenReady = false
                displayDialog("No internet connection.")
                connected = false
//                internet_connected.visibility = View.GONE
            } else {

                InternetCheck(object : InternetCheck.Consumer {
                    override fun accept(internet: Boolean?) {

                        if (internet!!) {
                            connected = true
                            runOnUiThread(java.lang.Runnable {
                                //  player.playWhenReady = true
                                //  player.playbackState
                                //  internet_disconnect.visibility = View.GONE
                                //  internet_connected.visibility = View.VISIBLE

                                val timer = Timer()
                                timer.schedule(timerTask {
                                    runOnUiThread(Runnable {
                                        //    internet_connected.visibility = View.GONE

                                    })
                                }, 2500)

                            })

                        } else {
                            //  player.playWhenReady = false
                            //  internet_disconnect.visibility = View.VISIBLE

                            Toast.makeText(applicationContext,"Slow internet connection", Toast.LENGTH_SHORT).show()

                            //internet_connected.visibility = View.GONE
                        }

                    }
                })


            }
        }

    }

    internal class InternetCheck(private val mConsumer: Consumer) :
        AsyncTask<Void, Void, Boolean>() {
        interface Consumer {
            fun accept(internet: Boolean?)
        }

        init {
            execute()
        }

        override fun doInBackground(vararg voids: Void): Boolean? {
            try {
                val sock = Socket()
                sock.connect(InetSocketAddress("8.8.8.8", 53) as SocketAddress?, 1500)
                sock.close()
                return true
            } catch (e: IOException) {
                return false
            }
        }

        override fun onPostExecute(internet: Boolean?) {
            mConsumer.accept(internet)
        }
    }
}
