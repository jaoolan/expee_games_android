package com.fourello.expee.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.CountDownTimer
import android.util.Log
import android.view.WindowManager
import android.widget.ProgressBar
import com.fourello.expee.R
import kotlinx.android.synthetic.main.activity_story_two.*

class StoryOneActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_story_one)
        window.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN)


        val mProgressBar: ProgressBar
        val mCountDownTimer: CountDownTimer
        var i = 0

        mProgressBar = findViewById<ProgressBar>(com.fourello.expee.R.id.progress_two)
        mProgressBar.progress = i
        mCountDownTimer = object : CountDownTimer(5000, 10) {

            override fun onTick(millisUntilFinished: Long) {
                Log.v("Log_tag", "Tick of Progress$i$millisUntilFinished")
                i++
                mProgressBar.progress = i * 100 / (5000 / 10)

            }

            override fun onFinish() {
                //Do what you want

                i++
                mProgressBar.progress = 100

                finish()
                overridePendingTransition(R.anim.enterfrom,R.anim.enterto)
            }
        }
        mCountDownTimer.start()


        close_story_two.setOnClickListener {

            finish()
            overridePendingTransition(
                com.fourello.expee.R.anim.exitfrom,
                com.fourello.expee.R.anim.exitto
            )

        }

    }
}
