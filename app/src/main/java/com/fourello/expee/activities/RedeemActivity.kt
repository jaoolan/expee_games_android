package com.fourello.expee.activities

import android.Manifest
import android.content.Context
import android.content.pm.PackageManager
import android.graphics.BitmapFactory
import android.graphics.ImageFormat
import android.graphics.SurfaceTexture
import android.hardware.camera2.*
import android.media.Image
import android.media.ImageReader
import android.os.*
import androidx.appcompat.app.AppCompatActivity
import android.util.Log
import android.util.Size
import android.view.Surface
import android.view.TextureView
import android.view.View
import android.view.WindowManager
import android.widget.Button
import android.widget.Toast
import androidx.annotation.NonNull
import androidx.core.app.ActivityCompat
import com.fourello.expee.R
import kotlinx.android.synthetic.main.activity_redeem.*

import java.io.*
import java.util.*
import kotlin.collections.ArrayList


// ignore this, this is from last version
class RedeemActivity : AppCompatActivity() {

    private val TAG = "AndroidCameraApi"
    private var takePictureButton: Button? = null
    private var textureView: TextureView? = null
//    private val ORIENTATIONS = SparseIntArray() = ArrayList<SparseIntArray>(
//        ORIENTATIONS.append(Surface.ROTATION_0, 90))
//
//
////    static
////    {
////        ORIENTATIONS.append(Surface.ROTATION_0, 90);
////        ORIENTATIONS.append(Surface.ROTATION_90, 0);
////        ORIENTATIONS.append(Surface.ROTATION_180, 270);
////        ORIENTATIONS.append(Surface.ROTATION_270, 180);
////    }




    private var cameraId: String? = null
    protected var cameraDevice: CameraDevice? = null
    protected lateinit var cameraCaptureSessions: CameraCaptureSession
    protected var captureRequest: CaptureRequest? = null
    protected lateinit var captureRequestBuilder: CaptureRequest.Builder
    private var imageDimension: Size? = null
    private var imageReader: ImageReader? = null
    private val file: File? = null
    private val REQUEST_CAMERA_PERMISSION = 200
    private val mFlashSupported: Boolean = false
    private var mBackgroundHandler: Handler? = null
    private var mBackgroundThread: HandlerThread? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_redeem)

        window.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN
        )
        textureView = findViewById(R.id.texture)

        textureView!!.surfaceTextureListener = textureListener

        btn_capture_camera_red.setOnClickListener {

            after_capture_lay_red.visibility = View.VISIBLE
            btn_capture_camera_red.visibility = View.GONE

            takePicture()


        }


        btn_play_game_red.setOnClickListener {

            finish()
            overridePendingTransition(R.anim.exitfrom,R.anim.exitto)

        }

        btn_retry_image_red.setOnClickListener {

            mImageView_red.setImageBitmap(null)
            after_capture_lay_red.visibility = View.GONE
            btn_capture_camera_red.visibility = View.VISIBLE

        }


        btn_next_red.setOnClickListener {

           // if(!ifStepTwo) {
           //     ifStepTwo = true
                after_capture_lay_red.visibility = View.GONE
                btn_capture_camera_red.visibility = View.VISIBLE
                layout_step_two_red.foreground = null
                step_two_image_check_red.visibility = View.VISIBLE
                txt_step_two_red.visibility = View.VISIBLE
                step_two_image_check_red.setImageDrawable(getDrawable(R.drawable.scan_step_check))
                mImageView_red.setImageBitmap(null)


        //    }else {

                after_capture_lay_red.visibility = View.GONE
                step_tree_percentage_lay_red.visibility = View.VISIBLE
                btn_capture_camera_red.visibility = View.GONE
              //  layout_step_tree.foreground = null
              //  step_tree_image_check.setImageDrawable(getDrawable(R.drawable.scan_step_check))
               // step_tree_image_check.visibility = View.VISIBLE
              //  txt_step_tree.visibility = View.VISIBLE
                step2_overlay_red.visibility = View.GONE
                step_tree_lay_red.visibility = View.VISIBLE
                photo_instruc_red.visibility = View.GONE
                mImageView_red.setImageBitmap(null)
                val mCountDownTimer: CountDownTimer
                var i = 0
                var per = 0
                mCountDownTimer = object : CountDownTimer(3000, 100) {

                    override fun onTick(millisUntilFinished: Long) {
                        Log.v("Log_tag", "Tick of Progress$i$millisUntilFinished")
                        i++

                        per = i * 100 / (3000 / 100)
                        percentage_text_red.text = per.toString() + "%"

                        //   mProgressBar.progress = i * 100 / (5000 / 10)

                    }

                    override fun onFinish() {
                        i++
                        percentage_text_red.text =  "100%"
                        image_verified_text_red.text = "Claim successful!"
                       btn_play_game_red.visibility = View.VISIBLE
                        //finish()
                    }
                }
                mCountDownTimer.start()
          //  }
        }


    }

    internal var textureListener: TextureView.SurfaceTextureListener = object : TextureView.SurfaceTextureListener {
        override fun onSurfaceTextureAvailable(surface: SurfaceTexture, width: Int, height: Int) {
            //open your camera here
            openCamera()
        }

        override fun onSurfaceTextureSizeChanged(surface: SurfaceTexture, width: Int, height: Int) {
            // Transform you image captured size according to the surface width and height
        }

        override fun onSurfaceTextureDestroyed(surface: SurfaceTexture): Boolean {
            return false
        }

        override fun onSurfaceTextureUpdated(surface: SurfaceTexture) {}
    }
    private val stateCallback = object : CameraDevice.StateCallback() {
        override fun onOpened(camera: CameraDevice) {
            //This is called when the camera is open
            Log.e(TAG, "onOpened")
            cameraDevice = camera
            createCameraPreview()
        }

        override fun onDisconnected(camera: CameraDevice) {
            cameraDevice!!.close()
        }

        override fun onError(camera: CameraDevice, error: Int) {
            cameraDevice!!.close()
            cameraDevice = null
        }
    }
    internal val captureCallbackListener: CameraCaptureSession.CaptureCallback =
        object : CameraCaptureSession.CaptureCallback() {
            override fun onCaptureCompleted(
                session: CameraCaptureSession,
                request: CaptureRequest,
                result: TotalCaptureResult
            ) {
                super.onCaptureCompleted(session, request, result)
               // Toast.makeText(this@ScannerActivity, "Saved:$file", Toast.LENGTH_SHORT).show()
                createCameraPreview()
            }
        }


    protected fun startBackgroundThread() {
        mBackgroundThread = HandlerThread("Camera Background")
        mBackgroundThread!!.start()
        mBackgroundHandler = Handler(mBackgroundThread!!.getLooper())
    }

    protected fun stopBackgroundThread() {
        mBackgroundThread!!.quitSafely()
        try {
            mBackgroundThread!!.join()
            mBackgroundThread = null
            mBackgroundHandler = null
        } catch (e: InterruptedException) {
            e.printStackTrace()
        }

    }

    protected fun takePicture() {
        if (null == cameraDevice) {
            Log.e(TAG, "cameraDevice is null")
            return
        }
        val manager = getSystemService(Context.CAMERA_SERVICE) as CameraManager
        try {
            val characteristics = manager.getCameraCharacteristics(cameraDevice!!.getId())
            var jpegSizes: Array<Size>? = null
            if (characteristics != null) {
                jpegSizes = characteristics.get(CameraCharacteristics.SCALER_STREAM_CONFIGURATION_MAP)!!.getOutputSizes(
                    ImageFormat.JPEG
                )
            }
            var width = 640
            var height = 480
            if (jpegSizes != null && 0 < jpegSizes.size) {
                width = jpegSizes[0].width
                height = jpegSizes[0].height
            }
            val reader = ImageReader.newInstance(width, height, ImageFormat.JPEG, 1)
            val outputSurfaces = ArrayList<Surface>(2)
            outputSurfaces.add(reader.surface)
            outputSurfaces.add(Surface(textureView!!.getSurfaceTexture()))
            val captureBuilder = cameraDevice!!.createCaptureRequest(CameraDevice.TEMPLATE_STILL_CAPTURE)
            captureBuilder.addTarget(reader.surface)
            captureBuilder.set(CaptureRequest.CONTROL_MODE, CameraMetadata.CONTROL_MODE_AUTO)
            // Orientation
            val rotation = windowManager.defaultDisplay.rotation
            captureBuilder.set(CaptureRequest.JPEG_ORIENTATION, getJpegOrientation( characteristics, rotation))
            val file = File(Environment.getExternalStorageDirectory().toString() + "/pic.jpg")
            val readerListener = object : ImageReader.OnImageAvailableListener {
                override fun onImageAvailable(reader: ImageReader) {
                    var image: Image? = null
                    try {
                        image = reader.acquireLatestImage()
                        val buffer = image!!.planes[0].buffer
                        val bytes = ByteArray(buffer.capacity())
                        buffer.get(bytes)


                        save(bytes)
                        runOnUiThread {
                            val bitmapImage = BitmapFactory.decodeByteArray(bytes, 0, bytes.size, null)

                            mImageView_red.setImageBitmap(bitmapImage)
                        }

                    } catch (e: FileNotFoundException) {
                        e.printStackTrace()
                    } catch (e: IOException) {
                        e.printStackTrace()
                    } finally {
                        image?.close()
                    }
                }

                @Throws(IOException::class)
                private fun save(bytes: ByteArray) {


                    var output: OutputStream? = null
                    try {
                        output = FileOutputStream(file)
                        output!!.write(bytes)
                    } finally {
                        output?.close()
                    }
                }
            }
            reader.setOnImageAvailableListener(readerListener, mBackgroundHandler)
            val captureListener = object : CameraCaptureSession.CaptureCallback() {
                override fun onCaptureCompleted(
                    session: CameraCaptureSession,
                    request: CaptureRequest,
                    result: TotalCaptureResult
                ) {
                    super.onCaptureCompleted(session, request, result)
                   // Toast.makeText(this@ScannerActivity, "Saved:$file", Toast.LENGTH_SHORT).show()
                    createCameraPreview()
                }
            }
            cameraDevice!!.createCaptureSession(outputSurfaces, object : CameraCaptureSession.StateCallback() {
                override fun onConfigured(session: CameraCaptureSession) {
                    try {
                        session.capture(captureBuilder.build(), captureListener, mBackgroundHandler)
                    } catch (e: CameraAccessException) {
                        e.printStackTrace()
                    }

                }

                override fun onConfigureFailed(session: CameraCaptureSession) {}
            }, mBackgroundHandler)
        } catch (e: CameraAccessException) {
            e.printStackTrace()
        }

    }

    protected fun createCameraPreview() {
        try {
            val texture = textureView!!.getSurfaceTexture()!!
            texture.setDefaultBufferSize(imageDimension!!.getWidth(), imageDimension!!.getHeight())
            val surface = Surface(texture)
            captureRequestBuilder = cameraDevice!!.createCaptureRequest(CameraDevice.TEMPLATE_PREVIEW)
            captureRequestBuilder.addTarget(surface)
            cameraDevice!!.createCaptureSession(Arrays.asList(surface), object : CameraCaptureSession.StateCallback() {
                override fun onConfigured(@NonNull cameraCaptureSession: CameraCaptureSession) {
                    //The camera is already closed
                    if (null == cameraDevice) {
                        return
                    }
                    // When the session is ready, we start displaying the preview.
                    cameraCaptureSessions = cameraCaptureSession
                    updatePreview()
                }

                override fun onConfigureFailed(@NonNull cameraCaptureSession: CameraCaptureSession) {
                   // Toast.makeText(this@RedeemActivity, "Configuration change", Toast.LENGTH_SHORT).show()
                }
            }, null)
        } catch (e: CameraAccessException) {
            e.printStackTrace()
        }

    }

    private fun openCamera() {
        val manager = getSystemService(Context.CAMERA_SERVICE) as CameraManager
        Log.e(TAG, "is camera open")
        try {
            cameraId = manager.cameraIdList[0]
            val characteristics = manager.getCameraCharacteristics(cameraId)
            val map = characteristics.get(CameraCharacteristics.SCALER_STREAM_CONFIGURATION_MAP)!!
            imageDimension = map.getOutputSizes(SurfaceTexture::class.java)[0]
            // Add permission for camera and let user grant the permission
            if (ActivityCompat.checkSelfPermission(
                    this,
                    Manifest.permission.CAMERA
                ) !== PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                    this,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE
                ) !== PackageManager.PERMISSION_GRANTED
            ) {
                ActivityCompat.requestPermissions(
                    this@RedeemActivity,
                    arrayOf<String>(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE),
                    REQUEST_CAMERA_PERMISSION
                )
                return
            }
            manager.openCamera(cameraId, stateCallback, null)
        } catch (e: CameraAccessException) {
            e.printStackTrace()
        }

        Log.e(TAG, "openCamera X")
    }

    protected fun updatePreview() {
        if (null == cameraDevice) {
            Log.e(TAG, "updatePreview error, return")
        }
        captureRequestBuilder.set(CaptureRequest.CONTROL_MODE, CameraMetadata.CONTROL_MODE_AUTO)
        try {
            cameraCaptureSessions.setRepeatingRequest(captureRequestBuilder.build(), null, mBackgroundHandler)
        } catch (e: CameraAccessException) {
            e.printStackTrace()
        }

    }

    private fun closeCamera() {
        if (null != cameraDevice) {
            cameraDevice!!.close()
            cameraDevice = null
        }
        if (null != imageReader) {
            imageReader!!.close()
            imageReader = null
        }
    }






    override fun onRequestPermissionsResult(requestCode: Int, @NonNull permissions: Array<String>, @NonNull grantResults: IntArray) {
        if (requestCode == REQUEST_CAMERA_PERMISSION) {
            if (grantResults[0] == PackageManager.PERMISSION_DENIED) {
                // close the app
                Toast.makeText(
                    this@RedeemActivity,
                    "Sorry!!!, you can't use this app without granting permission",
                    Toast.LENGTH_LONG
                ).show()
                finish()
            }
        }
    }


    fun getJpegOrientation(c : CameraCharacteristics, deviceOrientation : Int) : Int {
        if (deviceOrientation == android.view.OrientationEventListener.ORIENTATION_UNKNOWN) return 0;
        var sensorOrientation = c.get(CameraCharacteristics.SENSOR_ORIENTATION);

        // Round device orientation to a multiple of 90
        var  deviceOrientationQ = (deviceOrientation + 45) / 90 * 90;

        // Reverse device orientation for front-facing cameras
        val facingFront = c.get(CameraCharacteristics.LENS_FACING) == CameraCharacteristics.LENS_FACING_FRONT;
        if (facingFront) deviceOrientationQ = -deviceOrientationQ;

        // Calculate desired JPEG orientation relative to camera orientation to make
        // the image upright relative to the device orientation
        var jpegOrientation = (sensorOrientation + deviceOrientation + 360) % 360

        return jpegOrientation;
    }

    protected override fun onResume() {
        super.onResume()
        Log.e(TAG, "onResume")
        startBackgroundThread()
        if (textureView!!.isAvailable()) {
            openCamera()
        } else {
            textureView!!.setSurfaceTextureListener(textureListener)
        }
    }

    protected override fun onPause() {
        Log.e(TAG, "onPause")
        //closeCamera();
        stopBackgroundThread()
        super.onPause()
    }


}
