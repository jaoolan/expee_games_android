package com.fourello.expee.activities

import android.content.*
import android.net.ConnectivityManager
import android.os.AsyncTask
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.method.PasswordTransformationMethod
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.core.content.ContextCompat
import com.facebook.*
import com.facebook.login.LoginBehavior
import com.facebook.login.LoginManager
import com.facebook.login.LoginResult
import com.fourello.expee.R
import com.fourello.expee.constant.keys
import com.fourello.expee.fragmentdialog.ForgotPassDialogFragment
import com.fourello.expee.model.GameCredits
import com.fourello.expee.model.UserProfileData
import com.fourello.expee.model.UserResponse
import com.fourello.expee.webservice.UserService
import com.google.gson.GsonBuilder
import com.pixplicity.easyprefs.library.Prefs
import kotlinx.android.synthetic.main.activity_login.*
import retrofit2.Call
import java.io.IOException
import java.net.InetSocketAddress
import java.net.Socket
import java.net.SocketAddress
import java.util.*
import kotlin.concurrent.timerTask

class LoginActivity : AppCompatActivity() {
    private var callbackManager: CallbackManager? = null
    internal lateinit var mReceiver: BroadcastReceiver
    private val RC_SIGN_IN = 9001
    var isRemember = false



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        val filter = IntentFilter()
        filter.addAction(ConnectivityManager.CONNECTIVITY_ACTION)
        mReceiver = MyReceiver()
        registerReceiver(mReceiver, filter)
        val sharedPref: SharedPreferences = getSharedPreferences(keys.REMEMBER_ME, Context.MODE_PRIVATE)

        go_to_register.setOnClickListener {

            val intent = Intent(applicationContext, RegisterActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
            applicationContext.startActivity(intent)
            overridePendingTransition(
                R.anim.enterfrom,
                R.anim.enterto
            )


        }

       // sharedPref.getString(keys.REMEMBER_ME, "")
        edt_log_email.setText( sharedPref.getString(keys.REMEMBER_EMail, ""))
        edt_log_password.setText( sharedPref.getString(keys.REMEMBER_Password, ""))
        if(edt_log_email.text.toString().length > 0){
            isRemember = true
            img_remember.setImageDrawable(
                ContextCompat.getDrawable(
                    applicationContext,
                    R.drawable.ic_check_circle_black_24dp
                )
            )
        }else{


        }


        edt_log_password.transformationMethod = PasswordTransformationMethod()
        FacebookSdk.sdkInitialize(this.applicationContext)

        callbackManager = CallbackManager.Factory.create()
        LoginManager.getInstance().loginBehavior = LoginBehavior.WEB_ONLY
        LoginManager.getInstance().registerCallback(callbackManager, object : FacebookCallback<LoginResult> {
            override fun onSuccess(loginResult: LoginResult) {
                Log.d("FBLOGIN", loginResult.accessToken.token.toString())
                Log.d("FBLOGIN", loginResult.recentlyDeniedPermissions.toString())
                Log.d("FBLOGIN", loginResult.recentlyGrantedPermissions.toString())
                val idid = loginResult.accessToken.userId

                val request = GraphRequest.newMeRequest(loginResult.accessToken) { `object`,
                                                                                   response ->
                    try {
                        //here is the data that you want
                        Log.d("FBLOGIN_JSON_RES", `object`.toString())

                        if (`object`.has("id")) {
//                            Will get info here
//                            `object`
                        } else {
                            Log.e("FBLOGIN_FAILED", `object`.toString())
                        }

                    } catch (e: Exception) {
                        e.printStackTrace()
//                        dismissDialogLogin()
                    }
                }

                val parameters = Bundle()
                parameters.putString("fields", "name,email,id,picture.type(large)")
                request.parameters = parameters
                request.executeAsync()

                callWebServiceFacebook(idid, loginResult.accessToken.token.toString())

            }

            override fun onCancel() {
                Log.e("FBLOGIN_FAILED", "Cancel")
            }

            override fun onError(error: FacebookException) {
                // Log.e("FBLOGIN_FAILED", "ERROR", error)
                Log.e("FBLOGIN_FAILED", "Cancel")
            }
        })


        btn_continue_login.setOnClickListener {

            val emailString = edt_log_email.text.toString()
            val passString = edt_log_password.text.toString()

            if (emailString.isNotEmpty()) {
                if (passString.isNotEmpty()) {
                    if (isEmailValid(emailString.trim())) {


                        callWebService()

                        if (isRemember) {
                            val editor = sharedPref.edit()
                            editor.putString(keys.REMEMBER_EMail, edt_log_email.text.toString())
                            editor.putString(keys.REMEMBER_Password, edt_log_password.text.toString())
                            editor.apply()
                        } else {
                            val editor = sharedPref.edit()
                            editor.putString(keys.REMEMBER_EMail, "")
                            editor.putString(keys.REMEMBER_Password,"")
                            editor.apply()
                        }


                    } else {

                        displayDialog("Email is invalid.")
                    }


                } else {

                    displayDialog("Please enter your password.")
                }

            } else {

                displayDialog("Please enter your email address.")

            }


        }

        btn_facebook.setOnClickListener {

            LoginManager.getInstance().logInWithReadPermissions(this, listOf("public_profile"))

        }

        btn_forgot.setOnClickListener {

            val ft = supportFragmentManager.beginTransaction()
            val dialogFragment = ForgotPassDialogFragment()
            dialogFragment.isCancelable = false
            dialogFragment.show(ft, "redeem")

        }

        img_remember.setOnClickListener {
            if (isRemember) {
                isRemember = false
                img_remember.setImageDrawable(
                    ContextCompat.getDrawable(
                        applicationContext,
                        R.drawable.border_black_circle
                    )
                )


            } else {
                isRemember = true
                img_remember.setImageDrawable(
                    ContextCompat.getDrawable(
                        applicationContext,
                        R.drawable.ic_check_circle_black_24dp
                    )
                )


            }

        }
    }

    private fun isEmailValid(email: CharSequence): Boolean {
        return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()
    }


    fun callWebService() {

        progress_layout_login.visibility = View.VISIBLE
        val apiService = UserService.create(this.getString(R.string.base_url))


        val email = edt_log_email.text.toString()
        val password = edt_log_password.text.toString()

        val callService = apiService.newLogin(email.trim(), password.trim(), "application/json")
        callService.enqueue(object : retrofit2.Callback<UserResponse> {
            override fun onResponse(
                call: Call<UserResponse>,
                response: retrofit2.Response<UserResponse>?
            ) {
                if (response != null) {

                    if (response.code() == 200) {
                        val userResponse: UserProfileData? = response.body()!!.userResponseData!!.user

                        Prefs.putString(keys.USER_ID, response.body()!!.userResponseData!!.user!!.id)
                        Prefs.putString(keys.USER_TOKEN, response.body()!!.userResponseData!!.token!!.accessToken)
                        val mGson = GsonBuilder()
                            .setLenient()
                            .create()


                        Prefs.putString(keys.USER_FULL_DATA, mGson.toJson(response.body()!!.userResponseData!!.user))


                        val gameCredsList = userResponse!!.game_credits


                        if (gameCredsList!!.size > 0) {

                            for (game: GameCredits in gameCredsList) {

                                if (game.game_id == "1") {
                                    var totalCreds = game.credit!!.toInt() - game.used!!.toInt()
                                    Prefs.putString(keys.CREDITS_ARIEL, totalCreds.toString())
                                } else if (game.game_id == "2") {
                                    var totalCreds = game.credit!!.toInt() - game.used!!.toInt()
                                    Prefs.putString(keys.CREDITS_PANTENE, totalCreds.toString())
                                } else if (game.game_id == "3") {
                                    var totalCreds = game.credit!!.toInt() - game.used!!.toInt()
                                    Prefs.putString(keys.CREDITS_LUCKYME, totalCreds.toString())
                                }
                            }
                        }


                        val intent = Intent(applicationContext, MainMenuActivity::class.java)
                        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                        applicationContext.startActivity(intent)
                        overridePendingTransition(
                            R.anim.enterfrom,
                            R.anim.enterto
                        )

                        finish()

                    } else if (response.code() == 400) {
                        displayDialog("Invalid email or password")
                    } else {
                        displayDialog("Something went wrong!")

                    }
                }
                progress_layout_login.visibility = View.GONE
            }

            override fun onFailure(call: Call<UserResponse>, t: Throwable) {
                Log.e("aa", t.toString());
                progress_layout_login.visibility = View.GONE
                displayDialog("Something went wrong!!")
            }
        })

    }

    fun callWebServiceFacebook(user_id: String, fb_token: String) {

        progress_layout_login.visibility = View.VISIBLE
        val apiService = UserService.create(this.getString(R.string.base_url))

        val callService = apiService.newLoginFacebook(user_id, fb_token, "application/json")
        callService.enqueue(object : retrofit2.Callback<UserResponse> {
            override fun onResponse(
                call: Call<UserResponse>,
                response: retrofit2.Response<UserResponse>?
            ) {
                if (response != null) {

                    if (response.code() == 200) {
                        val userResponse: UserProfileData? = response.body()!!.userResponseData!!.user

                        Prefs.putString(keys.USER_ID, response.body()!!.userResponseData!!.user!!.id)
                        Prefs.putString(keys.USER_TOKEN, response.body()!!.userResponseData!!.token!!.accessToken)
                        val mGson = GsonBuilder()
                            .setLenient()
                            .create()


                        Prefs.putString(keys.USER_FULL_DATA, mGson.toJson(response.body()!!.userResponseData!!.user))


                        val gameCredsList = userResponse!!.game_credits


                        if (gameCredsList!!.size > 0) {

                            for (game: GameCredits in gameCredsList) {

                                if (game.game_id == "1") {
                                    var totalCreds = game.credit!!.toInt() - game.used!!.toInt()
                                    Prefs.putString(keys.CREDITS_ARIEL, totalCreds.toString())
                                } else if (game.game_id == "2") {
                                    var totalCreds = game.credit!!.toInt() - game.used!!.toInt()
                                    Prefs.putString(keys.CREDITS_PANTENE, totalCreds.toString())
                                } else if (game.game_id == "3") {
                                    var totalCreds = game.credit!!.toInt() - game.used!!.toInt()
                                    Prefs.putString(keys.CREDITS_LUCKYME, totalCreds.toString())
                                }
                            }
                        }


                        val intent = Intent(applicationContext, MainMenuActivity::class.java)
                        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                        applicationContext.startActivity(intent)
                        overridePendingTransition(
                            R.anim.enterfrom,
                            R.anim.enterto
                        )

                        finish()

                    } else if (response.code() == 400) {
                        displayDialog("Invalid email or password")
                    } else {
                        displayDialog("Something went wrong!")

                    }

                }
                progress_layout_login.visibility = View.GONE
            }

            override fun onFailure(call: Call<UserResponse>, t: Throwable) {
                Log.e("aa", t.toString());
                progress_layout_login.visibility = View.GONE
                displayDialog("Something went wrong!!")
            }
        })

    }


    fun displayDialog(message: String) {
        val builder = AlertDialog.Builder(this)
        builder.setMessage(message)
        builder.setPositiveButton("Ok") { dialog, _ ->
            dialog.dismiss()
        }
        val dialog: AlertDialog = builder.create()
        dialog.show()
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)



        if (requestCode == RC_SIGN_IN) {
            //requestCode Of Google
//            val result : GoogleSignInResult = Auth.GoogleSignInApi.getSignInResultFromIntent(data)
//            handleSignInResult(result)


        } else {
            //requestCode Of Facebook
            callbackManager?.onActivityResult(requestCode, resultCode, data)

            Log.d("data", "data here: " + data)
        }
    }

    inner class MyReceiver : BroadcastReceiver() {

        override fun onReceive(context: Context, intent: Intent) {
            //    do something
            val isConnected = intent.getBooleanExtra(ConnectivityManager.EXTRA_NO_CONNECTIVITY, false)
            if (isConnected) {
                // player.playWhenReady = false
                displayDialog("No internet connection.")
//                internet_connected.visibility = View.GONE
            } else {

                InternetCheck(object : InternetCheck.Consumer {
                    override fun accept(internet: Boolean?) {

                        if (internet!!) {

                            runOnUiThread(java.lang.Runnable {
                                //  player.playWhenReady = true
                                //  player.playbackState
                                //  internet_disconnect.visibility = View.GONE
                                //  internet_connected.visibility = View.VISIBLE

                                val timer = Timer()
                                timer.schedule(timerTask {
                                    runOnUiThread(Runnable {
                                        //    internet_connected.visibility = View.GONE

                                    })
                                }, 2500)

                            })

                        } else {
                            //  player.playWhenReady = false
                            //  internet_disconnect.visibility = View.VISIBLE

                            Toast.makeText(applicationContext, "Slow internet connection", Toast.LENGTH_SHORT).show()

                            //internet_connected.visibility = View.GONE
                        }

                    }
                })


            }
        }

    }

    internal class InternetCheck(private val mConsumer: Consumer) :
        AsyncTask<Void, Void, Boolean>() {
        interface Consumer {
            fun accept(internet: Boolean?)
        }

        init {
            execute()
        }

        override fun doInBackground(vararg voids: Void): Boolean? {
            try {
                val sock = Socket()
                sock.connect(InetSocketAddress("8.8.8.8", 53) as SocketAddress?, 1500)
                sock.close()
                return true
            } catch (e: IOException) {
                return false
            }
        }

        override fun onPostExecute(internet: Boolean?) {
            mConsumer.accept(internet)
        }
    }


}
