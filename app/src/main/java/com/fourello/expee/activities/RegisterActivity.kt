package com.fourello.expee.activities

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.net.ConnectivityManager
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.text.method.LinkMovementMethod
import android.text.method.PasswordTransformationMethod
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import com.fourello.expee.R
import com.fourello.expee.constant.keys
import com.fourello.expee.model.UserResponse
import com.fourello.expee.webservice.UserService
import com.google.gson.GsonBuilder
import com.pixplicity.easyprefs.library.Prefs
import kotlinx.android.synthetic.main.activity_login.*
import kotlinx.android.synthetic.main.activity_register.*
import retrofit2.Call
import retrofit2.Response
import java.util.*
import kotlin.concurrent.timerTask

class RegisterActivity : AppCompatActivity() {
    private lateinit var mReceiver: BroadcastReceiver
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)



        val filter = IntentFilter()
        filter.addAction(ConnectivityManager.CONNECTIVITY_ACTION)
        mReceiver = MyReceiver()
        registerReceiver(mReceiver, filter)


        edt_reg_password.transformationMethod = PasswordTransformationMethod()
        edt_reg_password_very.transformationMethod = PasswordTransformationMethod()
        back_register.setOnClickListener {

            finish()
            overridePendingTransition(R.anim.exitfrom, R.anim.exitto)
        }

        edt_reg_password.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {}

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                if (edt_reg_password.text.toString().trim() == edt_reg_password_very.text.toString().trim()) {

                    if (edt_reg_password.text.toString().trim() != "") {

                        password_check.visibility = View.VISIBLE
                        password_very_check.visibility = View.VISIBLE
                    }
                } else {
                    password_check.visibility = View.GONE
                    password_very_check.visibility = View.GONE

                }

            }
        })

        edt_reg_password_very.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {}

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                if (edt_reg_password.text.toString().trim() == edt_reg_password_very.text.toString().trim()) {

                    if (edt_reg_password.text.toString().trim() != "") {


                        password_check.visibility = View.VISIBLE
                        password_very_check.visibility = View.VISIBLE
                    }

                } else {
                    password_check.visibility = View.GONE
                    password_very_check.visibility = View.GONE
                }

            }
        })

        terms_policy.setMovementMethod(LinkMovementMethod.getInstance())

        btn_continue_register.setOnClickListener {

            if (edt_reg_fullname.text.toString().trim() == "") {

                displayDialog("Please enter your full name.")

            } else if (edt_reg_email.text.toString().trim() == "") {


                displayDialog("Please enter your email.")

            } else if (!isEmailValid(edt_reg_email.text.toString().trim())) {

                displayDialog("Email is invalid")

            }  else if (edt_reg_password.text.toString().trim() == "") {

                displayDialog("Please enter your password.")

            } else if (edt_reg_password.text.toString().length < 6) {

                displayDialog("Password should be at least 6 characters.")
            } else if (edt_reg_password.text.toString().trim() != edt_reg_password_very.text.toString().trim()) {

                displayDialog("Please verify your password.")

            } else if(!terms_check.isChecked) {

                displayDialog("Please agree to Expee's Terms of Service & Privacy Policy.")

            }else {
                callWebService()
            }
        }

    }


    fun callWebService() {


        progress_layout_reg.visibility = View.VISIBLE

        val apiService = UserService.create(this.getString(R.string.base_url))



        apiService.createNewAccount(
            edt_reg_fullname.text.toString().trim(),
            edt_reg_email.text.toString().trim(),
            edt_reg_password.text.toString().trim(),
            edt_reg_password.text.toString().trim(),
            "Android",
            "application/json"
        ).enqueue(object : retrofit2.Callback<UserResponse> {

            override fun onResponse(call: Call<UserResponse>, response: Response<UserResponse>) {
                if (response.code() == 201) {
                    val userResponse: UserResponse? = response.body()


                    Prefs.putString(keys.USER_ID,response.body()!!.userResponseData!!.user!!.id)

                    Prefs.putString(keys.USER_TOKEN, response.body()!!.userResponseData!!.token!!.accessToken)
                    val mGson = GsonBuilder()
                        .setLenient()
                        .create()


                    Prefs.putString(keys.USER_FULL_DATA, mGson.toJson(response.body()!!.userResponseData!!.user))


                    val intent = Intent(applicationContext, MainMenuActivity::class.java)
                    intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                    applicationContext.startActivity(intent)
                    overridePendingTransition(
                        R.anim.enterfrom,
                        R.anim.enterto
                    )
                    finish()


                } else if (response.code() == 422) {

                    displayDialog("The email has already been taken.")

                } else {
                    displayDialog("Something went wrong!")
                }
                progress_layout_reg.visibility = View.GONE

            }

            override fun onFailure(call: Call<UserResponse>, t: Throwable) {
                progress_layout_reg.visibility = View.GONE
                Log.e("aa", t.message)
                displayDialog("Something went wrong!")
            }
        })


    }

    private fun isEmailValid(email: CharSequence): Boolean {
        return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()
    }

    fun displayDialog(message: String) {
        val builder = AlertDialog.Builder(this)
        builder.setMessage(message)
        builder.setPositiveButton("Ok") { dialog, _ ->
            dialog.dismiss()
        }
        val dialog: AlertDialog = builder.create()
        dialog.show()
    }

    inner class MyReceiver : BroadcastReceiver() {

        override fun onReceive(context: Context, intent: Intent) {
            //    do something
            val isConnected = intent.getBooleanExtra(ConnectivityManager.EXTRA_NO_CONNECTIVITY, false)
            if (isConnected) {
                // player.playWhenReady = false
                displayDialog("No internet connection.")
//                internet_connected.visibility = View.GONE
            } else {

                LoginActivity.InternetCheck(object : LoginActivity.InternetCheck.Consumer {
                    override fun accept(internet: Boolean?) {

                        if (internet!!) {

                            runOnUiThread(Runnable {
                                //  player.playWhenReady = true
                                //  player.playbackState
                                //  internet_disconnect.visibility = View.GONE
                                //  internet_connected.visibility = View.VISIBLE

                                val timer = Timer()
                                timer.schedule(timerTask {
                                    runOnUiThread(Runnable {
                                        //    internet_connected.visibility = View.GONE

                                    })
                                }, 2500)

                            })

                        } else {
                            //  player.playWhenReady = false
                            //  internet_disconnect.visibility = View.VISIBLE

                            Toast.makeText(applicationContext, "Slow internet connection", Toast.LENGTH_SHORT).show()

                            //internet_connected.visibility = View.GONE
                        }

                    }
                })


            }
        }

    }

}
