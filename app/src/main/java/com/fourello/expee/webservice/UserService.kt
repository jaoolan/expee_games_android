package com.fourello.expee.webservice

import com.fourello.expee.model.*
import com.fourello.expee_kotlin.model.BeaconMainResponse
import com.google.gson.GsonBuilder
import okhttp3.MultipartBody
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.converter.scalars.ScalarsConverterFactory
import retrofit2.http.*
import okhttp3.OkHttpClient
import java.util.concurrent.TimeUnit



//API's
interface UserService {

    @FormUrlEncoded
    @POST("api/login")
    fun newLogin(
        @Field("username") email: String,
        @Field("password") password: String,
        @Field("accept") accept : String
    ): Call<UserResponse>

//newLoginFacebook
    @FormUrlEncoded
    @POST("api/facebook/login")
    fun newLoginFacebook(
        @Field("user_id") user_id: String,
        @Field("token") token: String,
        @Field("application/json") accept : String
    ): retrofit2.Call<UserResponse>

    @FormUrlEncoded
    @POST("api/account/{id}")
    fun createProfile(
        @Path("id") id: String,
        @Field("gender") gender : String,
        @Field("age_range") age_range: String,
        @Field("_method") _method: String,
        @Header("Accept") accept: String,
        @Header("Authorization") authHeader: String
    ): Call<UpdateCreditsResponse>


    @FormUrlEncoded
    @POST("api/account/{id}")
    fun updateCredits(
        @Path("id") id: String,
        @Field("_method") _method: String,
        @Header("Accept") accept: String,
        @Header("Authorization") authHeader: String
    ): Call<UpdateCreditsResponse>

    @Headers("Content-Type: application/json")
    @GET("/api/beacon")
    fun getBeaconNewApi(
        @Query("show_relationship") show_relationship: Int,
        @Header("authorization") authHeader: String
    ): Call<BeaconMainResponse>


//api/v3/checkout
    @Multipart
    @POST("api/v3/checkout")
    fun gameCheckout(
        @Part file: MultipartBody.Part,
        @Part("game_code") game_code: String,
        @Part("branch_id") branch_id: String,
        @Header("Accept") accept: String,
        @Header("Authorization") authHeader: String
    ): retrofit2.Call<Object>



    //select winner based on game code ex. "ariel_spin_the_wheel"
    @FormUrlEncoded
    @POST("api/v2/game/{code}")
    fun selectWinner(
        @Path("code") code: String,
      //  @Field("branch_id") branch_id: String,
        @Field("_method") _method: String,
        @Header("Accept") accept: String,
        @Header("Authorization") authHeader: String
    ): Call<Object>

    @FormUrlEncoded
    @POST("api/account/register")
    fun createNewAccount(
        @Field("full_name") full_name: String,
        @Field("email") email: String,
        @Field("password") password: String,
        @Field("password_confirmation") password_confirmation: String,
        @Field("device") device: String,
        @Header("Accept") accept: String
    ): retrofit2.Call<UserResponse>



    @Headers("Content-Type: application/json")
    @GET("api/v2/game/prize")
    fun getPrizes(
        @Header("Accept") accept: String,
        @Header("Authorization") authHeader: String
    ): Call<PrizeAllResponse>



    @FormUrlEncoded
    @POST("api/v2/game/prize/{id}")
    fun claimPrizes(
        @Path("id") id: String,
        @Field("_method") method: String,
        @Header("Accept") accept: String,
        @Header("Authorization") authHeader: String
    ): Call<ClaimedPrizeResponse>

    @GET("api/reset/{email}")
         fun forgotPassword(
        @Path("email") email: String,
        @Header("Accept") accept: String
    ): Call<Object>


    @GET("api/v3/robinson/scan")
    fun enterGameCode(
        @Query("code") code: String,
        @Header("Accept") accept: String,
        @Header("Authorization") authHeader: String
    ): Call<Object>


    @FormUrlEncoded
    @POST("api/v2/push/register")
     fun registerToken(
        @Field("device_token") device_token: String,
        @Field("platform") platform: String,
        @Header("_method") method: String,
        @Header("accept") accept: String,
        @Header("authorization") authHeader: String
    ): Call<Object>

    @FormUrlEncoded
    @POST("api/v2/push/unregister")
    abstract fun unRegisterToken(
        @Field("device_token") device_token: String,
        @Header("_method") method: String,
        @Header("accept") accept: String,
        @Header("authorization") authHeader: String
    ): Call<Object>

    companion object Factory {
        val okHttpClient = OkHttpClient.Builder()
            .connectTimeout(40, TimeUnit.SECONDS)
            .writeTimeout(40, TimeUnit.SECONDS)
            .readTimeout(40, TimeUnit.SECONDS)
            .build()

        var gson = GsonBuilder()
            .setLenient()
            .create()
        fun create(baseurl: String): UserService {
            val retrofit = Retrofit.Builder()
                .baseUrl(baseurl)
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(okHttpClient)
                .build()
            return retrofit.create(UserService::class.java)
        }
    }

}